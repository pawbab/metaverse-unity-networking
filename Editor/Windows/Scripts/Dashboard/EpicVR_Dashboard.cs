using MUN.Service;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.EpicVR_MUN_Consts;

namespace MUN
{
    public class EpicVR_Dashboard : EditorWindow
    {
        private const string LOGGED_ASS_TEXT = "Logged as: {0}";

        [SerializeField] private VisualTreeAsset dashboardWindow;

        [Space]
        [SerializeField] private VisualTreeAsset applicationsWindow;

        [SerializeField] private VisualTreeAsset organizationsWindow;
        [SerializeField] private VisualTreeAsset accountWindow;

        private EpicVR_Dashboard_Account accountDashboard;
        private EpicVR_Dashboard_Organizations organizationsDashboard;
        private EpicVR_Dashboard_Applications applicationsDashboard;

        [MenuItem("MUN/MUN Dashboard", priority = 1)]
        public static void Init()
        {
            bool isLogged = !string.IsNullOrEmpty(ServiceRequest.Account.Token);

            if (!isLogged)
            {
                EpicVR_LoginWindow.Init();
                return;
            }

            EpicVR_Dashboard wnd = GetWindow<EpicVR_Dashboard>();
            wnd.minSize = EpicVR_MUN_Consts.MIN_WIN_SIZE;
            wnd.titleContent = new GUIContent("MUN Dashboard");
        }

        private void OnEnable()
        {
            accountDashboard = new EpicVR_Dashboard_Account();
            organizationsDashboard = new EpicVR_Dashboard_Organizations();
            applicationsDashboard = new EpicVR_Dashboard_Applications();
        }

        private void OnInspectorUpdate()
        {
            var loadingView = rootVisualElement.Q<VisualElement>(name: "loading-view");
            loadingView.style.display = ServiceRequest.IsSending ? DisplayStyle.Flex : DisplayStyle.None;
        }

        public async void CreateGUI()
        {
            VisualElement root = rootVisualElement;
            dashboardWindow.CloneTree(root);

            //Handle Username text:
            var loggedAsLabel = root.Q<Label>(name: "txt-logged-as");
            loggedAsLabel.text = string.Format(LOGGED_ASS_TEXT, ServiceRequest.Account.Username.WithColor(SelectedButtonColor).WithBold());

            //Handle Logout:
            var btnLogout = root.Q<Button>(name: "btn-logout");
            btnLogout.clicked += () =>
            {
                ServiceRequest.Account.Logout();
                Close();
                EpicVR_LoginWindow.Init();
            };

            //Handle Main Section:
            var mainSection = root.Q<VisualElement>(name: "section-main");

            //Handle buttons:
            var btnApps = root.Q<Button>(name: "btn-apps");
            var btnOrgs = root.Q<Button>(name: "btn-orgs");
            var btnAcct = root.Q<Button>(name: "btn-acct");
            var buttons = new Button[3] { btnApps, btnOrgs, btnAcct };

            ChangeMainSection(mainSection, applicationsWindow, buttons, 0);
            await applicationsDashboard.CreateGUIAsync(mainSection);

            btnApps.clicked += async () =>
            {
                ChangeMainSection(mainSection, applicationsWindow, buttons, 0);
                await applicationsDashboard.CreateGUIAsync(mainSection);
            };
            btnOrgs.clicked += async () =>
            {
                ChangeMainSection(mainSection, organizationsWindow, buttons, 1);
                await organizationsDashboard.CreateGuiAsync(mainSection);
            };
            btnAcct.clicked += async () =>
            {
                ChangeMainSection(mainSection, accountWindow, buttons, 2);
                await accountDashboard.CreateGuiAsync(mainSection);
            };
        }

        private void ChangeMainSection(VisualElement mainSection, VisualTreeAsset asset, Button[] buttons, int btnEnableIndex)
        {
            mainSection.Clear();

            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].style.backgroundColor = (i == btnEnableIndex) ? SelectedButtonColor : DefaultButtonColor;
            }

            asset.CloneTree(mainSection);
        }
    }
}