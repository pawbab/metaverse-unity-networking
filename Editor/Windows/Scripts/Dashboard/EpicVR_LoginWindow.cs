using MUN.Service;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static MUN.EpicVR_Validators;

namespace MUN
{
    public class EpicVR_LoginWindow : EditorWindow
    {
        [SerializeField] private VisualTreeAsset loginWindow;

        public static void Init()
        {
            EpicVR_LoginWindow wnd = GetWindow<EpicVR_LoginWindow>();
            wnd.minSize = EpicVR_MUN_Consts.MIN_WIN_SIZE;
            wnd.titleContent = new GUIContent("Login");
        }

        public void CreateGUI()
        {
            VisualElement root = rootVisualElement;
            loginWindow.CloneTree(root);

            //Handle PanelInfo:
            var panelInfo = root.Q<VisualElement>(name: "panel-info");
            panelInfo.style.display = DisplayStyle.None;

            //Handle Button:
            var btnLogin = root.Q<Button>(name: "btn-login");
            var btnRegister = root.Q<Button>(name: "btn-register");
            btnLogin.SetEnabled(false);

            //Handle Fields:
            var fieldUsername = root.Q<TextField>(name: "field-username");
            var fieldPassword = root.Q<TextField>(name: "field-password");

            fieldUsername.value = ServiceRequest.Account.Username;

            //Handle Validators:
            var usernameValidator = fieldUsername.Q<Label>(name: "txt-validator");
            var passwordValidator = fieldPassword.Q<Label>(name: "txt-validator");
            IsValid(Type.LoginOrEmail, in usernameValidator, false, fieldUsername.value);
            IsValid(Type.Password, in passwordValidator, false, fieldPassword.value);
            btnLogin.SetEnabled(!usernameValidator.visible && !passwordValidator.visible);

            fieldUsername.RegisterValueChangedCallback((evt) =>
            {
                if (panelInfo.enabledSelf)
                    panelInfo.style.display = DisplayStyle.None;

                var isValid = IsValid(Type.LoginOrEmail, usernameValidator, true, fieldUsername.value);
                btnLogin.SetEnabled(isValid && !passwordValidator.visible);
            });

            fieldPassword.RegisterValueChangedCallback((evt) =>
            {
                IsValid(Type.Password, passwordValidator, true, fieldPassword.value);
                btnLogin.SetEnabled(!usernameValidator.visible && !passwordValidator.visible);
                panelInfo.style.display = DisplayStyle.None;
            });

            btnLogin.clicked += async () =>
            {
                var hasErrors = false;
                btnLogin.SetEnabled(false);

                await ServiceRequest.Account.Login(fieldUsername.value, fieldPassword.value, (errors) =>
                {
                    hasErrors = true;
                    var errMsg = string.Join("; ", errors.Errors.Values);
                    panelInfo.Q<Label>(name: "txt-info").text = errMsg;
                    panelInfo.style.display = DisplayStyle.Flex;
                });

                fieldPassword.SetValueWithoutNotify(string.Empty);

                if (!hasErrors)
                {
                    Close();
                    EpicVR_Dashboard.Init();
                }
            };

            btnRegister.clicked += () =>
            {
                Close();
                EpicVR_RegisterWindow.Init();
            };
        }
    }
}