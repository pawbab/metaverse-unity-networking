using MUN.Service;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN
{
    public class EpicVR_Dashboard_Applications : EditorWindow
    {
        private const string APPID_TEXT = "<b>AppId:</b> {0}";
        private const string MAX_PLAYERS_TEXT = "<b>Max players: </b> {0}";
        private const string TICK_RATE_TEXT = "<b>Tick rate: </b> {0}";

        [SerializeField] private VisualTreeAsset applicationPrefab;
        [SerializeField] private StyleSheet activeServerStyleSheet;
        [SerializeField] private StyleSheet disactiveServerStyleSheet;

        private ServiceRequest.IOrganization selectedOrganization;
        private EpicVR_Dashboard_Application_Create createApplication;

        private void OnEnable()
        {
            createApplication = new EpicVR_Dashboard_Application_Create();
        }

        public async Task CreateGUIAsync(VisualElement root)
        {
            #region Handle Fields

            //Handle Panels:
            var panelInfoErrors = root.Q<VisualElement>(name: "panel-info");
            var sectionApps = root.Q<VisualElement>(name: "section-applications");

            //Handle Dropdowns:
            var dropSelectOrganization = root.Q<DropdownField>(name: "drop-selected-organization");

            //Handle Lists:
            var appList = root.Q<ScrollView>(name: "app-list");

            //Handle Buttons:
            var btnCreate = root.Q<Button>(name: "btn-create-new");

            #endregion Handle Fields

            #region Init

            await InitDropdownOrganizations(true);

            #endregion Init

            #region Callbacks

            btnCreate.clicked += async () =>
            {
                await createApplication.CreateGUIAsync(root, selectedOrganization);
            };

            dropSelectOrganization.RegisterValueChangedCallback(async (callback) =>
            {
                selectedOrganization = ServiceRequest.Organization.Organizations[dropSelectOrganization.index];
                await UpdateApplicationsView(selectedOrganization);
            });

            #endregion Callbacks

            async Task InitDropdownOrganizations(bool updateMembers)
            {
                if (ServiceRequest.Organization.Organizations == null)
                    await ServiceRequest.Organization.GetAll();

                var orgNames = GetOrganizationNames(ServiceRequest.Organization.Organizations);

                if (orgNames.Length == 0)
                {
                    sectionApps.visible = false;
                    panelInfoErrors.Q<Label>(name: "txt-error-text").text = EpicVR_Dashboard_Organizations.NO_ORGANIZATIONS;
                    panelInfoErrors.style.display = DisplayStyle.Flex;
                    return;
                }

                dropSelectOrganization.choices = new List<string>();
                foreach (var organization in orgNames)
                {
                    dropSelectOrganization.choices.Add(organization);
                }
                dropSelectOrganization.index = 0;
                selectedOrganization = ServiceRequest.Organization.Organizations[dropSelectOrganization.index];

                if (updateMembers)
                    await UpdateApplicationsView(selectedOrganization);
            }

            async Task UpdateApplicationsView(ServiceRequest.IOrganization organization)
            {
                appList.Clear();
                panelInfoErrors.style.display = DisplayStyle.None;

                var content = new VisualElement();
                content.style.flexDirection = FlexDirection.Row;
                content.style.flexWrap = Wrap.Wrap;
                content.style.width = sectionApps.style.width;

                await ServiceRequest.Application.GetAll(organization.Id);

                appList.Add(content);

                foreach (var application in ServiceRequest.Application.Applications)
                {
                    var itsMyName = ServiceRequest.Account.Username.Equals(application);
                    var app = applicationPrefab.Instantiate();
                    var shortAppId = $"{application.AppId.Substring(0, 6)}...";

                    app.styleSheets.Add(application.IsActive ? activeServerStyleSheet : disactiveServerStyleSheet);
                    app.Q<Label>(name: "txt-app-name").text = application.Name;
                    app.Q<Label>(name: "txt-app-id").text = string.Format(APPID_TEXT, shortAppId);
                    app.Q<Label>(name: "txt-max-players").text = string.Format(MAX_PLAYERS_TEXT, application.MaxPlayers);
                    app.Q<Label>(name: "txt-tick-rate").text = string.Format(TICK_RATE_TEXT, application.TickRate);

                    var btnDetails = app.Q<Button>(name: "btn-details");

                    content.Add(app);
                    if (!btnDetails.enabledSelf) continue;

                    btnDetails.clicked += () =>
                    {
                        EpicVR_Dashboard_Application_Details.Init(application, async () =>
                        {
                            await UpdateApplicationsView(selectedOrganization);
                        });
                    };
                }
            }
        }

        private string[] GetOrganizationNames(in ServiceRequest.IOrganization[] organizations)
        {
            string[] organizationNames = new string[organizations.Length];
            for (int i = 0; i < organizationNames.Length; i++)
            {
                organizationNames[i] = organizations[i].Name;

                if (organizations[i].IAmOwner)
                    organizationNames[i] += " [owner]";
            }

            return organizationNames;
        }
    }
}