﻿using UnityEngine.UIElements;

namespace MUN
{
    public interface IEpicVR_Validator
    {
        bool IsValid(Label validatorLabel, bool showErrorMessage, params string[] values);
    }
}