﻿using System.Text.RegularExpressions;

namespace MUN
{
    public class EpicVR_Validator_Builder
    {
        private readonly EpicVR_Validator _target;

        public EpicVR_Validator_Builder()
        {
            _target = new EpicVR_Validator();
            _target.AllowEmpty = true;
            _target.CheckLength = false;
            _target.MinLength = 0;
            _target.MaxLength = int.MaxValue;
            _target.Regex = null;
        }

        public EpicVR_Validator_Builder WithNoEmpty(in string emptyErrorMessage)
        {
            _target.AllowEmpty = false;
            _target.EmptyErrorMessage = emptyErrorMessage;
            return this;
        }

        public EpicVR_Validator_Builder WithLength(in int minLength, in int maxLength, in string wrongLengthErrorMessage)
        {
            _target.CheckLength = true;
            _target.MinLength = minLength;
            _target.MaxLength = maxLength;
            _target.WrongLengthMessage = wrongLengthErrorMessage;
            return this;
        }

        public EpicVR_Validator_Builder WithPattern(in Regex regex, in string regexNotMatchErrorMessage)
        {
            _target.Regex = regex;
            _target.RegexNotMachMessage = regexNotMatchErrorMessage;
            return this;
        }

        public EpicVR_Validator_Builder WithCompareWithOther(in string noEqualToOtherMessage)
        {
            _target.NotEqualToOtherMessage = noEqualToOtherMessage;
            return this;
        }

        private EpicVR_Validator Build() => _target;

        public static implicit operator EpicVR_Validator(EpicVR_Validator_Builder builder) => builder.Build();
    }
}