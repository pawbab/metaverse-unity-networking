using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN
{
    public class EpicVR_Validator : IEpicVR_Validator
    {
        public bool AllowEmpty { get; set; }
        public bool CheckLength { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public Regex Regex { get; set; }

        public string EmptyErrorMessage { get; set; }
        public string WrongLengthMessage { get; set; }
        public string RegexNotMachMessage { get; set; }
        public string NotEqualToOtherMessage { get; set; }

        public bool IsValid(Label validatorLabel, bool showErrorMessage, params string[] values)
        {
            if (values is null || values.Length == 0)
            {
                Debug.LogError("Validate values are null or empty!");
                return false;
            }

            if (!AllowEmpty && string.IsNullOrEmpty(values[0]))
            {
                ShowError(EmptyErrorMessage, in showErrorMessage);
                return false;
            }

            if (CheckLength && (values[0].Length < MinLength || values[0].Length > MaxLength))
            {
                ShowError(WrongLengthMessage, in showErrorMessage);
                return false;
            }

            if (Regex != null && !Regex.IsMatch(values[0]))
            {
                ShowError(RegexNotMachMessage, in showErrorMessage);
                return false;
            }

            if (values.Length > 1 && !string.IsNullOrEmpty(NotEqualToOtherMessage))
            {
                var firstValue = values[0];
                var allAreEquals = values.Skip(1).All(x => x.Equals(firstValue));

                if (!allAreEquals)
                {
                    ShowError(NotEqualToOtherMessage, in showErrorMessage);
                    return false;
                }
            }

            validatorLabel.visible = false;
            return true;

            void ShowError(in string msg, in bool showErrorMessage)
            {
                validatorLabel.visible = showErrorMessage;
                ((INotifyValueChanged<string>)validatorLabel).SetValueWithoutNotify(msg);
            }
        }
    }
}