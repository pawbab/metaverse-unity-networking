using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN
{
    public static class EpicVR_Validators
    {
        public enum Type
        {
            Login,
            Password,
            Email,
            ConfirmPassword,
            OrganizationName,
            ApplicationName,
            AppId,
            LoginOrEmail
        }

        private static Dictionary<Type, IEpicVR_Validator> Validators { get; }

        static EpicVR_Validators()
        {
            Validators = new Dictionary<Type, IEpicVR_Validator>();
            Seed();
        }

        public static bool IsValid(Type validatorType, in Label validatorLabel, bool showErrorMessage = true, params string[] values)
        {
            if (!Validators.TryGetValue(validatorType, out IEpicVR_Validator validator))
            {
                Debug.LogError($"Not found validator with type = {validatorType}.");
                return false;
            }

            return validator.IsValid(validatorLabel, showErrorMessage, values);
        }

        private static void Seed()
        {
            EpicVR_Validator loginValidator = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty.")
                .WithLength(5, 24, "The field must contains from 5 to 24 characters.")
                .WithPattern(new Regex(@"^[a-zA-Z][a-zA-Z0-9]{4,24}$"), "Unacceptable characters. Allowed: a-z, A-Z, 0-9.");

            Validators.Add(Type.Login, loginValidator);

            EpicVR_Validator passwordValidator = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty.")
                .WithLength(5, 24, "The field must contains from 5 to 24 characters.")
                .WithPattern(new Regex(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{5,24}$"), "Must contains: 1 uppercase; 1 lowercase; 1 number.");

            Validators.Add(Type.Password, passwordValidator);

            EpicVR_Validator confirmPasswordValidator = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty.")
                .WithCompareWithOther("The passwords are not the same. Try again.");

            Validators.Add(Type.ConfirmPassword, confirmPasswordValidator);

            EpicVR_Validator emailValidator = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty.")
                .WithPattern(new Regex(@"^([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)$"), "The required field is the email address.");

            Validators.Add(Type.Email, emailValidator);

            EpicVR_Validator organizationName = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty")
                .WithLength(5, 24, "The field must contains from 5 to 24 characters.")
                .WithPattern(new Regex(@"^[a-zA-Z0-9 ]{5,24}$"), "Unacceptable characters. Allowed: a-z, A-Z, 0-9.");

            Validators.Add(Type.OrganizationName, organizationName);

            EpicVR_Validator applicationName = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty")
                .WithLength(5, 24, "The field must contains from 5 to 24 characters.")
                .WithPattern(new Regex(@"^[a-zA-Z0-9 ]{5,24}$"), "Unacceptable characters. Allowed: a-z, A-Z, 0-9.");

            Validators.Add(Type.ApplicationName, applicationName);

            EpicVR_Validator appId = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty")
                .WithLength(36, 36, "The field must contains 36 characters.")
                .WithPattern(new Regex(@"^[\da-zA-z]{8}-([\da-zA-z]{4}-){3}[\da-zA-z]{12}$"), "Invalid GUID (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx) format.");

            Validators.Add(Type.AppId, appId);

            EpicVR_Validator loginOrEmail = new EpicVR_Validator_Builder()
                .WithNoEmpty("The field cannot be empty")
                .WithLength(5, 24, "The field must contains from 5 to 24 characters.")
                .WithPattern(new Regex(@"^[a-zA-Z][a-zA-Z0-9]{4,24}$|^([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)$"), "The required field is login (allowed characters: a-z, A-Z, 0-9) or email address.");

            Validators.Add(Type.LoginOrEmail, loginOrEmail);
        }
    }
}