using UnityEngine;

namespace MUN
{
    public static class EpicVR_MUN_Consts
    {
        internal const string EPICVR_WEBSITE = "https://epicvr.pl/pl/filmy-360-gry-aplikacje-vr/";
        internal const string MUN_DOCUMENTATION = "http://vagency.smarthost.pl/mun-doc/";

        internal static readonly Vector2 MIN_WIN_SIZE = new Vector2(720, 520);

        private const string DEFAULT_BTN_COLOR = "#585858";
        private const string SELECTED_BTN_COLOR = "#2A8163";

        public static Color DefaultButtonColor
        {
            get
            {
                if (ColorUtility.TryParseHtmlString(DEFAULT_BTN_COLOR, out var buttonColor))
                    return buttonColor;

                return Color.clear;
            }
        }

        public static Color SelectedButtonColor
        {
            get
            {
                if (ColorUtility.TryParseHtmlString(SELECTED_BTN_COLOR, out var buttonColor))
                    return buttonColor;

                return Color.clear;
            }
        }
    }
}