using MUN.Service;
using System;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace MUN
{
    public class EpicVR_Asset_Setup : EditorWindow
    {
        [SerializeField] private VisualTreeAsset visualTree;

        [Space]
        [SerializeField] private VisualTreeAsset generalSettingsWindow;

        [SerializeField] private VisualTreeAsset rpcsWindow;

        private EpicVR_Asset_Setup_General general;
        private EpicVR_Asset_Setup_RPCs rpcs;

        private Action<VisualElement> mainSectionFunc;

        [MenuItem("MUN/MUN Asset Setup", priority = 2)]
        public static void Init()
        {
            EpicVR_Asset_Setup wnd = GetWindow<EpicVR_Asset_Setup>();
            wnd.minSize = EpicVR_MUN_Consts.MIN_WIN_SIZE;
            wnd.titleContent = new GUIContent("MUN Asset Setup");
        }

        private void OnEnable()
        {
            general = new EpicVR_Asset_Setup_General();
            rpcs = new EpicVR_Asset_Setup_RPCs();
        }

        private void OnInspectorUpdate()
        {
            var loadingView = rootVisualElement.Q<VisualElement>(name: "loading-view");
            loadingView.style.display = (ServiceRequest.IsSending) ? DisplayStyle.Flex : DisplayStyle.None;
        }

        private void OnFocus()
        {
            if (mainSectionFunc is null) return;
            var mainSection = rootVisualElement.Q<VisualElement>(name: "main-section");
            mainSectionFunc.Invoke(mainSection);
        }

        public void CreateGUI()
        {
            VisualElement root = rootVisualElement;
            visualTree.CloneTree(root);

            #region Handle Inputs:

            var mainSection = root.Q<VisualElement>(name: "main-section");

            var btnGeneral = root.Q<Button>(name: "btn-general");
            var btnRPCs = root.Q<Button>(name: "btn-rpcs");

            var btnWWW = root.Q<Button>(name: "btn-www");
            var btnDiscord = root.Q<Button>(name: "btn-discord");
            var btnYoutube = root.Q<Button>(name: "btn-youtube");
            var btnDocumentation = root.Q<Button>(name: "btn-documentation");

            #endregion Handle Inputs:

            #region Init

            var buttons = new Button[2] { btnGeneral, btnRPCs };
            ChangeMainSection(mainSection, generalSettingsWindow, in buttons, 0);
            mainSectionFunc = general.CreateGUI;
            mainSectionFunc.Invoke(mainSection);

            //TODO: Doda� rozwi�zanie dla tych dw�ch przycisk�w:
            btnDiscord.SetEnabled(false);
            btnYoutube.SetEnabled(false);

            #endregion Init

            #region Callbacks

            btnGeneral.clicked += () =>
            {
                ChangeMainSection(mainSection, generalSettingsWindow, in buttons, 0);
                mainSectionFunc = general.CreateGUI;
                mainSectionFunc.Invoke(mainSection);
            };

            btnRPCs.clicked += () =>
            {
                ChangeMainSection(mainSection, rpcsWindow, in buttons, 1);
                mainSectionFunc = rpcs.CreateGUI;
                mainSectionFunc.Invoke(mainSection);
            };

            btnWWW.clicked += () =>
            {
                Process.Start(EpicVR_MUN_Consts.EPICVR_WEBSITE);
            };

            btnDiscord.clicked += () =>
            {
                //TODO:
            };

            btnYoutube.clicked += () =>
            {
                //TODO:
            };

            btnDocumentation.clicked += () =>
            {
                Process.Start(EpicVR_MUN_Consts.MUN_DOCUMENTATION);
            };

            #endregion Callbacks
        }

        private void ChangeMainSection(VisualElement mainSection, VisualTreeAsset asset, in Button[] buttons, int btnEnableIndex)
        {
            mainSection.Clear();
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].style.backgroundColor = (i == btnEnableIndex) ? EpicVR_MUN_Consts.SelectedButtonColor : EpicVR_MUN_Consts.DefaultButtonColor;
            }

            asset.CloneTree(mainSection);
        }
    }
}