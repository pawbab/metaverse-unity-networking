﻿using MUN.Client;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

namespace MUN
{
    public class EpicVR_Asset_Setup_RPCs : EditorWindow
    {
        public void CreateGUI(VisualElement root)
        {
            #region Handle Inputs

            var rpcList = root.Q<ScrollView>(name: "rpcs-list");

            var btnRefresh = root.Q<Button>(name: "btn-refresh");
            var btnClear = root.Q<Button>(name: "btn-clear");

            #endregion Handle Inputs

            #region Init

            var settings = MUNSettings.Get();
            settings.RefreshRpcs();

            FillRPCList(settings.RPCs);

            #endregion Init

            #region Callbacks

            btnClear.clicked += () =>
            {
                rpcList.Clear();
            };

            btnRefresh.clicked += () =>
            {
                settings.RefreshRpcs();
                FillRPCList(settings.RPCs);
            };

            #endregion Callbacks

            #region Functions

            void FillRPCList(IEnumerable<string> rpcs)
            {
                rpcList.Clear();

                if (rpcs is null | rpcs.Count() == 0)
                {
                    rpcList.Add(new Label("\t--- EMPTY ---".WithColor(EpicVR_MUN_Consts.SelectedButtonColor)));
                    return;
                }
                int index = 0;
                foreach (var rpc in rpcs)
                {
                    rpcList.Add(new Label($"\t{++index}. {rpc}"));
                }
            }

            #endregion Functions
        }
    }
}