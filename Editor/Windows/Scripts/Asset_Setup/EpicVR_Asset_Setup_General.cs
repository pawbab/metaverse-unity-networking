using MUN.Client;
using MUN.Service;
using UnityEditor;
using UnityEngine.UIElements;
using static MUN.EpicVR_Validators;

namespace MUN
{
    public class EpicVR_Asset_Setup_General : EditorWindow
    {
        private const string NO_APPID_MESSAGE = "For the multiplayer system to work properly, you must enter the application ID, which you will find in the details of the application you created (Dashboard -> Applications)";

        public void CreateGUI(VisualElement root)
        {
            #region Handle Inputs

            var panelSuccess = root.Q<VisualElement>(name: "panel-info-success");
            var successText = panelSuccess.Q<Label>(name: "txt-info");

            var panelError = root.Q<VisualElement>(name: "panel-info");
            var errorText = panelError.Q<Label>(name: "txt-info");

            var fieldAppId = root.Q<TextField>(name: "field-appid");
            var validatorAppId = fieldAppId.Q<Label>(name: "txt-validate");

            var btnShowAppId = fieldAppId.Q<Button>(name: "btn-show");
            var btnTestAppId = fieldAppId.Q<Button>(name: "btn-test");

            var tglLogDefault = root.Q<Toggle>(name: "log-type-default");
            var tglLogWarning = root.Q<Toggle>(name: "log-type-warning");
            var tglLogError = root.Q<Toggle>(name: "log-type-error");
            var tglLogException = root.Q<Toggle>(name: "log-type-exception");

            #endregion Handle Inputs

            #region Init

            var settings = MUNSettings.Get();

            fieldAppId.SetValueWithoutNotify(settings.AppID);

            if (string.IsNullOrEmpty(settings.AppID))
                SetErrorMessage(NO_APPID_MESSAGE);
            else
                IsValid(Type.AppId, validatorAppId, true, settings.AppID);

            CheckLog(tglLogDefault, MUNLogs.LogTypes.Log);
            CheckLog(tglLogWarning, MUNLogs.LogTypes.Warning);
            CheckLog(tglLogError, MUNLogs.LogTypes.Error);
            CheckLog(tglLogException, MUNLogs.LogTypes.Exception);

            #endregion Init

            #region Callbacks

            fieldAppId.RegisterValueChangedCallback((callback) =>
            {
                if (string.IsNullOrEmpty(fieldAppId.value))
                {
                    SetErrorMessage(NO_APPID_MESSAGE);
                    settings.AppID = string.Empty;
                    return;
                }

                var isValid = IsValid(Type.AppId, validatorAppId, true, fieldAppId.value);

                if (isValid && settings.AppID != fieldAppId.value)
                    settings.AppID = fieldAppId.value;
            });

            btnShowAppId.clicked += () =>
            {
                fieldAppId.isPasswordField = !fieldAppId.isPasswordField;
            };

            btnTestAppId.clicked += async () =>
            {
                var isSuccess = await ServiceRequest.Application.Test(fieldAppId.value, (err) =>
                 {
                     var errMsg = string.Join("; ", err.Errors.Values);
                     SetErrorMessage(errMsg);
                 });

                if (isSuccess)
                {
                    SetSuccessMessage("AppId is correct");
                }
            };

            tglLogDefault.RegisterValueChangedCallback((callback) =>
            {
                if (tglLogDefault.value)
                    settings.LogTypes = settings.LogTypes.Add(MUNLogs.LogTypes.Log);
                else
                    settings.LogTypes = settings.LogTypes.Remove(MUNLogs.LogTypes.Log);
            });

            tglLogWarning.RegisterValueChangedCallback((callback) =>
            {
                if (tglLogWarning.value)
                    settings.LogTypes = settings.LogTypes.Add(MUNLogs.LogTypes.Warning);
                else
                    settings.LogTypes = settings.LogTypes.Remove(MUNLogs.LogTypes.Warning);
            });

            tglLogError.RegisterValueChangedCallback((callback) =>
            {
                if (tglLogError.value)
                    settings.LogTypes = settings.LogTypes.Add(MUNLogs.LogTypes.Error);
                else
                    settings.LogTypes = settings.LogTypes.Remove(MUNLogs.LogTypes.Error);
            });

            tglLogException.RegisterValueChangedCallback((callback) =>
            {
                if (tglLogException.value)
                    settings.LogTypes = settings.LogTypes.Add(MUNLogs.LogTypes.Exception);
                else
                    settings.LogTypes = settings.LogTypes.Remove(MUNLogs.LogTypes.Exception);
            });

            #endregion Callbacks

            #region Functions

            void SetSuccessMessage(string msg)
            {
                successText.text = msg;
                panelSuccess.style.display = DisplayStyle.Flex;
                panelError.style.display = DisplayStyle.None;
            }

            void SetErrorMessage(string msg)
            {
                errorText.text = msg;
                panelError.style.display = DisplayStyle.Flex;
                panelSuccess.style.display = DisplayStyle.None;
            }

            void CheckLog(Toggle tgl, MUNLogs.LogTypes log)
            {
                tgl.SetValueWithoutNotify(settings.LogTypes.HasFlag(log));
            }

            #endregion Functions
        }
    }
}