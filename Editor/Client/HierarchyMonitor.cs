using MUN.Client;
using UnityEditor;
using UnityEngine;

namespace MUN.Editor
{
    [InitializeOnLoad]
    public class HierarchyMonitor
    {
        static HierarchyMonitor()
        {
            EditorApplication.hierarchyChanged += OnHierarchyChanged;
        }

        private static void OnHierarchyChanged()
        {
            if (Application.isPlaying) return;

            UpdateMUNView();
        }

        private static void UpdateMUNView()
        {
            MUNView[] munViews = GameObject.FindObjectsOfType<MUNView>(true);
            if (munViews == null || munViews.Length == 0) return;

            MUNView.UpdateViewsId(munViews);

            foreach (var munView in munViews)
            {
                munView.FindSerializeComponents();
            }
        }
    }
}