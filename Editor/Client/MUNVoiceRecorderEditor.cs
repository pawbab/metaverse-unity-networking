using MUN.Client.VoiceChat;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace MUN.Editor
{
    [UnityEditor.CustomEditor(typeof(MUNVoiceRecorder))]
    public class MUNVoiceRecorderEditor : UnityEditor.Editor
    {
        private MUNVoiceRecorder voiceRecorder;
        private SerializedProperty minValueToPass;

        private int selectedInputDevice;
        private string[] inputDevices;

        private void Awake()
        {
            if (Application.isPlaying)
                inputDevices = GetInputDevices();
        }

        private void OnEnable()
        {
            voiceRecorder = (MUNVoiceRecorder)target;
            minValueToPass = serializedObject.FindProperty("minValueToPass");

            selectedInputDevice = voiceRecorder.CurrentDeviceIndex;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(minValueToPass);

            DisplayInputDevices();

            serializedObject.ApplyModifiedProperties();

            void DisplayInputDevices()
            {
                GUILayout.Label("Microphone:", EditorStyles.boldLabel);

                if (!Application.isPlaying)
                {
                    EditorGUILayout.HelpBox("Settings will be shown in playmode only.", MessageType.Warning);
                    return;
                }

                selectedInputDevice = EditorGUILayout.Popup(voiceRecorder.CurrentDeviceIndex, inputDevices);

                if (selectedInputDevice != voiceRecorder.CurrentDeviceIndex)
                {
                    voiceRecorder.ChangeDevice(selectedInputDevice);
                }
            }
        }

        private string[] GetInputDevices()
        {
            var devices = voiceRecorder.Devices.ToArray();
            for (int i = 0; i < devices.Length; i++)
            {
                devices[i] = devices[i].Replace('/', '\\');
            }

            return devices;
        }
    }
}