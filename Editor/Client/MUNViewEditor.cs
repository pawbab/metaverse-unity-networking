using MUN.Client;
using UnityEditor;
using UnityEngine;

namespace MUN.Editor
{
    [CustomEditor(typeof(MUNView))]
    public class MUNViewEditor : UnityEditor.Editor
    {
        private MUNView view;

        private SerializedProperty serializeComponents;
        private SerializedProperty id;

        private void OnEnable()
        {
            view = (MUNView)target;

            serializeComponents = serializedObject.FindProperty("serializeComponents");
            id = serializedObject.FindProperty("id");
            if (!Application.isPlaying) ((MUNView)target).FindSerializeComponents();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(id, new GUIContent("View ID:"));
            if (Application.isPlaying)
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField($"Owner: {view.Owner?.Name}");
                EditorGUILayout.LabelField($"Is mine: {view.IsMine}");
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(serializeComponents);

            serializedObject.ApplyModifiedProperties();
        }
    }
}