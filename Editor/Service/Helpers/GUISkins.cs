﻿using UnityEngine;

namespace MUN.Editor
{
    internal static class GUISkins
    {
        private static readonly Font codeFont;

        public static GUIStyle GroupOffset(RectOffset margin, RectOffset padding)
        {
            var skin = new GUIStyle(GUI.skin.box);
            skin.margin = margin;
            skin.padding = padding;

            return skin;
        }

        public static GUIStyle ExpandButton()
        {
            var skin = new GUIStyle(GUI.skin.box);

            return skin;
        }

        public static GUIStyle GroupOffsetWithTextClipping(RectOffset margin, RectOffset padding,
            bool richText = true)
        {
            var skin = new GUIStyle(GUI.skin.box);
            skin.margin = margin;
            skin.padding = padding;

            skin.richText = richText;

            skin.clipping = TextClipping.Clip;

            return skin;
        }

        public static GUIStyle Margin(RectOffset margin)
        {
            var skin = new GUIStyle();
            skin.margin = margin;

            return skin;
        }

        public static GUIStyle TextRich
        {
            get
            {
                var skin = new GUIStyle(GUI.skin.label);
                skin.richText = true;
                return skin;
            }
        }

        public static GUIStyle CodeText()
        {
            var skin = new GUIStyle
            {
                richText = true,
                clipping = TextClipping.Clip,
                stretchWidth = false,
                font = codeFont,
                fontSize = 12,
            };
            return skin;
        }

        public static GUIStyle CodeText(float width)
        {
            var skin = new GUIStyle
            {
                richText = true,
                clipping = TextClipping.Clip,
                stretchWidth = false,
                fixedWidth = width,
                font = codeFont,
                fontSize = 12,
            };
            return skin;
        }
    }
}