using System.Diagnostics;
using UnityEditor;

namespace MUN.Client.MUNEditor
{
    public class MUN_Documentation : EditorWindow
    {
        [MenuItem("MUN/MUN Documentation", false, 999)]
        public static void OpenDocumentation()
        {
            Process.Start(EpicVR_MUN_Consts.MUN_DOCUMENTATION);
        }
    }
}