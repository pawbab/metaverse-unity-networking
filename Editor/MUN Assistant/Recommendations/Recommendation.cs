using System;
using System.IO;
using System.Linq;
using UnityEditor;

namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public class Recommendation
    {
        private string _path;

        /// <summary>
        /// The path to the file that the Recommendation applies to.
        /// </summary>
        public string Path
        {
            get => _path;
            private set
            {
                _path = value;

                var pathMembers = Path.Split('/', '\\');
                FileName = pathMembers[pathMembers.Length - 1];
            }
        }

        /// <summary>
        /// File name generated during setting file path.
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Init recommendation file code.
        /// </summary>
        public string Code { get; private set; }

        /// <summary>
        /// Fixes in recommendation.
        /// </summary>
        public FixView[] Fixes { get; private set; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="path">The path to the file that the Recommendation applies to.</param>
        /// <param name="code">Init recommendation file code.</param>
        /// <param name="fixes">Fixes in recommendation.</param>
        public Recommendation(string path, string code, FixView[] fixes)
        {
            Path = path;
            Code = code;
            Fixes = fixes;
        }

        /// <summary>
        /// The method fixes the code based on the selected fix.
        /// Method don't changing the source file.
        /// </summary>
        /// <param name="fixView">Fix to provide.</param>
        /// <param name="withHighlightTags">Add to code highlight tags.</param>
        /// <returns>Code with the provided fix.</returns>
        public string Fix(FixView fixView, bool withHighlightTags)
        {
            string beforeFixExtent = Code.Substring(0, fixView.Start);
            string afterFixExtent = Code.Substring(fixView.Stop, Code.Length - fixView.Stop);

            if (withHighlightTags)
                return beforeFixExtent + $"<b>{fixView.Text}</b>" + afterFixExtent;

            return beforeFixExtent + $"{fixView.Text}" + afterFixExtent;
        }

        /// <summary>
        /// The method fixes the code based on the selected fix.
        /// Method don't changing the source file.
        /// </summary>
        /// <param name="fixIndex">Index of Fix to provide.</param>
        /// <param name="withHighlightTags">Add to code highlight tags.</param>
        /// <returns>Code with the provided fix.</returns>
        public string Fix(int fixIndex, bool withHighlightTags)
            => Fix(Fixes[fixIndex], withHighlightTags);

        /// <summary>
        /// The method fixes the code based on the selected fix
        /// and provide changes to the source file.
        /// </summary>
        /// <param name="fixIndex">Index of Fix to provide.</param>
        public void ProvideFix(int fixIndex)
        {
            if (Fixes[fixIndex].Applied
                || Fixes[fixIndex].Ignored)
                return;

            Fixes[fixIndex].Applied = true;

            string fixedCode = Fix(fixIndex, false);

            File.WriteAllText(Path, fixedCode);

            int indexesOffset = fixedCode.Length - Code.Length;

            Code = fixedCode;

            IgnoreConflicts(fixIndex);
            MoveIndexesIfFixIsBefore(fixIndex, indexesOffset);

            if (AllFixesIgnored())
                FilePaths.RemovePath(Constants.SCRIPTS_TO_ANALISE, Path);

            AssetDatabase.Refresh();
        }

        /// <summary>
        /// The method fixes the code based on the selected fix
        /// and provide changes to the source file.
        /// </summary>
        /// <param name="fixView">Fix to provide.</param>
        public void ProvideFix(FixView fixView)
        {
            if (fixView.Applied || fixView.Ignored) return;

            int fixIndex = Array.IndexOf(Fixes, fixView);

            fixView.Applied = true;

            string fixedCode = Fix(fixView, false);

            File.WriteAllText(Path, fixedCode);

            int indexesOffset = fixedCode.Length - Code.Length;

            Code = fixedCode;

            IgnoreConflicts(fixIndex);
            MoveIndexesIfFixIsBefore(fixIndex, indexesOffset);

            if (AllFixesIgnored())
                FilePaths.RemovePath(Constants.SCRIPTS_TO_ANALISE, Path);
        }

        /// <summary>
        /// Set Fix to ignore for the selected fix,
        /// if the fix is in conflict with the selected one.
        /// </summary>
        /// <param name="fixIndex">Index of Fix to check conflicts.</param>
        public void IgnoreConflicts(int fixIndex)
        {
            for (int i = 0; i < Fixes.Length; i++)
            {
                if (i == fixIndex) continue;

                // If fix start is in another fix extent
                if (Fixes[i].Start >= Fixes[fixIndex].Start
                    && Fixes[i].Start <= Fixes[fixIndex].Stop)
                {
                    Fixes[i].Ignored = true;

                    if (AllFixesIgnored())
                        FilePaths.RemovePath(Constants.SCRIPTS_TO_ANALISE, Path);

                    continue;
                }

                // If fix stop is in another fix extent
                if (Fixes[i].Stop >= Fixes[fixIndex].Start
                    && Fixes[i].Stop <= Fixes[fixIndex].Stop)
                {
                    Fixes[i].Ignored = true;

                    if (AllFixesIgnored())
                        FilePaths.RemovePath(Constants.SCRIPTS_TO_ANALISE, Path);
                }
            }
        }

        /// <summary>
        /// Move "Start" and "Stop" indexes for each fixes,
        /// if the fix was before the selected fix.
        /// </summary>
        /// <param name="fixIndex">Index of the fix for comparison.</param>
        /// <param name="indexesOffset">Indexes movement offset.</param>
        private void MoveIndexesIfFixIsBefore(int fixIndex, int indexesOffset)
        {
            for (int i = 0; i < Fixes.Length; i++)
            {
                if (i == fixIndex) continue;

                // If fix was before another fix
                if (Fixes[fixIndex].Start < Fixes[i].Start)
                    Fixes[i].MoveIndexes(indexesOffset);
            }
        }

        /// <summary>
        /// Move "Start" and "Stop" indexes for each fixes,
        /// if the selected fix was before. Method move additionally
        /// fix by the pragma text length.
        /// </summary>
        /// <param name="fixIndex">Index of fix for comparison.</param>
        /// <param name="startPragmaLenght">Lenght of the start pragma text.</param>
        /// <param name="pragmaLenght">Lenght of start and end pragmas.</param>
        private void MoveIndexesForPragmas(int fixIndex, int startPragmaLenght, int pragmaLenght)
        {
            for (int i = 0; i < Fixes.Length; i++)
            {
                if (i != fixIndex)
                {
                    // If fix was in another checked fix line
                    if (GetStartLineIndex(fixIndex) == GetStartLineIndex(i))
                        Fixes[i].MoveIndexes(startPragmaLenght);
                    // If fix was before checked fix line
                    else if (GetStartLineIndex(fixIndex) < GetStartLineIndex(i))
                        Fixes[i].MoveIndexes(pragmaLenght);
                }
            }

            Fixes[fixIndex].MoveIndexes(startPragmaLenght);
        }

        /// <summary>
        /// Add "b" tags for selected fix.
        /// </summary>
        /// <param name="fixIndex">Index of Fix for which method adds tags.</param>
        /// <returns>Return code with "b" tags around the selected fix.</returns>
        public string AddBoldTags(int fixIndex)
        {
            return AddBoldTags(Fixes[fixIndex]);
        }

        /// <summary>
        /// Get index of start text line of the selected fix.
        /// </summary>
        /// <param name="fixIndex">Index of Fix</param>
        /// <returns>Index of the line where the fix begins.</returns>
        public int GetStartLineIndex(int fixIndex)
        {
            return GetStartLineIndex(Fixes[fixIndex]);
        }

        /// <summary>
        /// Get lines count with selected fix.
        /// Count lines from Start to Stop index.
        /// </summary>
        /// <param name="fixIndex">Index of fix</param>
        /// <returns>Lines count of the selected fix.</returns>
        public int GetLinesCount(int fixIndex)
        {
            return GetLinesCount(Fixes[fixIndex]);
        }

        /// <summary>
        /// Add ignore pragma for the selected fix. Write pragmas to the source file.
        /// Ana at the end move indexes after pragma.
        /// </summary>
        /// <param name="fixIndex">Index of fix to ignore.</param>
        public void IgnoreFix(int fixIndex)
        {
            Fixes[fixIndex].Ignored = true;

            var codeLines = Code.Split('\n').ToList();

            int fixStartLine = GetStartLineIndex(fixIndex);
            int fixEndLine = GetEndLineIndex(fixIndex);

            string startPragma = Constants.GetStartPragma(Fixes[fixIndex].Type);
            codeLines.Insert(fixStartLine, startPragma);
            string endPragma = Constants.GetEndPragma(Fixes[fixIndex].Type);
            codeLines.Insert(fixEndLine + 2, endPragma);

            Code = String.Join("\n", codeLines);
            File.WriteAllText(Path, Code);

            int moveOffset = 2 + startPragma.Length + endPragma.Length; // Plus two end line marks
            MoveIndexesForPragmas(fixIndex, startPragma.Length + 1, moveOffset);

            if (AllFixesIgnored())
                FilePaths.RemovePath(Constants.SCRIPTS_TO_ANALISE, Path);

            AssetDatabase.Refresh();
        }

        private bool AllFixesIgnored()
        {
            return Fixes.All(f => f.Applied || f.Ignored);
        }

        /// <summary>
        /// Add "b" tags for selected fix.
        /// </summary>
        /// <param name="fixView">Fix for which method adds tags.</param>
        /// <returns>Return code with "b" tags around the selected fix.</returns>
        private string AddBoldTags(FixView fixView)
        {
            string beforeFixExtent = Code.Substring(0, fixView.Start);
            string afterFixExtent = Code.Substring(fixView.Stop + 1, Code.Length - fixView.Stop - 1);
            string textToBold = Code.Substring(fixView.Start, fixView.Stop - fixView.Start + 1);

            return beforeFixExtent + $"<b>{textToBold}</b>" + afterFixExtent;
        }

        /// <summary>
        /// Get start line index of the selected fix.
        /// </summary>
        /// <param name="fixView">Selected fix</param>
        /// <returns>Index of fix start line.</returns>
        private int GetStartLineIndex(FixView fixView)
        {
            int result = 0;
            for (int i = 0; i < fixView.Start; i++)
            {
                if (Code[i] == '\n')
                    result++;
            }

            return result;
        }

        /// <summary>
        /// Get end line index of the selected fix.
        /// </summary>
        /// <param name="fixView">Selected fix</param>
        /// <returns>Index of fix end line.</returns>
        private int GetEndLineIndex(FixView fixView)
        {
            int result = 0;
            for (int i = 0; i < fixView.Stop; i++)
            {
                if (Code[i] == '\n')
                    result++;
            }

            return result;
        }

        /// <summary>
        /// Get end line index of the selected fix.
        /// </summary>
        /// <param name="fixIndex">Index of the selected fix</param>
        /// <returns>Index of fix end line.</returns>
        public int GetEndLineIndex(int fixIndex)
        {
            FixView fixView = Fixes[fixIndex];

            int result = 0;
            for (int i = 0; i < fixView.Stop; i++)
            {
                if (Code[i] == '\n')
                    result++;
            }

            return result;
        }

        /// <summary>
        /// Get lines count with selected fix.
        /// Count lines from Start to Stop index.
        /// </summary>
        /// <param name="fixView">Selected fix</param>
        /// <returns>Lines count of the selected fix.</returns>
        private int GetLinesCount(FixView fixView)
        {
            int startLine = GetStartLineIndex(fixView);
            int endLine = GetEndLineIndex(fixView);
            return endLine - startLine + 1;
        }

        /// <summary>
        /// Get lines count of the selected fix. Count lines of the fix Text.
        /// </summary>
        /// <param name="fixIndex">Index of selected fix</param>
        /// <returns>Lines count of the selected fix</returns>
        public int GetFixLinesCount(int fixIndex)
        {
            FixView fix = Fixes[fixIndex];
            return fix.Text.Split('\n').Length;
        }
    }
}