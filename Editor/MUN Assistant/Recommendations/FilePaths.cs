﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;

namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public static class FilePaths
    {
        public static string GetDirectoryPath => Path.Combine(Application.persistentDataPath, "MUN/Data");

        public static string GetFullPath(in string fileName) => Path.Combine(GetDirectoryPath, fileName);

        public static string[] GetPaths(in string fileName)
        {
            var fullPath = GetFullPath(in fileName);

            if (File.Exists(fullPath))
                return File.ReadAllLines(fullPath, System.Text.Encoding.UTF8);

            return Array.Empty<string>();
        }

        public static void AddPath(in string fileName, in string path)
        {
            string textToWrite = path + Environment.NewLine;
            var fullPath = GetFullPath(in fileName);

            if (!Directory.Exists(GetDirectoryPath))
                Directory.CreateDirectory(GetDirectoryPath);

            if (File.Exists(fullPath))
            {
                var allLines = File.ReadAllLines(fullPath);
                if (allLines.Contains(path)) return;
            }

            File.AppendAllText(fullPath, textToWrite, System.Text.Encoding.UTF8);
        }

        public static void RemovePath(in string fileName, in string path)
        {
            var fullPath = GetFullPath(in fileName);

            if (!File.Exists(fullPath)) return;

            var paths = File.ReadAllLines(fullPath).ToList();
            paths.Remove(path);

            DeleteFile(in fileName);
            File.WriteAllLines(fullPath, paths);
        }

        public static void DeleteFile(in string fileName)
        {
            var fullPath = GetFullPath(in fileName);

            if (!File.Exists(fullPath)) return;
            File.Delete(fullPath);
        }
    }
}