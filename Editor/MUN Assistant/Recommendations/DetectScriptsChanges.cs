using UnityEditor;
using UnityEditor.Callbacks;

namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public class DetectScriptsChanges : AssetPostprocessor
    {
        private const string IGNORE_WORD = "MUN";

        [DidReloadScripts]
        private static void OnScriptsReloaded()
        {
            var paths = FilePaths.GetPaths(Constants.LAST_MODIFIED_SCRIPTS);

            if (paths.Length > 0)
                RecommendationWindow.OnScriptsChanged(paths);
        }

        private static void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            foreach (string path in importedAssets)
            {
                if (path.Contains(IGNORE_WORD)) continue;

                string[] splitStr = path.Split('/', '.');
                string extension = splitStr[splitStr.Length - 1];

                if (extension == "cs")
                    FilePaths.AddPath(Constants.LAST_MODIFIED_SCRIPTS, in path);
            }
        }
    }
}