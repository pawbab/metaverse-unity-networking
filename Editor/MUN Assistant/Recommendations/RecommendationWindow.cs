using CodeAnalysis;
using MUN.Editor.MUN_Assistant.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public class RecommendationWindow : EditorWindow
    {
        private static EditorWindow _window;

        private static Vector2 _scrollPosition;

        private static CodeAnalyzer _codeAnalyzer;

        private static List<Recommendation> _recommendations = new List<Recommendation>();

        private static RefBool noneSwitch = new RefBool();
        private static RefBool logicalSwitch = new RefBool();
        private static RefBool missingThrowSwitch = new RefBool();
        private static RefBool controlFlowManagementSwitch = new RefBool();
        private static RefBool incorrectAssigmentSwitch = new RefBool();
        private static RefBool initializationSwitch = new RefBool();
        private static RefBool vrSwitch = new RefBool();

        private static RefBool[] filters = new RefBool[]
        {
            noneSwitch, logicalSwitch, missingThrowSwitch, controlFlowManagementSwitch,
            incorrectAssigmentSwitch, initializationSwitch, vrSwitch
        };

        private static bool munRecommendationsEnabled;

        private float ScrollViewWidth => _window.position.width - 5;

        private static CodeAnalyzer CodeAnalyzer
        {
            get
            {
                if (_codeAnalyzer == null)
                {
                    string projectPath = Path.GetFullPath("Packages/com.epicvr.mun/Runtime/Plugins/CodeAnalysis");
                    string modelPath = Path.Combine(projectPath, "model.zip");
                    _codeAnalyzer = new CodeAnalyzer(projectPath, modelPath);
                }

                return _codeAnalyzer;
            }
        }

        /// <summary>
        /// Open recommendation window
        /// </summary>
        [MenuItem("MUN/MUN Recommendations")]
        private static void Init()
        {
            _window = (RecommendationWindow)GetWindow(typeof(RecommendationWindow));
            _window.titleContent = new GUIContent("MUN Recommendations");
            _window.minSize = new Vector2(750, 128);
            _window.autoRepaintOnSceneChange = false;

            InitFilteringSwitches();

            OnScriptsChanged(Array.Empty<string>(), false);
            _window.Show();
        }

        public static void OnScriptsChanged(IList<string> changedScripts, bool runInit = true)
        {
            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.MunRecommendations))
            {
                munRecommendationsEnabled = EditorPrefs.GetBool(Constants.EditorPrefKeys.MunRecommendations);
            }
            else
            {
                munRecommendationsEnabled = true;
                EditorPrefs.SetBool(Constants.EditorPrefKeys.MunRecommendations, munRecommendationsEnabled);
            }

            var pathsToAnalise = SavePathsToAnalise(changedScripts).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            if (!munRecommendationsEnabled || changedScripts.Count <= 0)
                return;

            _recommendations.Clear();

            foreach (string path in pathsToAnalise)
            {
                if (!File.Exists(path))
                {
                    FilePaths.RemovePath(Constants.SCRIPTS_TO_ANALISE, in path);
                    continue;
                }

                string code = File.ReadAllText(path);

                var fixesRaw = CodeAnalyzer.Repair(code);

                var fixes = fixesRaw
                    .Where(x => x != null)
                    .Select(x => new FixView(x))
                    .ToArray();

                if (fixes == null || fixes.Length == 0)
                {
                    FilePaths.RemovePath(Constants.SCRIPTS_TO_ANALISE, in path);
                    continue;
                }

                Recommendation recommendation = new Recommendation(path, code, fixes);
                _recommendations.Add(recommendation);
            }

            if (_recommendations.Count > 0 && runInit)
                Init();
        }

        private static string[] SavePathsToAnalise(IList<string> changedScripts)
        {
            var savedPaths = FilePaths.GetPaths(Constants.SCRIPTS_TO_ANALISE);

            // Convert changed scripts to full path.
            changedScripts = changedScripts
                .Select(GetFullPath)
                .ToArray();

            var resultPaths = new List<string>(savedPaths);

            foreach (string script in changedScripts)
            {
                if (savedPaths.Contains(script)) continue;

                resultPaths.Add(script);
                FilePaths.AddPath(Constants.SCRIPTS_TO_ANALISE, in script);
            }

            return resultPaths.ToArray();
        }

        private void OnGUI()
        {
            if (EditorApplication.isCompiling)
            {
                EditorGUILayout.HelpBox("Compilling scripts...", MessageType.Info);
                return;
            }

            int visibleRecommendationsCount = _recommendations
                .Sum(x => x.Fixes
                    .Count(f => !ShouldFilterFix(f) && !f.Ignored && !f.Applied));
            int recommendationsCount = _recommendations.Sum(x => x.Fixes
                .Count(f => !f.Ignored && !f.Applied));

            RecommendationWindowGUIFabric.Toolbar(IgnoreAll, ApplyAll,
                () =>
                {
                    noneSwitch.Value = !noneSwitch.Value;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.NoneSwitch, noneSwitch.Value);
                },
                () =>
                {
                    logicalSwitch.Value = !logicalSwitch.Value;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.LogicalSwitch, logicalSwitch.Value);
                },
                () =>
                {
                    missingThrowSwitch.Value = !missingThrowSwitch.Value;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.MissingThrowSwitch, missingThrowSwitch.Value);
                },
                () =>
                {
                    controlFlowManagementSwitch.Value = !controlFlowManagementSwitch.Value;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.ControlFlowManagementSwitch,
                        controlFlowManagementSwitch.Value);
                },
                () =>
                {
                    incorrectAssigmentSwitch.Value = !incorrectAssigmentSwitch.Value;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.IncorrectAssigmentSwitch,
                        incorrectAssigmentSwitch.Value);
                },
                () =>
                {
                    initializationSwitch.Value = !initializationSwitch.Value;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.InitializationEditorSwitch,
                        initializationSwitch.Value);
                },
                () =>
                {
                    vrSwitch.Value = !vrSwitch.Value;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.VrSwitch, vrSwitch.Value);
                },
                ref filters, ref munRecommendationsEnabled, recommendationsCount, visibleRecommendationsCount,
                _recommendations);

            _scrollPosition =
                RecommendationWindowGUIFabric.ContentScrollView(_scrollPosition, () =>
                {
                    int recommendationIndex = 0;
                    foreach (Recommendation recommendation in _recommendations)
                    {
                        int fixIndex = 0;
                        foreach (FixView fix in recommendation.Fixes)
                        {
                            if (ShouldFilterFix(fix))
                            {
                                fixIndex++;
                                continue;
                            }

                            if (fix.Ignored || fix.Applied)
                            {
                                fixIndex++;
                                continue;
                            }

                            GUILayout.Space(5);
                            var tempRecommendationIndex = recommendationIndex;
                            var tempFixIndex = fixIndex;
                            RecommendationWindowGUIFabric.DrawRecommendationItem(_recommendations, recommendationIndex,
                                fixIndex, ScrollViewWidth,
                                () => _recommendations[tempRecommendationIndex].IgnoreFix(tempFixIndex),
                                () => _recommendations[tempRecommendationIndex].ProvideFix(tempFixIndex));
                            GUILayout.Space(5);

                            fixIndex++;
                        }

                        recommendationIndex++;
                    }
                });
        }

        /// <summary>
        /// Is fix to skip?
        /// </summary>
        /// <param name="fix">Fix to check</param>
        /// <returns>Return true if the fix should be skipped.</returns>
        private bool ShouldFilterFix(FixView fix)
        {
            return (fix.Type == CodeError.None && !noneSwitch.Value)
                   || (fix.Type == CodeError.LogicalError && !logicalSwitch.Value)
                   || (fix.Type == CodeError.MissingThrowError && !missingThrowSwitch.Value)
                   || (fix.Type == CodeError.ControlFlowManagementError && !controlFlowManagementSwitch.Value)
                   || (fix.Type == CodeError.IncorrectAssignmentError && !incorrectAssigmentSwitch.Value)
                   || (fix.Type == CodeError.InitializationError && !initializationSwitch.Value)
                   || (fix.Type == CodeError.VRError && !vrSwitch.Value);
        }

        private static void InitFilteringSwitches()
        {
            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.NoneSwitch))
                noneSwitch.Value = EditorPrefs.GetBool(Constants.EditorPrefKeys.NoneSwitch);
            else
            {
                EditorPrefs.SetBool(Constants.EditorPrefKeys.NoneSwitch, true);
                noneSwitch.Value = true;
            }

            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.LogicalSwitch))
                logicalSwitch.Value = EditorPrefs.GetBool(Constants.EditorPrefKeys.LogicalSwitch);
            else
            {
                EditorPrefs.SetBool(Constants.EditorPrefKeys.LogicalSwitch, true);
                logicalSwitch.Value = true;
            }

            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.MissingThrowSwitch))
                missingThrowSwitch.Value = EditorPrefs.GetBool(Constants.EditorPrefKeys.MissingThrowSwitch);
            else
            {
                EditorPrefs.SetBool(Constants.EditorPrefKeys.MissingThrowSwitch, true);
                missingThrowSwitch.Value = true;
            }

            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.ControlFlowManagementSwitch))
                controlFlowManagementSwitch.Value =
                    EditorPrefs.GetBool(Constants.EditorPrefKeys.ControlFlowManagementSwitch);
            else
            {
                EditorPrefs.SetBool(Constants.EditorPrefKeys.ControlFlowManagementSwitch, true);
                controlFlowManagementSwitch.Value = true;
            }

            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.IncorrectAssigmentSwitch))
                incorrectAssigmentSwitch.Value = EditorPrefs.GetBool(Constants.EditorPrefKeys.IncorrectAssigmentSwitch);
            else
            {
                EditorPrefs.SetBool(Constants.EditorPrefKeys.IncorrectAssigmentSwitch, true);
                incorrectAssigmentSwitch.Value = true;
            }

            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.InitializationEditorSwitch))
                initializationSwitch.Value = EditorPrefs.GetBool(Constants.EditorPrefKeys.InitializationEditorSwitch);
            else
            {
                EditorPrefs.SetBool(Constants.EditorPrefKeys.InitializationEditorSwitch, true);
                initializationSwitch.Value = true;
            }

            if (EditorPrefs.HasKey(Constants.EditorPrefKeys.VrSwitch))
                vrSwitch.Value = EditorPrefs.GetBool(Constants.EditorPrefKeys.VrSwitch);
            else
            {
                EditorPrefs.SetBool(Constants.EditorPrefKeys.VrSwitch, true);
                vrSwitch.Value = true;
            }
        }

        private void IgnoreAll()
        {
            foreach (Recommendation recommendation in _recommendations)
            {
                int i = 0;
                foreach (FixView fix in recommendation.Fixes)
                {
                    if (ShouldFilterFix(fix))
                    {
                        i++;
                        continue;
                    }

                    recommendation.IgnoreFix(i);
                    i++;
                }
            }

            AssetDatabase.Refresh();
        }

        private void ApplyAll()
        {
            foreach (Recommendation recommendation in _recommendations)
                foreach (FixView fix in recommendation.Fixes)
                    if (!ShouldFilterFix(fix))
                        recommendation.ProvideFix(fix);

            AssetDatabase.Refresh();
        }

        private static string GetFullPath(string path)
        {
            var appPathMembers = Application.dataPath.Split('/', '\\');
            // Remove last element - Assets, because next array contains Assets folder
            appPathMembers = appPathMembers.Take(appPathMembers.Length - 1).ToArray();
            var pathMembers = path.Split('/', '\\');
            return Path.Combine(String.Join("/", appPathMembers), String.Join("/", pathMembers));
        }
    }
}