using CodeAnalysis;
using System;
using UnityEngine;

namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public class FixView
    {
        /// <summary>
        /// Fix content.
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Fix description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Index of the fix beginning.
        /// </summary>
        public int Start { get; private set; }

        /// <summary>
        /// Index of the fix end. It excludes itself.
        /// </summary>
        public int Stop { get; private set; }

        public CodeError Type { get; set; }

        public bool IsExpanded { get; set; }
        public Vector2 ScrollViewPosition { get; set; }
        public bool Ignored { get; set; }
        public bool Applied { get; set; }

        public FixView(string text, int start, int stop, string description = "", CodeError type = CodeError.None)
        {
            Text = text;
            Start = start;
            Stop = stop;
            Type = type;
            Description = description;
        }

        public FixView(Fix fix)
        {
            Text = fix.Text;
            Description = fix.Message;
            Start = fix.Start;
            Stop = fix.Stop;
            Type = fix.ErrorType;
        }

        /// <summary>
        /// Move "Start" and "Stop" indexes by "i" value.
        /// </summary>
        /// <param name="i"></param>
        public void MoveIndexes(int i)
        {
            Start += i;
            Stop += i;
        }

        public override string ToString()
        {
            return String.Concat("Fix text: '", Text, "', position in source code: (", Start, ", ", Stop, ")");
        }
    }
}