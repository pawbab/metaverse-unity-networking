using CodeAnalysis;

// ReSharper disable InconsistentNaming

namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public static class Constants
    {
        public const string SCRIPTS_TO_ANALISE = "ScriptsToAnalise.txt";
        public const string LAST_MODIFIED_SCRIPTS = "LastModified.txt";

        public static string GetStartPragma(CodeError type)
        {
            return $"#pragma warning disable MunRecommendationIgnore:{(int)type}";
        }

        public static string GetEndPragma(CodeError type)
        {
            return $"#pragma warning restore MunRecommendationIgnore:{(int)type}";
        }

        public static class EditorPrefKeys
        {
            public const string NoneSwitch = "RecommendationsNoneSwitch";
            public const string LogicalSwitch = "RecommendationsLogicalSwitch";
            public const string MissingThrowSwitch = "RecommendationsMissingThrowSwitch";
            public const string ControlFlowManagementSwitch = "RecommendationsControlFlowManagementSwitch";
            public const string IncorrectAssigmentSwitch = "RecommendationsIncorrectAssigmentSwitch";
            public const string InitializationEditorSwitch = "RecommendationsInitializatinSwitch";
            public const string VrSwitch = "RecommendationsVrSwitch";

            public const string MunRecommendations = "RecommendationsEnabled";
        }
    }
}