using CodeAnalysis;
using MUN.Editor.MUN_Assistant.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public static class RecommendationWindowGUIFabric
    {
        private const float rowHeigth = 20;

        private const float ItemShortContentHeight = 60;
        private const float ItemLongContentHeight = 240;

        private static RectOffset _margin0 = new RectOffset(0, 0, 0, 0);
        private static RectOffset _padding0 = new RectOffset(0, 0, 0, 0);

        private static float tableSpacing = 5f;

        /// <summary>
        /// Draw toolbar
        /// </summary>
        /// <param name="onIgnoreAll"></param>
        /// <param name="onApplyAll"></param>
        /// <param name="onNoneSwitch"></param>
        /// <param name="onLogicalSwitch"></param>
        /// <param name="onMissingThrow"></param>
        /// <param name="onControlFlowManagement"></param>
        /// <param name="onIncorrectAssigment"></param>
        /// <param name="onInitialization"></param>
        /// <param name="onVr"></param>
        /// <param name="filters"></param>
        /// <param name="munRecommendationsEnabled"></param>
        /// <param name="recommendationsCount"></param>
        /// <param name="recommendations"></param>
        public static void Toolbar(Action onIgnoreAll, Action onApplyAll, Action onNoneSwitch, Action onLogicalSwitch,
            Action onMissingThrow, Action onControlFlowManagement, Action onIncorrectAssigment,
            Action onInitialization, Action onVr, ref RefBool[] filters, ref bool munRecommendationsEnabled,
            int recommendationsCount, int visibleRecommendationsCount, List<Recommendation> recommendations)
        {
            EditorGUILayout.BeginHorizontal("Toolbar", GUILayout.ExpandWidth(true));
            {
                bool munRecommendationsEnabledTemp = EditorGUILayout.Toggle("Show on scripts compiled",
                    munRecommendationsEnabled);

                if (munRecommendationsEnabledTemp != munRecommendationsEnabled) // Edit Prefs only if value is different
                {
                    munRecommendationsEnabled = munRecommendationsEnabledTemp;
                    EditorPrefs.SetBool(Constants.EditorPrefKeys.MunRecommendations, munRecommendationsEnabled);
                }

                GUILayout.FlexibleSpace();

                //if (GUILayout.Button("Regenerate test script"))
                //    RegenerateTestScript();

                GUILayout.Label($"Recommendations count: {visibleRecommendationsCount}/{recommendationsCount}. Check <b>Filters</b> if you don't see all.  ", GUISkins.TextRich);

                if (GUILayout.Button("Filters", "ToolbarButton"))
                    ShowFilteringPopup(ref filters, onNoneSwitch, onLogicalSwitch, onMissingThrow,
                        onControlFlowManagement, onIncorrectAssigment, onInitialization, onVr, recommendations);

                GUILayout.Label("    Quick: ");

                if (GUILayout.Button("Ignore all", "ToolbarButton"))
                    onIgnoreAll?.Invoke();

                if (GUILayout.Button("Apply all", "ToolbarButton"))
                    onApplyAll?.Invoke();
            }
            EditorGUILayout.EndHorizontal();
        }

        #region ToRemove

        //private static void RegenerateTestScript()
        //{
        //    string testScriptPath = GetFullPath(@"Assets\MUN\Editor\MUN Assistant\Recommendations\CodeAnalyzisExample.cs");
        //    string testScriptPath2 = GetFullPath(@"Assets\MUN\Editor\MUN Assistant\Recommendations\CodeAnalyzisExample2.cs");
        //    string testScriptPath3 = GetFullPath(@"Assets\MUN\Editor\MUN Assistant\Recommendations\CodeAnalyzisExample3.cs");
        //    string sourceScriptPath = GetFullPath(@"Assets\MUN\Editor\MUN Assistant\Recommendations\CodeAnalyzisExampleSource.txt");
        //    string sourceScriptPath2 = GetFullPath(@"Assets\MUN\Editor\MUN Assistant\Recommendations\CodeAnalyzisExampleSource2.txt");
        //    string sourceScriptPath3 = GetFullPath(@"Assets\MUN\Editor\MUN Assistant\Recommendations\CodeAnalyzisExampleSource3.txt");

        //    string sourceCode = File.ReadAllText(sourceScriptPath);
        //    File.WriteAllText(testScriptPath, sourceCode);
        //    sourceCode = File.ReadAllText(sourceScriptPath2);
        //    File.WriteAllText(testScriptPath2, sourceCode);
        //    sourceCode = File.ReadAllText(sourceScriptPath3);
        //    File.WriteAllText(testScriptPath3, sourceCode);
        //}

        private static string GetFullPath(string path)
        {
            var appPathMembers = Application.dataPath.Split('/', '\\');
            // Remove last element - Assets, because next array contains Assets folder
            appPathMembers = appPathMembers.Take(appPathMembers.Length - 1).ToArray();
            var pathMembers = path.Split('/', '\\');
            return Path.Combine(String.Join("/", appPathMembers), String.Join("/", pathMembers));
        }

        #endregion ToRemove

        /// <summary>
        /// Show filters popup.
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="onNoneSwitch"></param>
        /// <param name="onLogicalSwitch"></param>
        /// <param name="onMissingThrow"></param>
        /// <param name="onControlFlowManagement"></param>
        /// <param name="onIncorrectAssigment"></param>
        /// <param name="onInitialization"></param>
        /// <param name="onVr"></param>
        /// <param name="recommendations"></param>
        private static void ShowFilteringPopup(ref RefBool[] filters, Action onNoneSwitch, Action onLogicalSwitch,
            Action onMissingThrow, Action onControlFlowManagement, Action onIncorrectAssigment, Action onInitialization,
            Action onVr, List<Recommendation> recommendations)
        {
            GenericMenu menu = new GenericMenu();

            int noneRecommendationsCount =
                recommendations.Sum(x => x.Fixes
                    .Where(x => !x.Ignored && !x.Applied)
                    .Count(f => f.Type == CodeError.None));
            menu.AddMenuItem($"None ({noneRecommendationsCount})", ref filters, 0, onNoneSwitch);

            int logicalRecoCount =
                recommendations.Sum(x => x.Fixes
                    .Where(x => !x.Ignored && !x.Applied)
                    .Count(f => f.Type == CodeError.LogicalError));
            menu.AddMenuItem($"Logical ({logicalRecoCount})", ref filters, 1, onLogicalSwitch);

            int missThrRecoCount =
                recommendations.Sum(x => x.Fixes
                    .Where(x => !x.Ignored && !x.Applied)
                    .Count(f => f.Type == CodeError.MissingThrowError));
            menu.AddMenuItem($"Missing Throw ({missThrRecoCount})", ref filters, 2, onMissingThrow);

            int ctrlFlowCount =
                recommendations.Sum(x => x.Fixes
                    .Where(x => !x.Ignored && !x.Applied)
                    .Count(f => f.Type == CodeError.ControlFlowManagementError));
            menu.AddMenuItem($"Control Flow ({ctrlFlowCount})", ref filters, 3, onControlFlowManagement);

            int incorAssigCount =
                recommendations.Sum(x => x.Fixes
                    .Where(x => !x.Ignored && !x.Applied)
                    .Count(f => f.Type == CodeError.IncorrectAssignmentError));
            menu.AddMenuItem($"Incorrect Assigment ({incorAssigCount})", ref filters, 4, onIncorrectAssigment);

            int initsCount = recommendations.Sum(x => x.Fixes
                .Where(x => !x.Ignored && !x.Applied)
                .Count(f => f.Type == CodeError.InitializationError));
            menu.AddMenuItem($"Initialization ({initsCount})", ref filters, 5, onInitialization);

            int vrsCount = recommendations.Sum(x => x.Fixes
                .Where(x => !x.Ignored && !x.Applied)
                .Count(f => f.Type == CodeError.VRError));
            menu.AddMenuItem($"Vr ({vrsCount})", ref filters, 6, onVr);

            menu.ShowAsContext();
        }

        /// <summary>
        /// Add menu item to filters popup.
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="path"></param>
        /// <param name="filters"></param>
        /// <param name="index"></param>
        /// <param name="onSelected"></param>
        private static void AddMenuItem(this GenericMenu menu, string path, ref RefBool[] filters, int index,
            Action onSelected)
        {
            menu.AddItem(new GUIContent(path), filters[index].Value, () => onSelected?.Invoke());
        }

        /// <summary>
        /// Draw content scroll view.
        /// </summary>
        /// <param name="scrollPosition"></param>
        /// <param name="onDrawContent"></param>
        /// <returns></returns>
        public static Vector2 ContentScrollView(Vector2 scrollPosition, Action onDrawContent)
        {
            var result = // @formatter:off
            EditorGUILayout.BeginScrollView(scrollPosition,
               GUISkins.Margin(new RectOffset(5, 0, 0, 0))); // @formatter:on
            {
                onDrawContent?.Invoke();
            }
            EditorGUILayout.EndScrollView();
            return result;
        }

        /// <summary>
        /// Draw recommendation item.
        /// </summary>
        /// <param name="recommendations"></param>
        /// <param name="recommendationIndex"></param>
        /// <param name="fixIndex"></param>
        /// <param name="scrollViewWidth"></param>
        /// <param name="onIgnore"></param>
        /// <param name="onApply"></param>
        public static void DrawRecommendationItem(List<Recommendation> recommendations, int recommendationIndex,
            int fixIndex, float scrollViewWidth, Action onIgnore, Action onApply)
        {
            EditorGUILayout.BeginVertical();
            {
                RecommendationHeader(
                    scrollViewWidth,
                    recommendations[recommendationIndex].FileName,
                    recommendations[recommendationIndex].Fixes[fixIndex]);
                GUILayout.Space(5);

                // --- Content ---
                EditorGUILayout.BeginHorizontal(GUILayout.Height(
                    GetCodeSectionHeight(recommendations[recommendationIndex].Fixes[fixIndex])));
                {
                    var spacingsWidth = 2 * tableSpacing;

                    string[] codeLines = recommendations[recommendationIndex]
                        .AddBoldTags(fixIndex)
                        .Split('\n');
                    codeLines = codeLines
                        .Skip(recommendations[recommendationIndex].GetStartLineIndex(fixIndex))
                        .Take(recommendations[recommendationIndex].GetLinesCount(fixIndex))
                        .ToArray();

                    AddTagsIfWasBroken(ref codeLines);

                    CodeSection(recommendations, "Code before:", spacingsWidth, codeLines, recommendationIndex,
                        fixIndex, scrollViewWidth);
                    GUILayout.Space(tableSpacing);

                    string[] afterFixCodeLines = recommendations[recommendationIndex].Fix(fixIndex, true)
                        .Split('\n');
                    afterFixCodeLines = afterFixCodeLines
                        .Skip(recommendations[recommendationIndex].GetStartLineIndex(fixIndex))
                        .Take(recommendations[recommendationIndex].GetFixLinesCount(fixIndex))
                        .ToArray();

                    AddTagsIfWasBroken(ref afterFixCodeLines);

                    CodeSection(recommendations, "Code after:", spacingsWidth, afterFixCodeLines,
                        recommendationIndex, fixIndex, scrollViewWidth, true);
                    GUILayout.Space(tableSpacing);

                    RecommendationActions(recommendations, recommendationIndex, fixIndex, scrollViewWidth,
                        spacingsWidth, onIgnore, onApply);
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// Draw recommendation actions, like Ignore action and Apply action.
        /// </summary>
        /// <param name="recommendations"></param>
        /// <param name="recommendationIndex"></param>
        /// <param name="fixIndex"></param>
        /// <param name="scrollViewWidth"></param>
        /// <param name="spacingsWidth"></param>
        /// <param name="onIgnore"></param>
        /// <param name="onApply"></param>
        private static void RecommendationActions(List<Recommendation> recommendations, int recommendationIndex,
            int fixIndex, float scrollViewWidth, float spacingsWidth, Action onIgnore, Action onApply)
        {
            EditorGUILayout.BeginVertical(
                GUISkins.GroupOffset(_margin0, _padding0),
                GUILayout.Width(scrollViewWidth * 1 / 8 - spacingsWidth),
                GUILayout.Height(GetCodeSectionHeight(recommendations[recommendationIndex].Fixes[fixIndex])));
            {
                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Ignore"))
                    onIgnore?.Invoke();

                if (GUILayout.Button("Apply"))
                    onApply?.Invoke();
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// Draw recommendation header with base information about recommendation.
        /// </summary>
        /// <param name="scrollViewWidth"></param>
        /// <param name="fileName"></param>
        /// <param name="fix"></param>
        private static void RecommendationHeader(float scrollViewWidth, string fileName, FixView fix)
        {
            EditorGUILayout.BeginHorizontal();
            {
                var spacingsWidth = 2 * tableSpacing;

                GUILayout.Box(
                    fix.Description,
                    GUISkins.GroupOffsetWithTextClipping(_margin0,
                        new RectOffset(2, 2, 2, 2)),
                    GUILayout.Width(scrollViewWidth * 2 / 4 - spacingsWidth),
                    GUILayout.Height(rowHeigth));

                GUILayout.Space(tableSpacing);

                GUILayout.Box(
                    fileName,
                    GUISkins.GroupOffsetWithTextClipping(_margin0,
                        new RectOffset(2, 2, 2, 2)),
                    GUILayout.Width(scrollViewWidth * 1 / 4 - spacingsWidth),
                    GUILayout.Height(rowHeigth));

                GUILayout.Space(tableSpacing);

                GUILayout.Box(
                    Enum.GetName(typeof(CodeError), fix.Type),
                    GUISkins.GroupOffsetWithTextClipping(_margin0,
                        new RectOffset(2, 2, 2, 2)),
                    GUILayout.Width(scrollViewWidth * 1 / 4 - spacingsWidth),
                    GUILayout.Height(rowHeigth));
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// Draw the code section for code snippets.
        /// </summary>
        /// <param name="recommendations"></param>
        /// <param name="label"></param>
        /// <param name="spacingsWidth"></param>
        /// <param name="codeLines"></param>
        /// <param name="recommendationIndex"></param>
        /// <param name="fixIndex"></param>
        /// <param name="scrollViewWidth"></param>
        /// <param name="codeWillBeRemoved"></param>
        private static void CodeSection(List<Recommendation> recommendations, string label, float spacingsWidth,
            string[] codeLines, int recommendationIndex, int fixIndex, float scrollViewWidth,
            bool codeWillBeRemoved = false)
        {
            float width = scrollViewWidth * 7 / 8 / 2 - spacingsWidth;

            EditorGUILayout.BeginVertical(
                GUISkins.GroupOffset(_margin0, _padding0),
                GUILayout.Width(width),
                GUILayout.Height(GetCodeSectionHeight(recommendations[recommendationIndex].Fixes[fixIndex])));
            {
                EditorGUILayout.BeginHorizontal(
                    GUISkins.GroupOffset(_margin0, _padding0),
                    GUILayout.ExpandHeight(true),
                    GUILayout.ExpandWidth(true));
                {
                    EditorGUILayout.BeginVertical(
                        GUILayout.ExpandHeight(true),
                        GUILayout.ExpandWidth(true));
                    {
                        GUILayout.Label(label);

                        if (recommendations[recommendationIndex].Fixes[fixIndex].Text.Length == 0 && codeWillBeRemoved)
                        {
                            GUILayout.Label("\t<b>The code will be removed.</b>", GUISkins.CodeText());
                        }
                        else
                        {
                            // If long expanded code
                            if (codeLines.Length > 2 &&
                                recommendations[recommendationIndex].Fixes[fixIndex].IsExpanded)
                            {
                                // @formatter:off
                                recommendations[recommendationIndex].Fixes[fixIndex].ScrollViewPosition =
                                EditorGUILayout.BeginScrollView(
                                    recommendations[recommendationIndex].Fixes[fixIndex].ScrollViewPosition,
                                    GUILayout.Height(ItemLongContentHeight - 22)); // @formatter:on
                                {
                                    int i = recommendations[recommendationIndex].GetStartLineIndex(fixIndex) + 1;
                                    foreach (var line in codeLines)
                                    {
                                        GUILayout.Label($" {i} | {line}", GUISkins.CodeText());
                                        i++;
                                    }
                                }
                                EditorGUILayout.EndScrollView();
                            }

                            // If long collapsed code
                            else if (codeLines.Length > 2 &&
                                     !recommendations[recommendationIndex].Fixes[fixIndex]
                                         .IsExpanded)
                            {
                                // @formatter:off
                                recommendations[recommendationIndex].Fixes[fixIndex].ScrollViewPosition =
                                EditorGUILayout.BeginScrollView(
                                    recommendations[recommendationIndex].Fixes[fixIndex].ScrollViewPosition,
                                    GUIStyle.none, GUIStyle.none); // @formatter:on
                                {
                                    int counter = 1;
                                    int i = recommendations[recommendationIndex].GetStartLineIndex(fixIndex) + 1;
                                    foreach (var line in codeLines)
                                    {
                                        if (counter == 2) // If last line to show, showing fragment code
                                        {
                                            GUILayout.Label($" {i} | {line} ...",
                                                GUISkins.CodeText(width - 20));
                                            break;
                                        }

                                        GUILayout.Label($" {i} | {line}", GUISkins.CodeText(width - 20));

                                        counter++;
                                        i++;
                                    }
                                }
                                EditorGUILayout.EndScrollView();
                            }
                            else // If short code
                            {
                                // @formatter:off
                                recommendations[recommendationIndex].Fixes[fixIndex].ScrollViewPosition =
                                EditorGUILayout.BeginScrollView(
                                    recommendations[recommendationIndex].Fixes[fixIndex].ScrollViewPosition,
                                    GUIStyle.none, GUIStyle.none); // @formatter:on
                                {
                                    int i = recommendations[recommendationIndex].GetStartLineIndex(fixIndex) + 1;
                                    foreach (var line in codeLines)
                                    {
                                        GUILayout.Label($" {i} | {line}", GUISkins.CodeText(width - 20));
                                        i++;
                                    }
                                }
                                EditorGUILayout.EndScrollView();
                            }
                        }
                    }
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginVertical(
                        GUILayout.Height(
                            GetCodeSectionHeight(recommendations[recommendationIndex].Fixes[fixIndex])),
                        GUILayout.Width(20)
                    );
                    {
                        GUILayout.FlexibleSpace();
                        if (codeLines.Length > 2)
                            if (GUILayout.Button((recommendations[recommendationIndex].Fixes[fixIndex].IsExpanded
                                        ? "▲"
                                        : "▼"),
                                    GUISkins.ExpandButton()))
                            {
                                recommendations[recommendationIndex].Fixes[fixIndex].IsExpanded
                                    = !recommendations[recommendationIndex].Fixes[fixIndex].IsExpanded;
                            }
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// When code is splitting into lines that process can break highlight tags.
        /// Method repairs tags.
        /// </summary>
        /// <param name="codeLines"></param>
        private static void AddTagsIfWasBroken(ref string[] codeLines)
        {
            bool collectLinesToBold = false;
            List<int> lines = new List<int>();
            for (int i = 0; i < codeLines.Length; i++)
            {
                if (collectLinesToBold)
                {
                    lines.Add(i);
                }

                if (codeLines[i].Contains("<b>")
                   && !codeLines[i].Contains("</b>"))
                {
                    codeLines[i] = $"{codeLines[i]}</b>";
                    collectLinesToBold = true;
                }
                else if (codeLines[i].Contains("</b>")
                         && !codeLines[i].Contains("<b>"))
                {
                    codeLines[i] = $"<b>{codeLines[i]}";
                    collectLinesToBold = false;
                    lines.Remove(i);
                }
            }

            foreach (int lineIndex in lines)
                codeLines[lineIndex] = $"<b>{codeLines[lineIndex]}</b>";
        }

        /// <summary>
        /// Get code section height
        /// </summary>
        /// <param name="fixView"></param>
        /// <returns></returns>
        private static float GetCodeSectionHeight(FixView fixView)
        {
            return fixView.IsExpanded ? ItemLongContentHeight : ItemShortContentHeight;
        }
    }
}