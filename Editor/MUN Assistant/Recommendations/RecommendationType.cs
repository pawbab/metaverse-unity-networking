namespace MUN.Editor.MUN_Assistant.Recommendations
{
    public enum RecommendationType
    {
        Syntax,
        Architecture
    }
}