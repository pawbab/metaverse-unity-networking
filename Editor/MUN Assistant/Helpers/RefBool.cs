namespace MUN.Editor.MUN_Assistant.Helpers
{
    /// <summary>
    /// Wraps bool type into object in order keeping value as reference.
    /// </summary>
    public class RefBool
    {
        public bool Value;
    }
}