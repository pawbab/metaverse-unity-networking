﻿using System;
using System.Threading.Tasks;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private abstract partial class SendRequest
        {
            public SendRequest(Func<string, string, object, string, Task<RequestHandler>> sendRequest)
            {
                Request = sendRequest;
            }

            /// <summary>
            /// URL, Method, Object, Token, Result
            /// </summary>
            /// 
            protected Func<string, string, object, string, Task<RequestHandler>> Request { get; }
        }
    }
}
