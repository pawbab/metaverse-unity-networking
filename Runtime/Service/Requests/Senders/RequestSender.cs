﻿using System;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        /// <summary>
        /// True if MUN is waiting for a response from the servivce
        /// </summary>
        public static bool IsSending { get; private set; }

        private static class RequestSender
        {
            internal static async Task<RequestHandler> SendRequestAsync(string url, string method, object data, string token)
            {
                IsSending = true;
                UnityWebRequest request = new UnityWebRequest(url, method);
                request.SetRequestHeader("Content-Type", "application/json");
                request.SetRequestHeader("Accept", "application/json");
                request.certificateHandler = new CertificateWhore();
                request.downloadHandler = new DownloadHandlerBuffer();

                byte[] jsonBytes = GetJsonBytes(data);
                if (jsonBytes?.Length > 0)
                    request.uploadHandler = new UploadHandlerRaw(jsonBytes);

                if (!string.IsNullOrEmpty(token))
                    request.SetRequestHeader("Authorization", $"Bearer {token}");

                RequestHandler result = await SendRequestAsync(request);
                request.Dispose();
                jsonBytes = null;
                GC.Collect();

                IsSending = false;
                return result;
            }

            private static async Task<RequestHandler> SendRequestAsync(UnityWebRequest request)
            {
                try
                {
                    UnityWebRequestAsyncOperation operation = request.SendWebRequest();

                    if (!UnityEngine.Application.isPlaying)
                        while (!operation.isDone) await Task.Yield();
                    else
                        while (!operation.isDone) Task.Delay(100).GetAwaiter().GetResult();


                    DownloadHandler downloadHandler = request.downloadHandler;
                    RequestHandler requestHandler = new RequestHandler()
                    {
                        isSuccess = request.result == UnityWebRequest.Result.Success,
                        error = request.error,
                        text = downloadHandler.text
                    };

                    return requestHandler;
                }
                catch (Exception ex)
                {
                    MUNLogs.ShowException(ex);
                    return default;
                }
            }

            private static byte[] GetJsonBytes(object data)
            {
                byte[] jsonBytes = new byte[0];

                if (data != null)
                {
                    string json = JsonUtility.ToJson(data);
                    jsonBytes = Encoding.UTF8.GetBytes(json);
                }

                return jsonBytes;
            }

            private class CertificateWhore : CertificateHandler
            {
                protected override bool ValidateCertificate(byte[] certificateData)
                {
                    return true;
                }
            }
        }
    }
}
