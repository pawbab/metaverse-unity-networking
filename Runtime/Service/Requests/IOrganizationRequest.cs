﻿using System;
using System.Threading.Tasks;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        /// <summary>
        /// Interface contains CRUD functions to exchange information about organizations between the service and the client 
        /// </summary>
        public interface IOrganizationRequest
        {
            /// <summary>
            /// Array of organizations to which the currently logged in user belongs
            /// </summary>
            IOrganization[] Organizations { get; }
            /// <summary>
            /// Sending a request to the website for an attempt to get an array of the organizations of the currently logged on user
            /// </summary>
            /// <returns><see cref="Organizations"/></returns>
            Task<IOrganization[]> GetAll();
            /// <summary>
            /// Sending a request to the website for a attempt to load members of the indicated organization
            /// </summary>
            /// <param name="organization">Load members for this organizations</param>
            /// <returns><see cref="IOrganization"/></returns>
            Task<IOrganization> LoadOrganizationMembers(IOrganization organization);
            /// <summary>
            /// Sending a request to the website for a attempt to create new organization
            /// </summary>
            /// <param name="organizationName">Unique organization name</param>
            /// <returns>True if the organization is successful otherwise false</returns>
            Task<bool> Create(string organizationName, Action<IResultErrorHandle> onError);
            /// <summary>
            /// Sending a request to the website for a attempt to delete your own organization
            /// </summary>
            /// <param name="organizationId">Organization Id that you are an owner</param>
            /// <returns>True if the organization is successful otherwise false</returns>
            Task<bool> Delete(int organizationId, Action<IResultErrorHandle> onError);
            /// <summary>
            /// Sending a request to the website for a attempt to add a member to your own organization
            /// </summary>
            /// <param name="username">Username of a potential new member of the organization</param>
            /// <param name="organizationId">Organization Id that you are an owner</param>
            /// <returns>True if the organization is successful otherwise false</returns>
            Task<bool> AddMemberToOrganization(string username, int organizationId, Action<IResultErrorHandle> onError);
            /// <summary>
            /// Sending a request to the website for a attempt to remove a member from your own organization
            /// </summary>
            /// <param name="username">The name of the user you want to remove from your organization</param>
            /// <param name="organizationId">Organization Id that you are an owner</param>
            /// <returns>True if the organization is successful otherwise false</returns>
            Task<bool> RemoveMemberFromOrganization(string username, int organizationId);
            /// <summary>
            /// Sending a request to the website for a attempt to remove yourself from organization that you are member
            /// </summary>
            /// <param name="organizationId">Organization Id that you are a member (not owner)</param>
            /// <returns>True if the organization is successful otherwise false</returns>
            Task<bool> RemoveFromOrganization(int organizationId, Action<IResultErrorHandle> onError);
            /// <summary>
            /// Clear array of <see cref="Organizations"/>
            /// </summary>
            void ClearOrganizations();
        }
    }
}
