﻿using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using UnityEngine.Networking;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private class RegionRequest : SendRequest, IRegionRequest
        {
            private string[] regions;

            public string Controller { get; }


            public RegionRequest(string Controller, Func<string, string, object, string, Task<RequestHandler>> sendRequest) : base(sendRequest)
            {
                this.Controller = Controller;
                this.regions = null;
            }

            public async Task<string[]> GetAll()
            {
                if (regions == null)
                {
                    string url = string.Concat(URL, Controller);
                    string method = UnityWebRequest.kHttpVerbGET;

                    RequestHandler result = await Request.Invoke(url, method, null, string.Empty);

                    if (result.isSuccess)
                    {
                        MUNLogs.ShowLog("Get Regions Success!".WithColorSuccess());
                        regions = JsonRoot<string[]>.FromJson(result.text).root;
                    }
                    else
                    {
                        ResultErrorHandle errorResult = JsonConvert.DeserializeObject<ResultErrorHandle>(result.text);
                        MUNLogs.ShowError(errorResult.ToString());
                    }
                }
                return regions;
            }
        }
    }
}
