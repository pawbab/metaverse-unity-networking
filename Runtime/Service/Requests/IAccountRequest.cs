﻿using System;
using System.Threading.Tasks;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        /// <summary>
        /// Interface contains CRUD functions to exchange information about account between the service and the client 
        /// </summary>
        public interface IAccountRequest
        {
            /// <summary>
            /// A string of characters used to authorize the user on the service, in order to fulfill some queries that require authorization 
            /// (e.g. editing the organization, those that are created by me) 
            /// </summary>
            string Token { get; }

            /// <summary>
            /// Last known logged username into service in this project. Username is saved in PlayerPrefs
            /// </summary>
            string Username { get; }

            /// <summary>
            /// Current logged username. Name, email and role (user or admin).
            /// Only to display basic information about the logged in user 
            /// </summary>
            IGetUser User { get; }

            /// <summary>
            /// Sending a request to the website for an attempt to log in using the login and password 
            /// </summary>
            /// <param name="login">user login</param>
            /// <param name="password">user password</param>
            /// <returns>Returns authorization token</returns>
            Task<string> Login(string login, string password, Action<IResultErrorHandle> onErrors);
            /// <summary>
            /// Sending a request to the website for an attempt to get details of current logged user
            /// </summary>
            Task Get(Action<IResultErrorHandle> onErrors);

            /// <summary>
            /// Sending a request to the website for a attempt to update details of current logged user
            /// </summary>
            /// <param name="username">New (unique) username</param>
            /// <param name="email">New (unique) address email</param>
            /// <param name="currentPassword">Current password to confirm update</param>
            /// <param name="password">New password</param>
            /// <param name="confirmPassword">New password (confirm)</param>
            Task Update(string username, string email, string currentPassword, string password, string confirmPassword, Action<IResultErrorHandle> onErrors);

            Task<bool> Register(string username, string email, string password, string confirmPassword, Action<IResultErrorHandle> onErrors);

            /// <summary>
            /// Logout current logged user
            /// </summary>
            void Logout();
        }
    }
}
