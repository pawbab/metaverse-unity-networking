﻿using System;
using System.Threading.Tasks;
using UnityEngine.Networking;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class AccountRequest : SendRequest, IAccountRequest
        {

            public string Token
            {
                get => PlayerPrefsConfig.UserToken;
                private set => PlayerPrefsConfig.UserToken = value;
            }
            public string Username
            {
                get => PlayerPrefsConfig.UserName;
                private set => PlayerPrefsConfig.UserName = value;
            }
            public IGetUser User { get; private set; }
            private string Controller { get; }

            public AccountRequest(string controller, Func<string, string, object, string, Task<RequestHandler>> sendRequest) : base(sendRequest)
            {
                this.Controller = controller;
            }

            public async Task<string> Login(string login, string password, Action<IResultErrorHandle> onErrors)
            {
                LoginUser user = new LoginUser()
                {
                    Username = login,
                    Password = password
                };

                string url = string.Concat(URL, Controller, "login");
                string method = UnityWebRequest.kHttpVerbPOST;

                RequestHandler result = await Request.Invoke(url, method, user, string.Empty);

                if (result.isSuccess)
                {
                    Token = result.text;
                    await Get(null);
                    Username = User.Username;
                    MUNLogs.ShowLog("Login Success!".WithColorSuccess());
                }
                else
                {
                    Token = string.Empty;
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onErrors?.Invoke(errorResult);
                }

                return Token;
            }

            public async Task Get(Action<IResultErrorHandle> onErrors)
            {
                string url = string.Concat(URL, Controller);
                string method = UnityWebRequest.kHttpVerbGET;

                RequestHandler result = await Request.Invoke(url, method, null, Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("GetUser Success!".WithColorSuccess());
                    User = JsonRoot<GetUser>.FromJson(result.text).root;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());
                    Logout();

                    onErrors?.Invoke(errorResult);
                }
            }

            public async Task Update(
                string username,
                string email,
                string currentPassword,
                string password,
                string confirmPassword,
                Action<IResultErrorHandle> onErrors)
            {
                string url = string.Concat(URL, Controller);
                string method = UnityWebRequest.kHttpVerbPUT;
                UpdateUser updateUser = new UpdateUser(username, email, currentPassword, password, confirmPassword);

                RequestHandler result = await Request.Invoke(url, method, updateUser, Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Update User Success!".WithColorSuccess());
                    User = new GetUser()
                    {
                        email = updateUser.email,
                        username = updateUser.username,
                        role = User.Role
                    };

                    Username = User.Username;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onErrors?.Invoke(errorResult);
                }
            }

            public void Logout()
            {
                Token = string.Empty;
                User = null;
                Organization.ClearOrganizations();
                Application.ClearApplications();
            }

            public async Task<bool> Register(string username, string email, string password, string confirmPassword, Action<IResultErrorHandle> onErrors)
            {
                RegisterUser registerUser = new RegisterUser()
                {
                    Username = username,
                    Email = email,
                    Password = password,
                    ConfirmPassword = confirmPassword
                };

                string url = string.Concat(URL, Controller, "register");
                string method = UnityWebRequest.kHttpVerbPOST;

                RequestHandler result = await Request.Invoke(url, method, registerUser, string.Empty);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Register Success!".WithColorSuccess());
                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onErrors?.Invoke(errorResult);
                }

                return false;
            }
        }
    }
}
