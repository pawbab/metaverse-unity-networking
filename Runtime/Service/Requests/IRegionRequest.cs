﻿using System.Threading.Tasks;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        public interface IRegionRequest
        {
            /// <summary>
            /// Sending a request to the website for a attempt to get all available regions for Metaverse Servers
            /// </summary>
            /// <returns>All available regions</returns>
            Task<string[]> GetAll();
        }
    }
}
