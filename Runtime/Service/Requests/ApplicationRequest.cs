﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private partial class ApplicationRequest : SendRequest, IApplicationRequest
        {
            private string Controller { get; }

            public IApplication[] Applications { get; private set; }

            public ApplicationRequest(string Controller, Func<string, string, object, string, Task<RequestHandler>> sendRequest) : base(sendRequest)
            {
                this.Controller = Controller;
            }

            public async Task<bool> Create(CreateApplication createApplication, Action<IResultErrorHandle> onErrors)
            {
                string url = string.Concat(URL, Controller, "create");
                string method = UnityWebRequest.kHttpVerbPOST;

                RequestHandler result = await Request.Invoke(url, method, createApplication, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Create Application Success!".WithColorSuccess());
                    IApplication application = JsonRoot<GetApplication>.FromJson(result.text).root;
                    InsertApplicationToArray(application);

                    return true;
                }

                ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                MUNLogs.ShowError(errorResult.ToString());

                onErrors?.Invoke(errorResult);
                return false;
            }

            public async Task<bool> Delete(string appId, Action<IResultErrorHandle> onErrors)
            {
                string url = string.Concat(URL, Controller, $"delete/{appId}");
                string method = UnityWebRequest.kHttpVerbDELETE;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Delete Application Success!".WithColorSuccess());
                    IApplication applicationToDelete = Applications.First(x => x.AppId == appId);
                    DeleteApplicationFromArray(applicationToDelete);

                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());

                    onErrors?.Invoke(errorResult);
                    return false;
                }
            }

            public async Task<IEnumerable<IApplication>> GetAll(int organizationId)
            {
                string url = string.Concat(URL, Controller, $"get-all/{organizationId}");
                string method = UnityWebRequest.kHttpVerbGET;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("GetAll Application Success!".WithColorSuccess());
                    Applications = JsonRoot<GetApplication[]>.FromJson(result.text).root;

                    PingApplications(Applications);
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());
                    Applications = null;
                }

                return Applications;
            }

            public async Task<IApplication> Get(string appId)
            {
                if (string.IsNullOrEmpty(appId)) return null;

                string url = string.Concat(URL, Controller, $"get/{appId}");
                string method = UnityWebRequest.kHttpVerbGET;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);
                IApplication application = null;

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Get Application Success!".WithColorSuccess());
                    application = JsonRoot<GetApplication>.FromJson(result.text).root;
                    PingApplication(application);
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());
                }

                return application;
            }

            public async Task<bool> Update(UpdateApplication updateApplication, Action<IResultErrorHandle> onErrors)
            {
                string url = string.Concat(URL, Controller, $"update");
                string method = UnityWebRequest.kHttpVerbPUT;

                RequestHandler result = await Request.Invoke(url, method, updateApplication, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Update Application Success!".WithColorSuccess());
                    IApplication application = JsonRoot<GetApplication>.FromJson(result.text).root;

                    int updatedAppIndex = Application.Applications.ToList().FindIndex(x => x.AppId == updateApplication.appId);
                    if (updatedAppIndex != -1)
                    {
                        Application.Applications[updatedAppIndex] = application;
                    }

                    return true;
                }
                else
                {
                    ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                    MUNLogs.ShowError(errorResult.ToString());
                    onErrors?.Invoke(errorResult);

                    return false;
                }
            }

            public async Task<bool> Test(string appId, Action<IResultErrorHandle> onErrors)
            {
                if (string.IsNullOrEmpty(appId)) return false;

                string url = string.Concat(URL, Controller, $"test/{appId}");
                string method = UnityWebRequest.kHttpVerbGET;

                RequestHandler result = await Request.Invoke(url, method, null, Account.Token);

                if (result.isSuccess)
                {
                    MUNLogs.ShowLog("Test Application Success!".WithColorSuccess());
                    return true;
                }
  
                ResultErrorHandle errorResult = ResultErrorHandle.FromRequestHandler(result);
                MUNLogs.ShowError(errorResult.ToString());

                onErrors?.Invoke(errorResult);
                return false;


            }
            public void ClearApplications()
            {
                Applications = null;
            }

            private void InsertApplicationToArray(IApplication application)
            {
                List<IApplication> appsList = Applications.ToList();
                appsList.Add(application);
                Applications = appsList.ToArray();
            }

            private void DeleteApplicationFromArray(IApplication application)
            {
                List<IApplication> appsList = Applications.ToList();
                appsList.Remove(application);
                Applications = appsList.ToArray();
            }

            private void PingApplications(IEnumerable<IApplication> applications)
            {
                foreach (IApplication application in applications)
                {
                    PingApplication(application);
                }
            }

            private void PingApplication(IApplication application)
            {
                foreach (IServerInfo serverInfo in application.ServerInfos)
                {
                    string ip = serverInfo.IpPort.Split(':')[0];
                    long? ping = PingServer.Ping(ip);
                    if (ping.HasValue) serverInfo.Ping = ping.Value;
                }
            }

        }
    }
}
