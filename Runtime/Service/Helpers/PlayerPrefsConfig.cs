﻿using UnityEngine;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private class PlayerPrefsConfig
        {
            private const string USER_TOKEN = "user-token";
            private const string USER_NAME = "user-name";

            internal static string UserToken
            {
                get => GetConfigString(USER_TOKEN, string.Empty);
                set => SetConfig(USER_TOKEN, value.Replace("\"", ""));
            }

            internal static string UserName
            {
                get => GetConfigString(USER_NAME, string.Empty);
                set => SetConfig(USER_NAME, value);
            }

            #region Functions

            private static bool GetConfigBool(string name, bool defaultValue = false)
            {
                if (PlayerPrefs.HasKey(name))
                {
                    string getPrefs = PlayerPrefs.GetString(name);
                    if (bool.TryParse(getPrefs, out bool boolValue)) return boolValue;
                }

                return defaultValue;
            }

            private static int GetConfigInt(string name, int defaultValue = 0)
            {
                if (PlayerPrefs.HasKey(name))
                {
                    return PlayerPrefs.GetInt(name);
                }

                return defaultValue;
            }

            private static float GetConfigFloat(string name, float defaultValue = 0)
            {
                if (PlayerPrefs.HasKey(name))
                {
                    return PlayerPrefs.GetFloat(name);
                }

                return defaultValue;
            }

            private static string GetConfigString(string name, string defaultValue = "")
            {
                if (PlayerPrefs.HasKey(name))
                {
                    return PlayerPrefs.GetString(name);
                }

                return defaultValue;
            }

            private static T GetConfigEnum<T>(string name, T defaultValue) where T : struct
            {
                if (PlayerPrefs.HasKey(name))
                {
                    string prefsValue = PlayerPrefs.GetString(name);
                    if (System.Enum.TryParse(prefsValue, true, out T enumValue)) return enumValue;
                }

                return defaultValue;
            }

            private static void SetConfig(string name, bool value)
            {
                PlayerPrefs.SetString(name, value.ToString());
            }

            private static void SetConfig(string name, string value)
            {
                PlayerPrefs.SetString(name, value);
            }

            private static void SetConfig(string name, int value)
            {
                PlayerPrefs.SetInt(name, value);
            }

            private static void SetConfig(string name, float value)
            {
                PlayerPrefs.SetFloat(name, value);
            }

            private static void SetConfig(string name, System.Enum value)
            {
                PlayerPrefs.SetString(name, value.ToString());
            }
            #endregion
        }
    }


}
