﻿using System;
using UnityEngine;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private class JsonRoot<T>
        {
            public T root;

            public static JsonRoot<T> FromJson(string json)
            {
                try
                {
                    return JsonUtility.FromJson<JsonRoot<T>>("{\"root\":" + json + "}");
                }
                catch (ArgumentException e)
                {
                    MUNLogs.ShowException(e);
                    return default;
                }
            }
        }
    }
}
