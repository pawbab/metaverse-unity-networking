using System;
using System.Net.NetworkInformation;
using Ping = System.Net.NetworkInformation.Ping;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private static class PingServer
        {
            public static long? Ping(string ip)
            {
                try
                {
                    using (Ping ping = new Ping())
                    {
                        PingReply reply = ping.SendPingAsync(ip).GetAwaiter().GetResult(); ;

                        if (reply.Status == IPStatus.Success)
                            return reply.RoundtripTime;

                        throw new Exception();
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}