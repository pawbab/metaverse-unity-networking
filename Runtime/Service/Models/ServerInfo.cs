﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        [System.Serializable]
        internal class ServerInfo : IServerInfo
        {
            public string region;
            public string ipPort;
            public long ping;

            public string Region => region;
            public string IpPort => ipPort;
            public long Ping { get => ping; set => ping = value; }
        }
    }
}
