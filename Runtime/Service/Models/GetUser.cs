﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class AccountRequest
        {
            [System.Serializable]
            private class GetUser : IGetUser
            {
                public string username;
                public string email;
                public string role;

                public string Username => username;
                public string Email => email;
                public string Role => role;
            }
        }
    }
}
