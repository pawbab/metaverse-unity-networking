﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        [System.Serializable]
        private class ResultErrorHandle : IResultErrorHandle
        {
            public string Title { get; set; }
            public int Status { get; set; }

            [JsonIgnore]
            public Dictionary<string, string> Errors { get; set; }

            public ResultErrorHandle()
            {
                Errors = new Dictionary<string, string>();
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine($" Status: {Status}. <b><color=red>{Title}</color></b>");
                sb.AppendLine("<b>Errors:</b>");

                foreach (KeyValuePair<string, string> error in Errors)
                    sb.AppendLine($"  <color=red>{error.Key}</color> {error.Value}");
                sb.AppendLine("-----------------------");

                return sb.ToString();
            }

            public static ResultErrorHandle FromRequestHandler(RequestHandler requestHandler)
            {
                const string REGEX = "\"(.*?)\"";

                try
                {
                    ResultErrorHandle errorResult = JsonConvert.DeserializeObject<ResultErrorHandle>(requestHandler.text);

                    JObject parsed = JObject.Parse(requestHandler.text);
                    JObject errors = parsed.SelectToken("errors").ToObject<JObject>();

                    foreach (KeyValuePair<string, JToken> error in errors)
                    {
                        string result = Regex.Match(error.Value.ToString(), REGEX).Value;
                        errorResult.Errors.Add(error.Key, result.Substring(1, result.Length - 2));
                    }

                    return errorResult;
                }
                catch (JsonReaderException)
                {
                    return new ResultErrorHandle()
                    {
                        Title = requestHandler.error,
                        Errors =
                        {
                            { "Info", requestHandler.text }
                        }
                    };
                }
            }
        }
    }
}
