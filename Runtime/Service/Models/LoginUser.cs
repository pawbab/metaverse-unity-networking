﻿using System;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        private sealed partial class AccountRequest
        {
            [Serializable]
            private class LoginUser
            {
                public string Username;
                public string Password;
            }


            public class GetUserDto
            {
                public string Username { get; set; }
                public string Email { get; set; }
                public string Role { get; set; }
            }
        }
    }
}
