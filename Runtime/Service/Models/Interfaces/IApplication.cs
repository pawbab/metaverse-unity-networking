﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        /// <summary>
        /// Basic informations about application
        /// </summary>
        public interface IApplication
        {
            /// <summary>
            /// Unique application id
            /// </summary>
            string AppId { get; }
            string Name { get; }
            bool IsActive { get; }
            bool UseVoiceChat { get; }
            uint MaxPlayers { get; }
            byte TickRate { get; }
            string[] Regions { get; }
            IServerInfo[] ServerInfos { get; }
            IServerInfo LastServerInfo { get; }

            /// <summary>
            /// Found best region for this application
            /// </summary>
            /// <returns></returns>
            IServerInfo GetBestRegionOrDefault();
            IServerInfoShort[] GetServerInfos();
            IServerInfo[] GetFullServerInfos();
        }
    }
}
