﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        public interface IGetUser
        {
            string Username { get; }
            string Email { get; }
            string Role { get; }
        }
    }
}
