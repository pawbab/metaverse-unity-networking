﻿namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        public interface IServerInfo : IServerInfoShort
        {
            string IpPort { get; }
        }

        public interface IServerInfoShort
        {
            string Region { get; }
            long Ping { get; set; }
        }
    }
}
