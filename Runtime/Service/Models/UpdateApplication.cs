﻿using System;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {

        [Serializable]
        public class UpdateApplication
        {
            public string appId;
            public string name;
            public uint maxPlayers;
            public bool isActive;
            public bool useVoiceChat;
            public byte tickRate;
            public string[] regions;
        }
    }
}
