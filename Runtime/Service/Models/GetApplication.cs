﻿using System.Linq;

namespace MUN.Service
{
    public static partial class ServiceRequest
    {
        [System.Serializable]
        private class GetApplication : IApplication
        {
            public string appId;
            public string name;
            public uint maxPlayers;
            public bool useVoiceChat;
            public bool isActive;
            public byte tickRate;
            public string[] regions;
            public ServerInfo[] serverInfos;

            public string AppId => appId;
            public string Name => name;
            public uint MaxPlayers => maxPlayers;
            public bool IsActive => isActive;
            public bool UseVoiceChat => useVoiceChat;
            public byte TickRate => tickRate;
            public string[] Regions => regions;
            public IServerInfo[] ServerInfos => serverInfos;
            public IServerInfo LastServerInfo { get; private set; }

            public IServerInfo GetBestRegionOrDefault()
            {
                ServerInfo result = serverInfos.Where(x => x.ping > 0).OrderBy(x => x.ping).FirstOrDefault() ??
                                    serverInfos.FirstOrDefault();

                LastServerInfo = result ?? null;
                return result;
            }

            public IServerInfoShort[] GetServerInfos()
            {
                return ServerInfos;
            }

            public IServerInfo[] GetFullServerInfos()
            {
                return ServerInfos;
            }
        }
    }
}
