using MUN.Client.Extensions;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MUN.Client
{
    /// <summary>
    /// This is a simplified replacement for the Hashtable class that uses Dictionary <object, object> as the base class.
    /// </summary>
    public class LightHashTable : Dictionary<object, object>
    {
        internal static readonly object[] boxedByte;

        public new object this[object key]
        {
            get
            {
                TryGetValue(key, out object value);
                return value;
            }
            set => base[key] = value;
        }

        public object this[byte key]
        {
            get
            {
                TryGetValue(boxedByte[key], out object value);
                return value;
            }
            set => base[boxedByte[key]] = value;
        }

        static LightHashTable()
        {
            boxedByte = new object[256];
            for (int i = 0; i < 256; i++)
            {
                boxedByte[i] = (byte)i;
            }
        }

        public LightHashTable()
        {
        }

        public LightHashTable(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public LightHashTable(int capacity)
            : base(capacity)
        {
        }

        public T TryGetValue<T>(object key)
        {
            object value = this[key];

            if (value == null) return default(T);
            else if (value.GetType() != typeof(byte[])) return (T)value;
            else return ((byte[])value).ToObject<T>();
        }

        public void Add(byte key, object value)
        {
            Add(boxedByte[key], value);
        }

        public void Remove(byte key)
        {
            Remove(boxedByte[key]);
        }

        public bool ContainsKey(byte key)
        {
            return ContainsKey(boxedByte[key]);
        }

        public LightHashTable Clone()
        {
            LightHashTable clone = new LightHashTable();

            foreach (KeyValuePair<object, object> item in this)
            {
                clone.Add(item.Key, item.Value);
            }

            return clone;
        }
    }
}