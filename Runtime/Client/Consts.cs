﻿using MUN.Client.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;
using static MUN.Client.MUNNetwork.MUNClient;
using static MUN.Service.ServiceRequest;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        private static readonly HashSet<WaitForSendRPCTuple> waitForSendRPCs = new HashSet<WaitForSendRPCTuple>();
        private static readonly HashSet<WaitForRecieveRPCTuple> waitForRecieveRPCs = new HashSet<WaitForRecieveRPCTuple>();
        private static readonly HashSet<WaitForCreateNetworkObjectTuple> waitForCreateNetworkObjectTuples = new HashSet<WaitForCreateNetworkObjectTuple>();

        /// <summary>
        /// Determines what will happen to the objects of the player who leaves the room.
        /// </summary>
        private static MUNViewsLogicOnPlayerDisconected OnPlayerDisconectedMUNViewLogic
        {
            get => onPlayerDisconectedMUNViewLogic;
            set
            {
                if (MUNClient.ClientStatus != ClientStatus.Disconnected)
                {
                    MUNLogs.ShowError("OnPlayerDisconectedMUNViewLogic must be set before  connected to MasterServer");
                    return;
                }
                onPlayerDisconectedMUNViewLogic = value;
            }
        }

        /// <summary>
        /// Reference to local client data
        /// </summary>
        /// <remarks>
        /// It facilitates access to the player's own properties and name.
        /// When a client joins a room, these values are synchronized with other clients in the room.
        /// </remarks>
        public static Player LocalPlayer => Client.LocalPlayer;

        /// <summary>
        /// A reference to the room where the <see cref="LocalPlayer"/> is currently located.
        /// In case the local player is not in the room - it returns null
        /// </summary>
        public static Room CurrentRoom => Client.CurrentRoom;
        public static VoiceRoom CurrentVoiceRoom => Client.CurrentVoiceRoom;

        /// <summary>
        /// Simplification in reading and writing the PlayerName of the local player
        /// </summary>
        public static string PlayerName
        {
            get => LocalPlayer.Name;
            set
            {
                if (LocalPlayer is null) return;
                LocalPlayer.Name = value;
            }
        }

        /// <summary>
        /// Simplify the reading if a local player is in the room
        /// </summary>
        public static bool InRoom => MUNClient.ClientStatus == ClientStatus.InRoom || MUNClient.ClientStatus == ClientStatus.ChangingScene;

        /// <summary>
        /// True if scene is currently changing via MUNNetwork
        /// </summary>
        internal static bool IsSceneChanging => MUNSceneManager.IsSceneChanging;

        /// <summary>
        /// Gets the current connection status of the client to the server
        /// </summary>
        public static ClientStatus Status => MUNClient.ClientStatus;
        public static ClientVoiceStatus VoiceStatus => MUNClient.ClientVoiceStatus;

        /// <summary>
        /// Retrieves information about the application created on the site
        /// </summary>
        /// <remarks>
        /// It allows you to verify appid, maximum number of players, tickrate and more...
        /// </remarks>
        public static IApplication Application => Client.Application;

        /// <summary>
        /// Determines what will happen to the objects of the player who leaves the room.
        /// </summary>
        public static MUNViewsLogicOnPlayerDisconected onPlayerDisconectedMUNViewLogic = MUNViewsLogicOnPlayerDisconected.DestroyAllPlayerMUNViews;

        /// <summary>
        /// Gets all available ServerInfos (region and ping) for defined AppId
        /// </summary>
        public static IEnumerable<IServerInfoShort> GetServerInfoShorts()
        {
            return Client.Application.GetServerInfos();
        }

        /// <summary>
        /// Method connecting to the MasterServer. The system should start with a connection to the MasterServer
        /// </summary>
        /// <param name="useBestRegion">If true, the server will try to connect the client to the most favorable region</param>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool ConnectToMaster(bool useBestRegion = true)
        {
            if (!UnityEngine.Application.isPlaying)
            {
                MUNLogs.ShowException(new MethodAccessException("This method is not allowed in EditMode"));
                return false;
            }

            IServerInfo serverInfo = Client.Application.GetBestRegionOrDefault();
            return ConnectToMaster(serverInfo);
        }

        /// <summary>
        /// Method connecting to the MasterServer. The system should start with a connection to the MasterServer
        /// </summary>
        /// <remarks>
        /// You must ensure that the application is launched for your region.
        /// </remarks>
        /// <param name="region">Region you are trying to connect to.</param>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool ConnectToMaster(string region)
        {
            if (!UnityEngine.Application.isPlaying)
            {
                MUNLogs.ShowException(new MethodAccessException("This method is not allowed in EditMode"));
                return false;
            }

            //TODO: Na ten moment zawsze pobiera z najlepszego regionu.
            //      Pomija parametr funkcji
            IServerInfo serverInfo = Client.Application.GetBestRegionOrDefault();
            return ConnectToMaster(serverInfo);
        }

        /// <summary>
        /// Method connecting to the VoiceMasterServer. The system should start with a connection to the VoiceMasterServer
        /// </summary>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool ConnectToVoiceMasterServer()
        {


            if (MUNLoop.IsQuit) return false;
            if (MUNClient.ClientStatus < ClientStatus.ConnectedToMasterServer)
            {
                MUNLogs.ShowError($"ConnectToVoiceMasterServer failed. Required ClientStatus >= ConnectedToMasterServer. Current: {MUNClient.ClientStatus}.".WithColorFailed());
                return false;
            }

            if (MUNNetwork.VoiceStatus >= ClientVoiceStatus.ConnectedToVoiceMasterServer)
            {
                MUNLogs.ShowError($"ConnectToVoiceMasterServer failed. Required VoiceStatus < ConnectedToVoiceMasterServer. Current: {MUNNetwork.VoiceStatus}.".WithColorFailed());
                return false;
            }

            return Client.ConnectToVoiceMaster();
        }

        /// <summary>
        /// Disconnects the client from the MasterServer
        /// </summary>
        /// <remarks>
        /// The client can again try to connect to MasterServer.
        /// </remarks>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool DisconnectFromMasterServer()
        {
            if (MUNClient.ClientStatus == ClientStatus.Disconnected)
            {
                MUNLogs.ShowError("Player is already disconnected.");
                return false;
            }

            Client.Disconnect();

            return true;
        }

        /// <summary>
        /// Creates a new room.
        /// Each room has unique RoomId (short code)
        /// </summary>
        /// <remarks>
        /// After the room is successfully created, the OnRoomCreated callback will be called and then attempted to join that room.
        /// This method can only be called when the client is OnConnectedToMaster.
        /// </remarks>
        /// <param name="roomName">The proper name of the room</param>
        /// <param name="claims">Custom properties of the created room</param>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool CreateRoom(string roomName, Room.RoomClaims claims = null)
        {
            if (MUNClient.ClientStatus != ClientStatus.ConnectedToMasterServer)
            {
                MUNLogs.ShowError($"CreateRoom failed. Required ClientStatus = ConnectedToMasterServer. Current: {MUNClient.ClientStatus}.".WithColorFailed());
                return false;
            }

            CreateRoomInfo createRoomInfo = new CreateRoomInfo();
            createRoomInfo.Name = roomName;
            createRoomInfo.Claims = claims;

            return Client.CreateRoom(createRoomInfo);
        }

        /// <summary>
        /// The method connects the client to any open (and not full) voice room, if available. Otherwise, a new voice room is created.
        /// </summary>
        /// <remarks>
        /// Creating or joining a voice chat room is only possible when the player is in the room.
        /// If a room is created, its name and maximum number of players are equal to the main room Id and <see cref="RoomData.MaxPlayers">MaxPlayers</see>.
        /// </remarks>
        /// <return>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool CreateOrJoinVoiceRoom()
        {
            return CreateOrJoinVoiceRoom(MUNNetwork.CurrentRoom.RoomId, MUNNetwork.CurrentRoom.MaxPlayers);
        }

        /// <summary>
        /// The method connects the client to any open (and not full) voice room, if available. Otherwise, a new voice room is created.
        /// </summary>
        /// <remarks>
        /// Creating or joining a voice chat room is only possible when the player is in the room.
        /// </remarks>
        /// <param name="roomName">Name of VoiceChat room</param>
        /// <param name="maxPlayers">Max players for VoiceChat room</param>
        /// <return>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool CreateOrJoinVoiceRoom(string roomName, byte maxPlayers)
        {
            if (!MUNNetwork.Application.UseVoiceChat) return false;

            if (MUNClient.ClientVoiceStatus != ClientVoiceStatus.ConnectedToVoiceMasterServer)
            {
                MUNLogs.ShowError($"CreateVoiceRoom failed. Required ClientVoiceStatus = ConnectedToVoiceMasterServer. Current: {MUNClient.ClientVoiceStatus}.".WithColorFailed());
                return false;
            }

            if (MUNClient.ClientStatus != ClientStatus.InRoom)
            {
                MUNLogs.ShowError($"CreateVoiceRoom failed. Required ClientStatus = InRoom. Current: {MUNClient.ClientStatus}.".WithColorFailed());
                return false;
            }

            var createVoiceRoom = new CreateVoiceRoomInfo(roomName, maxPlayers);
            return Client.CreateOrJoinVoiceRoom(createVoiceRoom);
        }

        /// <summary>
        /// If you want to manually refresh the room list - you can call this method.
        /// However, it is resource-hungry. The list of rooms is refreshed automatically.
        /// </summary>
        /// <remarks>
        /// Serwer zwraca listę dostępnych pokoi i wywołuje callback OnRoomListUpdated
        /// </remarks>
        public static void UpdateingRoomsList()
        {
            if (CurrentRoom != null || MUNNetwork.Status != ClientStatus.ConnectedToMasterServer)
            {
                MUNLogs.ShowError("UpdateRoomsList available only if client is connected into MasterServer and is not connected to any room yet.");
                return;
            }

            Client.UpdateingRoomsList();
        }

        /// <summary>
        /// The method connects the client to any open (and not full) room, if available. Otherwise, a new room is created with <see cref="Room.RoomClaims"/> set (or default if null)
        /// </summary>
        /// <remarks>
        /// This method can only be called when the client is OnConnectedToMaster.
        /// When creating a room, the OnRoomCreated method is called. After the room is created/client is joined, the OnJoinedRoom method is then called.
        /// A room created with this method is given a name that is equivalent to RoomId.
        /// </remarks>
        /// <param name="claims">Custom settings when creating a room</param>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool JoinOrCreateRandomRoom(Room.RoomClaims claims = null)
        {
            if (MUNClient.ClientStatus != ClientStatus.ConnectedToMasterServer)
            {
                MUNLogs.ShowError($"CreateOrJoinRandomRoom failed. Required ClientStatus = ConnectedToMasterServer. Current: {MUNClient.ClientStatus}.".WithColorFailed());
                return false;
            }

            return Client.JoinOrCreateRandomRoom(claims);
        }

        /// <summary>
        /// Joins a room by unique roomId. 
        /// If success will callback OnJoinedRoom otherwise OnJoinedRoomFailed.
        /// </summary>
        /// <remarks>
        /// This method can only be called when the client is OnConnectedToMaster.
        /// When successful, the client will enter the room.
        /// Joining a room will fail if the room is full, locked, does not exist, or has password.
        /// </remarks>
        /// <param name="roomId">Unique roomId</param>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool JoinRoom(string roomId)
        {
            return JoinRoom(roomId, string.Empty);
        }

        /// <summary>
        /// Joins a room by unique roomId. 
        /// If success will callback OnJoinedRoom otherwise OnJoinedRoomFailed.
        /// </summary>
        /// <remarks>
        /// This method can only be called when the client is OnConnectedToMaster.
        /// When successful, the client will enter the room.
        /// Joining a room will fail if the room is full, locked, does not exist, or the user has not provided the correct password.
        /// </remarks>
        /// <param name="roomId">Unique roomId</param>
        /// <param name="password">Room password. Leave <code>string.Empty</code> if room has not password.</param>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool JoinRoom(string roomId, string password)
        {
            if (MUNClient.ClientStatus != ClientStatus.ConnectedToMasterServer)
            {
                MUNLogs.ShowError($"JoinRoom failed, cause: Required status = ClientStatus.ConnectedToMasterServer. Current status: {MUNClient.ClientStatus}.".WithColorFailed());
                return false;
            }
            if (string.IsNullOrEmpty(roomId))
            {
                MUNLogs.ShowError("JoinRoom failed. RoomId is required.".WithColorFailed());
                return false;
            }

            JoinRoomInfo joinRoom = new JoinRoomInfo()
            {
                RoomId = roomId,
                Password = password
            };
            return Client.JoinRoom(joinRoom);
        }

        /// <summary>
        /// Leave the current room and change <see cref="MUNNetwork.Status"/> to ConnectedToMaster.
        /// </summary>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool LeaveRoom()
        {
            if (!InRoom)
            {
                MUNLogs.ShowError("Current player is not in any room.");
                return false;
            }
            return Client.LeaveRoom();
        }

        /// <summary>
        /// Leave the current voice room.
        /// </summary>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        public static bool LeaveVoiceRoom()
        {
            if (VoiceStatus != ClientVoiceStatus.InRoom)
            {
                MUNLogs.ShowError("Current player is not in any voice room.");
                return false;
            }

            return Client.LeaveVoiceRoom();
        }

        /// <summary>
        /// This method ensures that the level is loaded for all clients in the room.
        /// </summary>
        /// <param name="sceneBuildIndex">The build index number of the level to be loaded.</param>
        /// <param name="changeSceneType">Be the first to load the scene or simultaneously with other users. Option two will not ensure RoomOwner joins first</param>
        /// <param name="destroyRoomObjects">If true, it will delete all network objects on scene change. Default true.</param>
        /// <remarks>
        /// 
        /// More about destroyRoomObjects:
        /// If the parameter is false, then: After changing the scene, all players' objects will be deleted, but the new player will still see this object. 
        /// This means that after a scene change, with the FALSE flag, the objects are not deleted on the server and are still available.
        /// </remarks>
        public static void ChangeScene(int sceneBuildIndex, ChangeSceneType changeSceneType, bool destroyRoomObjects = true)
        {
            if (MUNLoop.IsQuit) return;
            Client.ChangeScene(sceneBuildIndex, changeSceneType, destroyRoomObjects);
        }

        /// <summary>
        /// This method ensures that the level is loaded for all clients in the room.
        /// </summary>
        /// <param name="sceneName">Name of the level to load.</param>
        /// <param name="changeSceneType">Be the first to load the scene or simultaneously with other users. Option two will not ensure RoomOwner joins first</param>
        /// <param name="destroyRoomObjects">If true, it will delete all network objects on scene change. Default true.</param>
        public static void ChangeScene(string sceneName, ChangeSceneType changeSceneType, bool destroyRoomObjects = true)
        {
            if (MUNLoop.IsQuit) return;
            Client.ChangeScene(sceneName, changeSceneType, destroyRoomObjects);
        }

        /// <summary>
        /// Creates a network facility for all players in the room. The local player will own this facility
        /// </summary>
        /// <remarks>
        /// The local player presets a temporary Object Id (with a negative value),
        /// the server returns information about the created object for all players.
        /// The creator of the object then updates the Id of the object.
        /// 
        /// If you want to create an object locally use the default method for object instance.
        /// 
        /// The prefab must be in the resource folder to be found by the MUN. If the object is in a subfolder,
        /// the path should be additionally given in the name, for example: "folder/prefabName".
        /// </remarks>
        /// <param name="prefabName">The name of the prefab located in the resource folder</param>
        /// <returns></returns>
        public static MUNView CreateNetworkObject(string prefabName, Vector3 position, Quaternion rotation)
        {
            if (MUNNetwork.Status != ClientStatus.InRoom)
            {
                MUNLogs.ShowError($"Creating object is allowed only inside room. Current client status = {Status}.");
                return null;
            }

            int tempId = MUNViewsService.GetNextTemporaryMUNViewId(true);
            CreateObjectInfo createObjectInfo = new CreateObjectInfo(prefabName, tempId, LocalPlayer, position, rotation);

            return CreateObject(createObjectInfo, false);
        }

        /// <summary>
        /// Removes a network object with an id corresponding to the object from the parameter.
        /// The object will not be removed if the method is performed by a player other than the owner of the object (except RoomOwner)
        /// </summary>
        /// <remarks>
        /// All RPC calls associated with that object will be deleted as the object is cleared.
        /// 
        /// The destruction of network objects will succeed if and only if they have been added using <see cref="CreateNetworkObject(string, Vector3, Quaternion)"/> 
        /// or were on the scene when the scene was started.
        /// </remarks>
        /// <param name="munView">The object you want to destroy</param>
        public static void DestroyNetworkObject(MUNView munView)
        {
            if (munView is null) return;
            DestroyMUNObject(munView, false);
        }

        /// <summary>
        ///  Removes all network objects that <see cref="Player"/> is owner.
        ///  See more at: <see cref="DestroyNetworkObject(MUNView)"/>.
        /// </summary>
        /// <remarks>
        /// Can be called only by RoomOwner
        /// </remarks>
        /// <param name="player">The player you want to remove objects for</param>
        /// <param name="includeServerObjects">If true, it will also delete existing objects in the scene's build</param>
        public static void DestroyAllPlayerMUNViews(Player player, bool includeServerObjects = false)
        {
            if (!LocalPlayer.IsRoomOwner)
            {
                MUNLogs.ShowError("Only RoomOwner can destroy all player MUNViews.");
                return;
            }
            IEnumerable<MUNView> munViews = MUNView.FindAllByOwner(player.Id, includeServerObjects);
            foreach (MUNView munView in munViews) DestroyNetworkObject(munView);
        }

        /// <summary>
        /// Removes all network objects on the scene.
        /// </summary>
        /// <remarks>
        /// Can be called only by RoomOwner.
        /// </remarks>
        /// <param name="includeServerObjects">If true, it will also delete existing objects in the scene's build</param>
        public static void DestroyAllMUNViews(bool includeServerObjects = false)
        {
            if (!LocalPlayer.IsRoomOwner)
            {
                MUNLogs.ShowError("Only RoomOwner can destroy all player MUNViews.");
                return;
            }
            IEnumerable<MUNView> munViews = MUNView.FindAll(includeServerObjects);
            foreach (MUNView munView in munViews) DestroyNetworkObject(munView);
        }

        /// <summary>
        /// Allows you to change the owner of the selected network object
        /// </summary>
        /// <remarks>
        /// The owner can only be changed by the current owner of the property or by the owner of the room.
        /// 
        /// When the owner of an object changes, the OnMUNViewOwnerChanged method is called.
        /// </remarks>
        /// <param name="view">The object whose owner you want to change.</param>
        /// <param name="newOwner">New owner for the facility.</param>
        internal static void ChangeMUNViewOwner(MUNView view, Player newOwner)
        {
            if (newOwner == null)
            {
                MUNLogs.ShowError("New owner is null.");
                return;
            }

            Client.ChangeMUNViewOwner(view, newOwner);
        }

        internal static void SendRPC(MUNView view, string method, RPCTargets targets, ProtocolType protocolType = ProtocolType.UDP, params object[] args)
        {

            if (string.IsNullOrEmpty(method))
            {
                MUNLogs.ShowError("RPC method is null or empty.");
                return;
            }

            if (CurrentRoom == null || Client == null)
            {
                MUNLogs.ShowError("RPCs can be sent only via room. Check Client connection.");
                return;
            }

            SendRPC(view, method, targets, null, protocolType, args);
        }

        internal static void SendRPC(MUNView view, string method, Player target, ProtocolType protocolType = ProtocolType.UDP, params object[] args)
        {
            if (string.IsNullOrEmpty(method))
            {
                MUNLogs.ShowError("RPC method is null or empty.");
                return;
            }

            if (target == null)
            {
                MUNLogs.ShowError("RPC can not be sent to null player.");
                return;
            }

            if (CurrentRoom == null || Client == null)
            {
                MUNLogs.ShowError("RPCs can be sent only via room. Check Client connection.");
                return;
            }

            SendRPC(view, method, RPCTargets.OthersInRoom, target, protocolType, args);
        }

        internal static void SendVoice(VoiceData voiceData)
        {
            if (MUNLoop.IsQuit || MUNClient.ClientVoiceStatus != ClientVoiceStatus.InRoom) return;

            if (voiceData is null)
            {
                MUNLogs.ShowError("SendVoice failed. VoiceData is null.".WithColorFailed());
                return;
            }

            Client.SendVoice(voiceData);
        }

        /// <summary>
        /// Allows you to change the owner of a room. It can only be called by the current room owner.
        /// </summary>
        /// <remarks>
        /// When the room owner leaves the current room, the change of owner will be performed automatically to the next player on the server.
        /// If there is no other player on the server - the room is closed.
        /// </remarks>
        /// <param name="newOwner">The owner of the room you want to set.</param>
        /// <returns>
        /// True if the query was successfully sent to the server. This does not mean that the operation was successful, 
        /// but that the message was sent.
        /// </returns>
        internal static bool ChangeRoomOwner(Player newOwner)
        {
            if (newOwner == null) return false;
            return Client.ChangeRoomOwner(newOwner);
        }

        internal static void DestroyMUNObject(MUNView view, bool isLocal)
        {
            if (MUNLoop.IsQuit) return;
            if (view is null)
            {
                MUNLogs.ShowError("DestroyMUNObject Failed. MUNView is null.".WithColorFailed());
                return;
            }

            if (!isLocal)
                DestroyAndSendToServer();
            else
                DestroyLocaly();


            void DestroyLocaly()
            {
                PreDestroyClear(view);
                MUNViewsService.Destroy(view);
            }

            void DestroyAndSendToServer()
            {
                if (view.Owner.Id != LocalPlayer.Id && !LocalPlayer.IsRoomOwner)
                {
                    MUNLogs.ShowError($"DestroyMUNObject Failed. Only MUNView owner or RoomOwner can destroy this ({view.Id}) view.".WithColorFailed());
                    return;
                }

                MUNView[] allMUNViews = view.gameObject.GetComponentsInChildren<MUNView>(true);
                foreach (MUNView munView in allMUNViews)
                {
                    PreDestroyClear(munView);
                }

                Client.DestroyMUNViews(allMUNViews, true);
                MUNViewsService.Destroy(allMUNViews);
            }

            void PreDestroyClear(MUNView preDestroyView)
            {
                preDestroyView.transform.SetParent(null, true);
                preDestroyView.gameObject.SetActive(false);
                RemoveRPCForMUNView(preDestroyView.Id);
            }
        }

        private static MUNView CreateObject(ICreateObjectInfo createObjectInfo, bool isLocal)
        {
            if (!CurrentRoom.TryGetPlayer(createObjectInfo.OwnerId, out Player objectOwner))
            {
                MUNLogs.ShowLog($"CreateObject Failed. Object owner with id = {createObjectInfo.OwnerId} not found.".WithColorFailed());
                return null;
            }

            MUNView newMUNView = MUNViewsService.Create(createObjectInfo.PrefabName, createObjectInfo.Position, createObjectInfo.Rotation);

            if (newMUNView is null)
            {
                MUNLogs.ShowError($"CreateObject failed. Cannot find gameobject with name = {createObjectInfo.PrefabName}.".WithColorFailed());
                return null;
            }
            else if (newMUNView.gameObject.activeSelf)
            {
                MUNLogs.ShowWarning($"We recommend to CreateObject as inactive ({createObjectInfo.PrefabName}).".WithColorFailed());
            }
            if (newMUNView is null || newMUNView.gameObject.GetComponentsInChildren<MUNView>(true).Length > 1)
            {
                MUNLogs.ShowError("CreateObject failed. Can only create server objects contains only one MUNView component on prefab root.".WithColorFailed());
                return null;
            }
            newMUNView.SetViewId(isLocal ? createObjectInfo.ObjectId : createObjectInfo.TempObjectId);
            Consts.MUNViewHelper.ChangeOwner(newMUNView, objectOwner.Id);
            newMUNView.gameObject.SetActive(true);

            if (isLocal)
                return newMUNView;

            return Client.CreateObject(newMUNView, createObjectInfo, true) ? newMUNView : null;

        }

        private static MUNView CreateObject(ICreateObjectInfo createObjectInfo)
        {
            if (!InRoom)
            {
                MUNLogs.ShowError($"Creating object is allowed only inside room. Current client status = {Status}.".WithColorFailed());
                return null;
            }

            if (string.IsNullOrEmpty(createObjectInfo.PrefabName))
            {
                MUNLogs.ShowError("CreateObject Failed. Unknown PrefabName".WithColorFailed());
                return null;
            }

            if (createObjectInfo.ObjectId < 1)
            {
                MUNLogs.ShowError("CreateObject Failed. Incorrect MUNView Id".WithColorFailed());
                return null;
            }

            if (MUNSceneManager.IsSceneChanging)
            {
                var waitForCreateNetworkObjectTuple = new WaitForCreateNetworkObjectTuple(createObjectInfo, CreateObject);
                waitForCreateNetworkObjectTuples.Add(waitForCreateNetworkObjectTuple);
                return null;
            }

            MUNView munView = null;
            if (createObjectInfo.ObjectId < Consts.MAX_START_SERVER_OBJECT_ID)
                UpdateServerObject();
            else if (createObjectInfo.OwnerId == LocalPlayer.Id)
                UpdateMyOwnMUNView();
            else
                CreateNewMUNView();

            return munView;

            void UpdateMyOwnMUNView()
            {
                munView = MUNView.Find(createObjectInfo.TempObjectId);
                if (munView is null)
                {
                    if (MUNNetwork.CreatedMUNViews > createObjectInfo.TempObjectId)
                    {
                        CreateNewMUNView();
                    }
                    else
                    {
                        /*
                         * MUNView has been destroyed before server returns correct MUNView.Id
                         * so - we have to resend DestroyMUNView to others with correct Id
                         */
                        Client.DestroyMUNView(createObjectInfo.ObjectId, true);
                        return;
                    }
                }
                munView.SetViewId(createObjectInfo.ObjectId);
            }
            void CreateNewMUNView()
            {
                munView = CreateObject(createObjectInfo, true);
            }
            void UpdateServerObject()
            {
                munView = MUNView.Find(createObjectInfo.ObjectId);
                if (munView.Owner.Id != createObjectInfo.OwnerId)
                {
                    Consts.MUNViewHelper.ChangeOwner(munView, createObjectInfo.OwnerId);
                }
            }
        }

        private static bool ConnectToMaster(IServerInfo serverInfo)
        {
            if (string.IsNullOrEmpty(serverInfo?.IpPort))
            {
                MUNLogs.ShowError("The server is not responding. Check the correctness of <b>AppId</b> in <b>MUN Asset Setup</b> window (AppId can be tested). If it is correct, then try to disable and re-enable your application in <b>MUN Dashboard</b> => Applications => Application Details window.");
                return false;
            }

            if (string.IsNullOrEmpty(PlayerName))
                PlayerName = "MUNPlayer";

            if (MUNLoop.IsQuit) return false;

            return Client.ConnectToMaster(serverInfo.IpPort);
        }

        private static void WaitingSendRPCs()
        {
            if (waitForSendRPCs.Count == 0) return;

            foreach (WaitForSendRPCTuple waitForRPC in waitForSendRPCs)
            {
                if (waitForRPC.MUNView.Id <= 0) continue;
                waitForRPC.SendRPC.Invoke();
            }

            waitForSendRPCs.RemoveWhere(x => x.MUNView.Id > 0);
        }

        private static void WaitingRecieveRPCs()
        {
            if (waitForRecieveRPCs.Count == 0) return;

            foreach (WaitForRecieveRPCTuple waitForRPC in waitForRecieveRPCs)
            {
                waitForRPC.TryCount++;

                if (MUNView.Find(waitForRPC.MUNViewId) is null) continue;

                waitForRPC.TryCount = byte.MaxValue;
                waitForRPC.RecieveRPC.Invoke();
            }

            waitForRecieveRPCs.RemoveWhere(x => x.TryCount >= WaitForRecieveRPCTuple.MAX_TRY_COUNT);
        }

        private static void WaitForChangedSceneForCreatingNetworkObjects()
        {
            if (waitForCreateNetworkObjectTuples.Count == 0) return;

            foreach (WaitForCreateNetworkObjectTuple createNetworkObjectTuple in waitForCreateNetworkObjectTuples)
            {
                var createdObject = createNetworkObjectTuple.CreateObjectAction.Invoke(createNetworkObjectTuple.CreateObjectInfo);
                if (createdObject != null) createNetworkObjectTuple.IsCreated = true;
            }

            waitForCreateNetworkObjectTuples.RemoveWhere(x => x.IsCreated);
        }

        /// <summary>
        /// [0] = View id
        /// [1] = byte rpc method id
        /// [2] = byte method name   (if rpc method id is unknown)
        /// [3] = rpc arguments (if exists)
        /// </summary>
        static readonly LightHashTable rpcData = new LightHashTable();

        private static void SendRPC(MUNView view, string method, RPCTargets rpcTargets, Player target, ProtocolType protocolType, object[] args)
        {
            if (view == null)
            {
                MUNLogs.ShowError("Unknown MUNView or wrong MUNView id.");
                return;
            }

            if (view.Id < 1)
            {
                MUNLogs.ShowLog($"RPC: {method} for MUNView with tempId = {view.Id} waiting for Id from the MasterServer.");
                WaitForSendRPCTuple waitForRpc = new WaitForSendRPCTuple(view, delegate { SendRPC(view, method, rpcTargets, target, protocolType, args); });
                waitForSendRPCs.Add(waitForRpc);
                return;
            }

            #region Build RPC Data
            rpcData.Clear();

            rpcData[Consts.BYTE_ZERO] = view.Id;

            if (MUNRPCs.TryGetValue(method, out byte rpcId)) rpcData[Consts.BYTE_ONE] = rpcId;
            else rpcData[Consts.BYTE_TWO] = method;

            rpcData[Consts.BYTE_THREE] = rpcTargets == RPCTargets.AllInRoomSyncCache || rpcTargets == RPCTargets.AllInRoomCache || rpcTargets == RPCTargets.OthersInRoomCache;
            SaveArgumentsIntoData();
            #endregion

            #region Send RPC

            if (target != null)
            {
                if (LocalPlayer.Id == target.Id)
                    InvokeRPCMethod(rpcData, target);
                else
                    Client.SendRPC(rpcData, 0 /*SpecyficPlayer*/, target.Id, in protocolType);
                return;
            }

            switch (rpcTargets)
            {
                case RPCTargets.AllInRoom:
                    Client.SendRPC(rpcData, (byte)rpcTargets, 0, in protocolType);
                    InvokeRPCMethod(rpcData, LocalPlayer);
                    break;
                case RPCTargets.AllInRoomCache:
                    Client.SendRPC(rpcData, (byte)rpcTargets, 0, in protocolType);
                    InvokeRPCMethod(rpcData, LocalPlayer);
                    break;
                case RPCTargets.OthersInRoomCache:
                    Client.SendRPC(rpcData, (byte)rpcTargets, 0, in protocolType);
                    AddRPCToCache(rpcData, method, view.Id);
                    break;
                case RPCTargets.AllInRoomSyncCache:
                case RPCTargets.OthersInRoom:
                case RPCTargets.AllInRoomSynch:
                    Client.SendRPC(rpcData, (byte)rpcTargets, 0, in protocolType);
                    break;
                case RPCTargets.RoomOwnerOnly:
                    if (LocalPlayer.IsRoomOwner)
                        InvokeRPCMethod(rpcData, LocalPlayer);
                    else
                        Client.SendRPC(rpcData, (byte)rpcTargets, CurrentRoom.OwnerId, in protocolType);
                    break;
                default:
                    MUNLogs.ShowError("Unknown RPC target.");
                    break;
            }

            #endregion

            void SaveArgumentsIntoData()
            {
                //Arguments count:
                rpcData[Consts.BYTE_FOUR] = args.Length;

                for (int i = 0; i < args.Length; i++)
                {
                    rpcData.Add($"a{i}", args[i]);
                }
            }
        }

        private static void InvokeRPCMethod(LightHashTable rpcData, Player target)
        {
            if (!MUNNetwork.InRoom)
            {
                return;
            }

            if (rpcData is null || !rpcData.ContainsKey(Consts.BYTE_ZERO))
            {
                MUNLogs.ShowError("Unknown RPC data or destination MUNView id.");
                return;
            }

            MUNView munView = GetMUNView(out int munViewId);
            if (munView is null)
            {
                MUNLogs.ShowWarning("MUNView not exists. Try to resend in next MUNLoop.");

                WaitForRecieveRPCTuple waitForRpc = new WaitForRecieveRPCTuple(munViewId, delegate { InvokeRPCMethod(rpcData, target); });
                waitForRecieveRPCs.Add(waitForRpc);

                return;
            }

            string rpcMethodName = GetRPCMethodName();
            if (string.IsNullOrEmpty(rpcMethodName))
            {
                MUNLogs.ShowError("Unknown RPC method name.");
                return;
            }

            object[] args = GetRPCMethodArguments();
            Type[] argsTypes = GetRPCMethodArgumentsTypes();


            munView.RefreshRPCBehaviours();
            int rpcFoundMethods = 0;
            int rpcReceivers = 0;
            foreach (MonoBehaviour rpcBehaviour in munView.RPCBehaviours)
            {
                if (rpcBehaviour is null)
                {
                    MUNLogs.ShowError($"MUNView id = {munView.Id} has null rpc behaviours.");
                    continue;
                }

                Type type = rpcBehaviour.GetType();

                if (!Consts.behaviourRPCMethods.TryGetValue(type, out List<MethodInfo> methods))
                {
                    List<MethodInfo> MUNMethods = type.GetMethods(Consts.MUN_RPC);
                    Consts.behaviourRPCMethods[type] = MUNMethods;
                    methods = MUNMethods;
                }

                if (methods is null || methods.Count == 0) continue;

                foreach (MethodInfo method in methods)
                {
                    if (!method.Name.Equals(rpcMethodName)) continue;
                    rpcFoundMethods++;

                    ParameterInfo[] parameters = method.GetParameters();

                    if (args == null)
                    {
                        if (parameters.Length == 0)
                        {
                            InvokeMethod(method, rpcBehaviour, null);
                            rpcReceivers++;
                        }
                        continue;
                    }

                    if (args.Length == parameters.Length)
                    {
                        if (CompareTypes(parameters, argsTypes))
                        {
                            InvokeMethod(method, rpcBehaviour, args);
                            rpcReceivers++;
                        }
                        continue;
                    }

                    if (parameters.Length == 1 && parameters[0].ParameterType.IsArray)
                    {
                        InvokeMethod(method, rpcBehaviour, new object[] { args });
                        rpcReceivers++;
                        continue;
                    }
                }
            }

            bool saveInCache = (bool)rpcData[Consts.BYTE_THREE];

            if (rpcReceivers != 1) CheckErrors();
            else if (saveInCache) SaveInCache();        // Save in cache only if no errors

            MUNView GetMUNView(out int munViewId)
            {
                munViewId = (int)rpcData[Consts.BYTE_ZERO];
                return MUNNetwork.GetMUNView(munViewId);
            }
            string GetRPCMethodName()
            {
                if (rpcData.ContainsKey(Consts.BYTE_ONE))
                {
                    byte rpcMethodId = (byte)rpcData[Consts.BYTE_ONE];
                    return MUNRPCsList[rpcMethodId];
                }
                else
                {
                    return (string)rpcData[Consts.BYTE_TWO];
                }
            }
            object[] GetRPCMethodArguments()
            {
                int argsCount = (int)rpcData[Consts.BYTE_FOUR];
                if (argsCount == 0) return null;

                object[] returnArgs = new object[argsCount];

                for (int i = 0; i < argsCount; i++)
                {
                    returnArgs[i] = rpcData[$"a{i}"];
                }

                return returnArgs;
            }
            Type[] GetRPCMethodArgumentsTypes()
            {
                if (args != null && args.Length > 0)
                {
                    Type[] types = new Type[args.Length];
                    for (int i = 0; i < types.Length; i++)
                    {
                        if (args[i] != null)
                            types[i] = args[i].GetType();
                    }

                    return types;
                }

                return null;
            }
            bool CompareTypes(ParameterInfo[] methodParams, Type[] paramTypes)
            {
                if (methodParams.Length != paramTypes.Length) return false;

                Type type;
                for (int i = 0; i < methodParams.Length; i++)
                {

                    type = methodParams[i].ParameterType;
                    if (paramTypes[i] != null && !type.IsAssignableFrom(paramTypes[i]) && !(type.IsEnum && Enum.GetUnderlyingType(type).IsAssignableFrom(paramTypes[i])))
                    {
                        return false;
                    }
                }

                return true;
            }
            void InvokeMethod(MethodInfo method, MonoBehaviour behaviour, object[] arguments)
            {
                object invokeMethodObject = method.Invoke(behaviour, arguments);
                if (invokeMethodObject is IEnumerator)
                {
                    IEnumerator invokeEnumerator = invokeMethodObject as IEnumerator;
                    MUNLoop.InvokeCoroutine(invokeEnumerator);
                }
            }
            void CheckErrors()
            {
                StringBuilder argsString = new StringBuilder();
                if (argsTypes != null)
                {
                    foreach (Type argType in argsTypes)
                    {
                        if (argType == null)
                            argsString.Append("null, ");
                        else
                            argsString.Append($"argType.Name, ");
                    }
                }

                if (argsString.Length > 0)
                    argsString.Remove(argsString.Length - 2, 2);

                GameObject munGameObject = munView != null ? munView.gameObject : null;
                int munId = (int)rpcData[Consts.BYTE_ZERO];
                if (rpcReceivers == 0)
                {
                    if (rpcFoundMethods == 0)
                    {
                        MUNLogs.ShowError($"RPC method {rpcMethodName} (args: {argsString}) not found. Causes: 1) MUNView with id = {munId} not found; 2) Method {rpcMethodName} is static; 3) Method {rpcMethodName} has no [MunRPC] attribute; 3) Wrong return type. Only void or IEnumerator.", munGameObject);
                    }
                    else
                    {
                        MUNLogs.ShowError($"RPC method {rpcMethodName} (args: {argsString}) found on MUNView with id = {munId} with incorrect parameters.", munGameObject);
                    }
                }
                else
                {
                    MUNLogs.ShowError($"RPC method {rpcMethodName} (args: {argsString}) found multiple times ({rpcFoundMethods}x). System expected just one component.", munGameObject);
                }
            }
            void SaveInCache()
            {
                int munId = (int)rpcData[Consts.BYTE_ZERO];
                AddRPCToCache(rpcData, rpcMethodName, munId);
            }
        }

        private static void ClearWaitingFunctions()
        {
            waitForCreateNetworkObjectTuples.Clear();
            waitForRecieveRPCs.Clear();
            waitForSendRPCs.Clear();
        }

        #region Classess
        internal static class Consts
        {

            /// <summary>
            /// Reference to the type of MunRPC attribute
            /// </summary>
            internal static readonly Type MUN_RPC = typeof(MunRPCAttribute);

            /// <summary>
            /// Accuracy of accuracy when comparing vectors
            /// </summary>
            public const float VECTOR_SYNCHRONIZATION_PRECISSION = .005f;

            /// <summary>
            /// Precision of accuracy when comparing quaternion
            /// </summary>
            public const float QUATERNION_SYNCHRONIZATION_PRECISSION = 1f;

            /// <summary>
            /// Accuracy of accuracy when comparing floats
            /// </summary>
            public const float FLOAT_SYNCHRONIZATION_PRECISSION = .025f;

            /// <summary>
            /// Time from loading a scene after which MUNView is updated
            /// </summary>
            public const float WAIT_FOR_SCENE_TIME_TO_UPDATE_VIEW = 1f;

            /// <summary>
            /// Illegal Id for MUNView
            /// </summary>
            public const int NOT_ASSIGNED_MUN_VIEW_ID = 0;

            /// <summary>
            /// Used to convert shorts to floats and floats to shorts. 
            /// We will save on the size of parcel transmission between clients and the server.
            /// </summary>
            public const float FLOAT_TO_SHORT_CONVERSION_MULTIPLER = 32760f;

            public const byte INCREASE_PORT_FOR_VOICE_MASTERSERVER = 1;

            /// <summary>
            /// Minimum number of players per room to serialize MUNViews or send voice data
            /// </summary>
            public const byte MIN_PLAYERS_TO_SERIALIZE = 2;

            /// <summary>
            /// The minimum data length to read over the network and serialize data
            /// </summary>
            public const byte MIN_DATA_LENGTH_REQUIRED = 4;

            /// <summary>
            /// The maximum size of a package sent via the UDP protocol
            /// </summary>
            public const int MAX_UDP_PACKAGE_SIZE = 1024 * 2;

            /// <summary>
            /// The maximum size of a package sent via the TCP protocol
            /// </summary>
            public const int MAX_TCP_PACKAGE_SIZE = 2048 * 10;
            /// <summary>
            /// The maximum number of network objects in the scene by default
            /// </summary>
            public const int MAX_START_SERVER_OBJECT_ID = 1000;

            /// <summary>
            /// Every how many seconds UDP is updated - protection against disconnection
            /// </summary>
            public const byte LOOPS_TO_UDP_CONNECT = 2;

            /// <summary>
            /// Multiplier for computing the temporary identifier of network objects
            /// </summary>
            public const int TEMP_ID_MULTIPLER = 100;

            /// <summary>
            /// Maximum number of packets sent per network clock tick
            /// </summary>
            public const int MAX_PACKETS_PER_TICK = 35;

            /// <summary>
            /// Waiting time for connection with the MasterServer.
            /// Protection against too fast connection before we get the appropriate packages from the server.
            /// </summary>
            internal const int CONNECTING_WAIT_TIME = 2500;


            internal const byte BYTE_ZERO = 0;
            internal const byte BYTE_ONE = 1;
            internal const byte BYTE_TWO = 2;
            internal const byte BYTE_THREE = 3;
            internal const byte BYTE_FOUR = 4;

            /// <summary>
            /// Data types that can be serialized by MUN
            /// </summary>
            internal static readonly Dictionary<Type, byte> PACKET_PROPETY_TYPE = new Dictionary<Type, byte>()
            {
                {typeof(byte), 1 },
                {typeof(byte[]), 2 },
                {typeof(short), 3 },
                {typeof(int), 4 },
                {typeof(long), 5 },
                {typeof(float), 6 },
                {typeof(bool), 7 },
                {typeof(string), 8 },
                {typeof(Vector2), 9 },
                {typeof(Vector3), 10 },
                {typeof(Quaternion), 11 }
            };

            /// <summary>
            /// Methods called when reading packages from the server. If a given package does not have a known type - an exception is thrown.
            /// </summary>
            internal static readonly Dictionary<byte, PacketHandler> PACKET_HANDLERS = new Dictionary<byte, PacketHandler>
            {
                { (byte)ClientReceivePackets.ConnectedToMaster,       MUNClientHandler.OnConnectedToMaster },
                { (byte)ClientReceivePackets.ConnectedToMasterFailed, MUNClientHandler.OnConnectedToMasterFailed },
                { (byte)ClientReceivePackets.ReceiveSerialization,    MUNClientHandler.OnSerialization },
                { (byte)ClientReceivePackets.CreatedRoom,             MUNClientHandler.OnCreatedRoom },
                { (byte)ClientReceivePackets.CreatedRoomFailed,       MUNClientHandler.OnCreatedRoomFailed },
                { (byte)ClientReceivePackets.JoinedRoom,              MUNClientHandler.OnJoinedRoom },
                { (byte)ClientReceivePackets.JoiningRoomFailed,       MUNClientHandler.OnJoinedRoomFailed },
                { (byte)ClientReceivePackets.PlayerJoinedRoom,        MUNClientHandler.OnPlayerJoinedRoom },
                { (byte)ClientReceivePackets.LeftRoom,                MUNClientHandler.OnLeftRoom },
                { (byte)ClientReceivePackets.RoomListUpdate,          MUNClientHandler.OnRoomListUpdated },
                { (byte)ClientReceivePackets.ReceiveRoomProperties,   MUNClientHandler.OnRoomPropertiesUpdated },
                { (byte)ClientReceivePackets.ReceivePlayerProperties, MUNClientHandler.OnPlayerPropertiesUpdated },
                { (byte)ClientReceivePackets.OwnerChanged,            MUNClientHandler.OnRoomOwnerChanged },
                { (byte)ClientReceivePackets.ChangedScene,            MUNClientHandler.OnRoomSceenChanged },
                { (byte)ClientReceivePackets.MUNViewsOwnerchanged,    MUNClientHandler.OnMUNViewOwnerChanged },
                { (byte)ClientReceivePackets.ReceiveRPC,              MUNClientHandler.OnReceiveRPC},
                { (byte)ClientReceivePackets.CreatedObject,           MUNClientHandler.OnCreatedObject},
                { (byte)ClientReceivePackets.CreatedObjectFailed,     MUNClientHandler.OnCreateObjectFailed},
                { (byte)ClientReceivePackets.DestroyedObjects,        MUNClientHandler.OnDestroyedObject}
            };

            internal static readonly Dictionary<byte, PacketHandler> PACKET_VOICE_HANDLERS = new Dictionary<byte, PacketHandler>
            {
                {(byte)ClientVoiceReceivePackets.ConnectedToMaster,      MUNVoiceClientHandler.OnConnectedToMaster},
                {(byte)ClientVoiceReceivePackets.ReceiveVoice,           MUNVoiceClientHandler.OnReceiveVoice},
                {(byte)ClientVoiceReceivePackets.JoinedRoom,             MUNVoiceClientHandler.OnJoinedRoom},
                {(byte)ClientVoiceReceivePackets.PlayerJoinedRoom,       MUNVoiceClientHandler.OnPlayerJoinedRoom},
                {(byte)ClientVoiceReceivePackets.LeftRoom,               MUNVoiceClientHandler.OnLeftRoom}
            };
            internal static readonly Dictionary<Type, List<MethodInfo>> behaviourRPCMethods = new Dictionary<Type, List<MethodInfo>>();


            internal static class MUNViewHelper
            {
                static readonly Type munViewType = typeof(MUNView);
                static readonly MethodInfo changeOwnerMethod;
                static readonly object[] changeOwnerParameters = new object[2];

                static MUNViewHelper()
                {
                    changeOwnerMethod = munViewType.GetMethod("ChangeOwner", BindingFlags.NonPublic | BindingFlags.Static);
                }

                public static void ChangeOwner(MUNView view, int ownerId)
                {
                    changeOwnerParameters[0] = view;
                    changeOwnerParameters[1] = ownerId;
                    changeOwnerMethod.Invoke(view, changeOwnerParameters);
                }
            }
        }

        /// <summary>
        /// Auxiliary class while creating a room
        /// </summary>
        internal class CreateRoomInfo
        {
            /// <summary>
            /// Custom room name
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Room properties and custom room properties
            /// </summary>
            public Room.RoomClaims Claims { get; set; }
        }

        internal class CreateVoiceRoomInfo : ICreateVoiceRoomInfo
        {
            public string RoomName { get; }
            public byte MaxPlayers { get; }

            internal CreateVoiceRoomInfo(string roomName, byte maxPlayers)
            {
                RoomName = roomName;
                MaxPlayers = maxPlayers;
            }
        }

        internal interface ICreateVoiceRoomInfo
        {
            public string RoomName { get; }
            public byte MaxPlayers { get; }
        }

        /// <summary>
        /// Auxiliary class when joining a room
        /// </summary>
        internal class JoinRoomInfo
        {
            /// <summary>
            /// A flag for the unique room identifier property.
            /// Using RoomId, we can connect to the room.
            /// </summary>
            public string RoomId { get; set; }
            /// <summary>
            /// Password preventing unwanted clients from entering the room.
            /// Password saved and verified on the server side.
            /// </summary>
            public string Password { get; set; }
        }


        /// <summary>
        /// A helper class for creating network objects.
        /// </summary>
        [Serializable]
        internal class CreateObjectInfo : ICreateObjectInfo
        {
            internal string prefabName;
            internal int tempObjectId;
            internal int ownerId;
            internal Vector3 position;
            internal Quaternion rotation;
            internal int objectId;


            internal CreateObjectInfo(string prefabName, int tempObjectId, Player owner, Vector3 position, Quaternion rotation)
            {
                this.prefabName = prefabName;
                this.ownerId = owner.Id;
                this.position = position;
                this.rotation = rotation;
                this.tempObjectId = tempObjectId;
            }

            private CreateObjectInfo(string prefabName, int tempObjectId, int objectId, Player owner, Vector3 position, Quaternion rotation)
            {
                this.prefabName = prefabName;
                this.objectId = objectId;
                this.tempObjectId = tempObjectId;
                this.ownerId = owner.Id;
                this.position = position;
                this.rotation = rotation;
            }

            /// <summary>
            /// The name of the prefab from which the network object was created.
            /// </summary>
            public string PrefabName => prefabName;
            /// <summary>
            /// The temporary identifier of the object that the system uses by the server will respond with the real identifier.
            /// Identifiers are assigned on the server side.
            /// </summary>
            public int TempObjectId => tempObjectId;
            /// <summary>
            /// Information on which player is the owner of the object.
            /// </summary>
            public int OwnerId => ownerId;
            /// <summary>
            /// Network object creation position
            /// </summary>
            public Vector3 Position => position;
            /// <summary>
            /// Network object creation rotation
            /// </summary>
            public Quaternion Rotation => rotation;
            /// <summary>
            /// Unique identifier of the network object. Identifiers are assigned on the server side.
            /// </summary>
            public int ObjectId => objectId;

            internal static ICreateObjectInfo Build(string prefabName, int tempObjectId, Player owner, int objectId, Vector3 position, Quaternion rotation)
            {
                return new CreateObjectInfo(prefabName, tempObjectId, objectId, owner, position, rotation);
            }
        }

        private class WaitForCreateNetworkObjectTuple : Tuple<ICreateObjectInfo, Func<ICreateObjectInfo, MUNView>>
        {
            public WaitForCreateNetworkObjectTuple(ICreateObjectInfo createObjectInfo, Func<ICreateObjectInfo, MUNView> createObject) : base(createObjectInfo, createObject)
            {
                IsCreated = false;
            }

            public ICreateObjectInfo CreateObjectInfo => Item1;
            public Func<ICreateObjectInfo, MUNView> CreateObjectAction => Item2;
            public bool IsCreated { get; set; }
        }

        private class WaitForSendRPCTuple : Tuple<MUNView, Action>
        {
            public WaitForSendRPCTuple(MUNView munView, Action sendRPC) : base(munView, sendRPC)
            {
            }

            public MUNView MUNView => Item1;
            public Action SendRPC => Item2;
        }

        private class WaitForRecieveRPCTuple : Tuple<int, Action>
        {
            public const byte MAX_TRY_COUNT = 100;

            public WaitForRecieveRPCTuple(int munViewId, Action recieveRPC) : base(munViewId, recieveRPC)
            {
            }

            public byte TryCount { get; set; }
            public int MUNViewId => Item1;
            public Action RecieveRPC => Item2;
        }

        /// <summary>
        /// Auxiliary interface for creating a network object
        /// </summary>
        internal interface ICreateObjectInfo
        {
            /// <summary>
            /// Information on which player is the owner of the object.
            /// </summary>
            int OwnerId { get; }
            /// <summary>
            /// The temporary identifier of the object that the system uses by the server will respond with the real identifier.
            /// Identifiers are assigned on the server side.
            /// </summary>
            int TempObjectId { get; }
            /// <summary>
            /// Unique identifier of the network object. Identifiers are assigned on the server side.
            /// </summary>
            int ObjectId { get; }
            /// <summary>
            /// Network object creation position
            /// </summary>
            Vector3 Position { get; }
            /// <summary>
            /// The name of the prefab from which the network object was created.
            /// </summary>
            string PrefabName { get; }
            /// <summary>
            /// Network object creation rotation
            /// </summary>
            Quaternion Rotation { get; }
        }

        #endregion

        #region Enumes
        /// <summary>
        /// Enum defines type of change scene - <see cref="ChangeSceneType.Async"/> or <see cref="ChangeSceneType.NoAsync"/>.
        /// </summary>
        public enum ChangeSceneType
        {
            /// <summary>
            /// First, the scene change information is sent to the server, and the response captured by all players performs a scene change.
            /// </summary>
            Async,

            /// <summary>
            /// The scene change is triggered immediately. Then, a query is triggered to the server with information about the change of the scene for other players
            /// </summary>
            NoAsync
        }

        /// <summary>
        /// Enum defines to which group of recipients the RPC method should be sent, 
        /// as well as whether the given method should be saved for other players who will join the room.
        /// </summary>
        public enum RPCTargets : byte
        {
            /// <summary>
            /// Send to all in room (sender invoke localy).
            /// </summary>
            AllInRoom = 1,
            /// <summary>
            /// Send to all in room (sender invoke localy) save method in cache to execute for new players.
            /// </summary>
            AllInRoomCache,
            /// <summary>
            /// Send to others in room (not invoke in sender).
            /// </summary>
            OthersInRoom,
            /// <summary>
            /// Send to others in room (not invoke in sender) save method in cache to execute for new players.
            /// </summary>
            OthersInRoomCache,
            /// <summary>
            /// Send to all in room.
            /// </summary>
            AllInRoomSynch,
            /// <summary>
            /// Send to all in room save method in cache to execute for new players.
            /// </summary>
            AllInRoomSyncCache,
            /// <summary>
            /// Send to room owner only.
            /// </summary>
            RoomOwnerOnly
        }

        /// <summary>
        /// Enum that allows you to define the type of information sent to the server.
        /// </summary>
        /// <remarks>
        /// <see cref="ProtocolType.VoiceChat"/> is not recommended to use. Just keep calm and use TCP or UDP protocols :)
        /// </remarks>
        public enum ProtocolType : byte
        {
            TCP, UDP, VoiceChat
        }

        /// <summary>
        /// Enum that allows you to define how objects are handled by players who leave the room.
        /// </summary>
        public enum MUNViewsLogicOnPlayerDisconected
        {
            /// <summary>
            /// The object remains with the player who left the room.
            /// </summary>
            /// <remarks>
            /// This gives him the option to retrieve the items should he accidentally leave the room.
            /// </remarks>
            WaitForPlayer,
            /// <summary>
            /// Automatically changes the owner of all player objects to RoomOwner
            /// </summary>
            ChangeOwnerToRoomOwner,
            /// <summary>
            /// Automatically removes all player objects in the room.
            /// </summary>
            DestroyAllPlayerMUNViews
        }

        /// <summary>
        /// Enum specifying the client's status
        /// </summary>
        public enum ClientStatus : byte
        {
            /// <summary>
            /// Client not connected to MasterServer
            /// </summary>
            Disconnected,
            /// <summary>
            /// The client is connecting to the MasterServer
            /// </summary>
            ConnectingToMasterServer,
            /// <summary>
            /// Client connected to MasterServer
            /// </summary>
            ConnectedToMasterServer,
            /// <summary>
            /// The client is connecting to the room
            /// </summary>
            JoiningIntoRoom,
            /// <summary>
            /// The client is creating a room or connecting to a random room
            /// </summary>
            CreatingOrJoiningRandomRoom,
            /// <summary>
            /// Client in the process of creating a room
            /// </summary>
            CreatingRoom,
            /// <summary>
            /// The client has created a room
            /// </summary>
            CreatedRoom,
            /// <summary>
            /// The client is leaving the room
            /// </summary>
            LeavingRoom,
            /// <summary>
            /// The client is in the room
            /// </summary>
            InRoom,
            /// <summary>
            /// The client is in the process of changing the scene (while in the room)
            /// </summary>
            ChangingScene,
        }

        public enum ClientVoiceStatus : byte
        {
            /// <summary>
            /// Client not connected to VoiceMasterServer
            /// </summary>
            Disconnected,
            /// <summary>
            /// The client is connecting to the VoiceMasterServer
            /// </summary>
            ConnectingToVoiceMasterServer,
            /// <summary>
            /// The client is leaving the voice room
            /// </summary>
            LeavingRoom,
            /// <summary>
            /// Client connected to VoiceMasterServer
            /// </summary>
            ConnectedToVoiceMasterServer,
            /// <summary>
            /// The client is connecting to the voice room
            /// </summary>
            JoiningIntoRoom,
            /// <summary>
            /// The client is in the voice room
            /// </summary>
            InRoom
        }

        /// <summary>
        /// Enum containing known keys for custom <see cref="Player"/> properties
        /// </summary>
        internal enum PlayerProperties : byte
        {
            ///<summary>Unique Player Id</summary>
            Id = 1,
            ///<summary> Player Nickname</summary>
            Name = 2
        }

        /// <summary>
        /// Enum containing known keys for custom <see cref="RoomData"/> properties
        /// </summary>
        internal enum RoomProperties : byte
        {
            /// <summary>
            /// The maximum number of players allowed to join a room.
            /// </summary>
            MaxPlayers = 1,
            /// <summary>
            /// A flag that determines whether a given room will be displayed in the list of available rooms in OnRoomListUpdated.
            /// Players can still join a room by entering its Id.
            /// </summary>
            IsVisable = 2,
            /// <summary>
            /// A flag that determines whether the room will be open to other players who want to join the room.
            /// A locked room will not be available in the list of available rooms in OnRoomListUpdated.
            /// </summary>
            IsOpen = 3,
            /// <summary>
            /// The id of the <see cref="Player"/> who owns the room.
            /// RoomOwner may change the owner of a room.
            /// </summary>
            OwnerId,
            /// <summary>
            /// The flag indicating whether the room has a password. The password is saved on the server side.
            /// Clients only get information whether a given room has a password.
            /// The password is also verified on the server side.
            /// </summary>
            HasPassword,
            /// <summary>
            /// A flag for the unique room identifier property.
            /// Using RoomId, we can connect to the room.
            /// </summary>
            RoomId,
            /// <summary>
            /// A flag showing the current number of players in the room.
            /// </summary>
            PlayersCount
        }

        /// <summary>
        /// Identifiers of the packages that the client sends to the MasterServer.
        /// </summary>
        internal enum ClientSendPackets : byte
        {
            /// <summary>
            /// Connection to the MasterServer.
            /// </summary>
            ConnectingToMaster = 1,
            /// <summary>
            /// Connection with MasterServer via UDP.
            /// </summary>
            UDPConnecting,
            /// <summary>
            /// Sending MUNViews serialization.
            /// </summary>
            SendSerialization,
            /// <summary>
            /// Attempting to cteate room.
            /// </summary>
            CreatingRoom,
            /// <summary>
            /// Attempting to join an existing room or creating a new room.
            /// </summary>
            CreatingOrJoiningRandomRoom,
            /// <summary>
            /// Attempting to join the existing room.
            /// </summary>
            JoiningRoom,
            /// <summary>
            /// Leaving the room.
            /// </summary>
            LeavingRoom,
            /// <summary>
            /// Attempting to send updated room properties.
            /// </summary>
            SendRoomProperties,
            /// <summary>
            /// Attempting to send updated player properties.
            /// </summary>
            SendPlayerProperties,
            /// <summary>
            /// Attempt to throw a player out of the room.
            /// Only available for RoomOwner.
            /// </summary>
            KickPlayer,
            /// <summary>
            /// Attempting to change RoomOwner.
            /// Only available for the current RoomOwner.
            /// </summary>
            ChangeOwner,
            /// <summary>
            /// Attempting to change owner of MUNView.
            /// Only available to the current owner of MUNView and RoomOwner.
            /// </summary>
            ChangeMUNViewOwner,
            /// <summary>
            /// Attempting to change the scene with MUN.
            /// Only available for RoomOwner.
            /// </summary>
            ChangingScene,
            /// <summary>
            /// Attempt to send a Remote Procedure Call.
            /// </summary>
            SendingRPC,
            /// <summary>
            /// Attempting to send create a network object.
            /// </summary>
            CreatingObject,
            /// <summary>
            /// Attempting to send a network object delete.
            /// Available to the owner of the facility and RoomOwner.
            /// </summary>
            DestroingObjects,
            /// <summary>
            /// Request for a current list of rooms. The room list updates itself.
            /// Available when the client status is: ConnectedToMaster.
            /// </summary>
            UpdateingRoomsList
        }

        /// <summary>
        /// Identifiers of packages that the client receives from the MasterServer.
        /// </summary>
        internal enum ClientReceivePackets : byte
        {
            /// <summary>
            /// Package with information about correct connection to the MasterServer.
            /// </summary>
            ConnectedToMaster = 1,
            /// <summary>
            /// Package with information about incorrect connection to the MasterServer. The package contains an error message.
            /// </summary>
            ConnectedToMasterFailed,
            /// <summary>
            /// Receive MUNViews serialization.
            /// </summary>
            ReceiveSerialization,
            /// <summary>
            /// Return package with information about the created room.
            /// </summary>
            CreatedRoom,
            /// <summary>
            /// Return package with error message when creating a room.
            /// </summary>
            CreatedRoomFailed,
            /// <summary>
            /// Package with information about joining the room.
            /// </summary>
            JoinedRoom,
            /// <summary>
            /// Return package with error information when joining the room.
            /// </summary>
            JoiningRoomFailed,
            /// <summary>
            /// Return package with information that another player has joined the room.
            /// </summary>
            PlayerJoinedRoom,
            /// <summary>
            /// Return package with information about leaving the player's room.
            /// </summary>
            LeftRoom,
            /// <summary>
            /// Return package with a list of available rooms.
            /// The package will be sent to clients whose status is: ConnectedToMaster.
            /// </summary>
            RoomListUpdate,
            /// <summary>
            /// Return package with changed room properties.
            /// </summary>
            ReceiveRoomProperties,
            /// <summary>
            /// Return package with changed player properties.
            /// </summary>
            ReceivePlayerProperties,
            /// <summary>
            /// Return package with information about the new RoomOwner.
            /// </summary>
            OwnerChanged,
            /// <summary>
            /// Return package with information about the new owner of the MUNView.
            /// </summary>
            MUNViewsOwnerchanged,
            /// <summary>
            /// Return package with information about the scene change.
            /// </summary>
            ChangedScene,
            /// <summary>
            /// Return packet with information about calling Remote Procedure Call.
            /// </summary>
            ReceiveRPC,
            /// <summary>
            /// Return package with information about creating a network object.
            /// </summary>
            CreatedObject,
            /// <summary>
            /// Return package with error information when creating object is failed.
            /// </summary>
            CreatedObjectFailed,
            /// <summary>
            /// Return package with information that the network object has been removed.
            /// </summary>
            DestroyedObjects
        }

        /// <summary>
        /// Identifiers of the packages that the client sends to the VoiceMasterServer.
        /// </summary>
        internal enum ClientVoiceSendPackets : byte
        {
            /// <summary>
            /// Connection to the VoiceMasterServer.
            /// </summary>
            ConnectingToMaster = 1,
            /// <summary>
            /// Sending MUNVoiceViews voice from your microphone.
            /// </summary>
            SendVoice,
            /// <summary>
            /// Attempting to join the existing voice room.
            /// </summary>
            CreatingOrJoiningVoiceRoom,
            /// <summary>
            /// Leaving the voice room.
            /// </summary>
            LeavingRoom
        }

        /// <summary>
        /// Identifiers of packages that the client receives from the VoiceMasterServer.
        /// </summary>
        internal enum ClientVoiceReceivePackets : byte
        {
            /// <summary>
            /// Package with information about correct connection to the VoiceMasterServer.
            /// </summary>
            ConnectedToMaster = 1,
            /// <summary>
            /// Receive MUNVoiceViews voice from others microphone.
            /// </summary>
            ReceiveVoice,
            /// <summary>
            /// Package with information on successful connection to the voice room.
            /// </summary>
            JoinedRoom,
            /// <summary>
            /// Return package with information that another player has joined the voice room.
            /// </summary>
            PlayerJoinedRoom,
            /// <summary>
            /// Return package with information about leaving the player's voice room.
            /// </summary>
            LeftRoom
        }
        #endregion
    }
}
