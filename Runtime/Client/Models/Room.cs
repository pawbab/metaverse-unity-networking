using MUN.Client.Extensions;
using System.Collections.Generic;
using static MUN.Client.MUNNetwork;

namespace MUN.Client
{
    /// <summary>
    /// The model of the room that the client has joined.
    /// </summary>
    /// <remarks>
    /// A room contains a reference to the <see cref="Player"/>s in it,
    /// as well as some default properties, such as:
    /// <list type="bullet">
    /// <item>Is the room open</item>
    /// <item>Whether the room is visible</item>
    /// <item>How many players are in the room</item>
    /// </list>
    /// 
    /// And also has access to custom properties that can be set when creating a room,
    /// as well as after its creation using the <see cref="SetCustomProperties(LightHashTable)"/> function.
    /// </remarks>
    public partial class Room : RoomData
    {
        /// <summary>
        /// A flag that determines whether the room will be open to other players who want to join the room.
        /// A locked room will not be available in the list of available rooms in OnRoomListUpdated.
        /// By changing this flag you send: UpdateRoomProperties.
        /// </summary>
        public new bool IsOpen
        {
            get => base.IsOpen;
            set
            {
                if (value != base.IsOpen)
                {

                    base.IsOpen = value;
                    MUNNetwork.Client.UpdateRoomProperties(new LightHashTable() { { RoomProperties.IsOpen, value } });
                }
            }
        }
        /// <summary>
        /// A flag that determines whether a given room will be displayed in the list of available rooms in OnRoomListUpdated.
        /// Players can still join a room by entering its Id. By changing this flag you send: UpdateRoomProperties.
        /// </summary>
        public new bool IsVisable
        {
            get => base.IsVisable;
            set
            {
                if (value != base.IsVisable)
                {
                    base.IsVisable = value;
                    MUNNetwork.Client.UpdateRoomProperties(new LightHashTable() { { RoomProperties.IsVisable, value } });
                }
            }
        }
        /// <summary>
        /// Current number of players in the room.
        /// </summary>
        public byte PlayersInRoom
        {
            get
            {
                if (Players is null) return 0;
                else return (byte)Players.Count;
            }
        }
        /// <summary>
        /// A dictionary that stores a player's ID (<see cref="Player.Id"/>) as keys, and a model of a player with the same ID as a value.
        /// </summary>
        public Dictionary<int, Player> Players { get; private set; }

        protected internal Room(string roomName, RoomClaims claims = null) : base(roomName, claims?.Properties)
        {
            Players = new Dictionary<int, Player>();
        }

        /// <summary>
        /// A method that allows a room owner to appoint a new owner for a room.
        /// </summary>
        /// <param name="owner">The player who is to become the new owner of the room.</param>
        public void ChangeOwner(Player owner)
        {
            if (!LocalPlayer.IsRoomOwner) return;
            MUNNetwork.ChangeRoomOwner(owner);
        }
        /// <summary>
        /// A method that allows you to set custom properties for a room.
        /// </summary>
        /// <param name="properties">Custom room properties</param>
        public virtual void SetCustomProperties(LightHashTable properties)
        {
            if (properties == null
                || properties.Count == 0)
                return;

            SetupProperties(properties);
            LightHashTable stringProperties = properties.ToStringKeys();
            MUNNetwork.Client.UpdateRoomProperties(stringProperties);
        }
        /// <summary>
        /// Adds a player to the list of available players in the room
        /// </summary>
        public virtual bool AddPlayer(Player player)
        {
            if (!Players.ContainsKey(player.Id))
            {
                Players.Add(player.Id, player);
            }
            else
            {
                RemovePlayer(Players[player.Id]);
                AddPlayer(player);
            }

            player.Room = this;
            return true;
        }
        /// <summary>
        /// Get the player by Id.
        /// </summary>
        /// <param name="id">Id of the searched player</param>
        /// <returns>Wanted player or null - if not found.</returns>
        public virtual Player GetPlayer(int id)
        {
            Players.TryGetValue(id, out Player player);
            return player;
        }
        /// <summary>
        /// Try get the player by Id.
        /// </summary>
        /// <param name="id">Id of the searched player</param>
        /// <param name="player">Wanted player or null - if not found</param>
        /// <returns>True if player with <code>id</code> is available in PlayersList.</returns>
        public virtual bool TryGetPlayer(int id, out Player player)
        {
            return Players.TryGetValue(id, out player);
        }
        public override string ToString()
        {
            return $"{base.ToString()}, Players: {PlayersInRoom}";
        }
        /// <summary>
        /// A method that allows you to assign custom room properties
        /// </summary>
        /// <param name="properties">Custom room properties</param>
        protected internal override void SetupProperties(LightHashTable properties)
        {
            base.SetupProperties(properties);
        }
        /// <summary>
        /// Remove a player from the list of players by their Id.
        /// </summary>
        /// <param name="id">The Id of the player you want to remove from the player list</param>
        protected internal virtual void RemovePlayer(int id)
        {
            Player player = GetPlayer(id);
            RemovePlayer(player);
        }
        /// <summary>
        /// Remove a player from the list of players.
        /// </summary>
        /// <param name="player">The player you want to remove from the player list</param>
        protected internal virtual void RemovePlayer(Player player)
        {
            Players.Remove(player.Id);
            player.Room = null;
        }
    }
}