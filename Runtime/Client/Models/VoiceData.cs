﻿[System.Serializable]
public class VoiceData
{
    /// <summary>
    /// The segment index of the audio samples
    /// </summary>
    public int segmentIndex;
    /// <summary>
    /// The frequency (or sampling rate) of the audio
    /// </summary>
    public int frequency;
    /// <summary>
    /// THe numer of channels in the audio
    /// </summary>
    public int channelCount;
    /// <summary>
    /// A short array representing the audio sample data
    /// </summary>
    public short[] samples;
}
