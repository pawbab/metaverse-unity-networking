﻿namespace MUN.Client
{
    /// <summary>
    /// The class has detailed information about the voice room the player was connecting in.
    /// </summary>
    /// <remarks>
    /// <list type="bullet">
    /// <item>Unique RoomId</item>
    /// </list>
    /// </remarks>
    public class VoiceRoomData
    {
        /// <summary>
        /// A flag for the unique voice room identifier property.
        /// </summary>
        public string RoomId { get; }

        /// <summary>
        /// The maximum number of players allowed to join a room.
        /// </summary>
        public byte MaxPlayers { get; }

        protected VoiceRoomData(string roomId, byte maxPlayers)
        {
            RoomId = roomId;
            MaxPlayers = maxPlayers;
        }
    }
}