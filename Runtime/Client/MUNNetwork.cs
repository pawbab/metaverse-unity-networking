﻿using System;
using MUN.Client.Extensions;
using System.Collections.Generic;
using MUN.Client.VoiceChat;
using UnityEngine;

namespace MUN.Client
{
    /// <summary>
    /// The main class responsible for communication between the client and the MasterServer.
    /// </summary>
    /// <remarks>
    /// <list type="bullet">
    /// <listheader>The class enables:</listheader>
    /// <item>Get information about the current room in which the player is</item>
    /// <item>Local player access</item>
    /// <item>Access to customer status</item>
    /// <item>Change the name of the local player</item>
    /// <item>Access to the application created on the website</item>
    /// <item>Access to many built-in functions for Player / Players / Room / Object Management and more.</item>
    /// </list>
    /// </remarks>
    public static partial class MUNNetwork
    {
        private static readonly MUNPackage packetWrite = MUNPackage.MUNPackageWrite();
        private static readonly MUNPackage packetRead = MUNPackage.MUNPackageRead();

        private static Dictionary<string, byte> MUNRPCs = new Dictionary<string, byte>();
        private static readonly List<string> MUNRPCsList = new List<string>();

        /// <summary>
        /// Count of created MUNViews via LocalPlayer
        /// </summary>
        internal static int CreatedMUNViews => MUNViewsService.GetNextTemporaryMUNViewId(false);
        internal static MUNClient Client { get; private set; }
        private static IMUNViewService MUNViewsService { get; }

        static MUNNetwork()
        {
            MUNViewsService = new MUNViewService();
            Setup();
        }
        public static void RegisterCallbacks(object target)
        {
            if (target == null
                || target is MUNView
                || Client == null) return;

            Client.RegisterCallbacks(target);
        }
        public static void UnregisterCallbacks(object target)
        {
            if (target == null
               || target is MUNView
               || Client == null) return;

            Client.UnregisterCallbacks(target);
        }
        internal static MUNView GetMUNView(int viewId)
        {
            return MUNViewsService.GetMUNView(viewId);
        }

        internal static IEnumerable<MUNView> GetMUNViewsByOwner(int ownerId, bool includeServerObjects)
        {
            return MUNViewsService.GetMUNViewsByOwner(ownerId, includeServerObjects);
        }
        internal static IEnumerable<MUNView> GetAllMUNViews(bool includeServerObjects)
        {
            return MUNViewsService.GetAllMUNViews(includeServerObjects);
        }
        internal static void UpdateViews()
        {
            MUNViewsService.UpdateViews();
        }

        internal static void RegisterMUNView(MUNView view)
        {
            MUNViewsService.RegisterMUNView(view);
        }
        internal static void RemoveMUNViewLocaly(MUNView view)
        {
            MUNViewsService.RemoveMUNViewLocaly(view);
        }

        private static void Setup()
        {
            MUNSettings munSettings = MUNSettings.Get();

            string appId = munSettings.AppID;
            Client = MUNClient.CreateAsAsync(appId);

            MUNRPCs = new Dictionary<string, byte>(munSettings.RPCs.Count);
            string rpcName;
            for (byte id = 0; id < munSettings.RPCs.Count && id < byte.MaxValue; id++)
            {
                rpcName = munSettings.RPCs[id];
                MUNRPCs[rpcName] = id;
                MUNRPCsList.Add(rpcName);
            }

            MUNLoop.onLoopEvent += WaitingSendRPCs;
            MUNLoop.onLoopEvent += WaitingRecieveRPCs;
            MUNSceneManager.onSceneChangedEvent += WaitForChangedSceneForCreatingNetworkObjects;
        }

        /// <summary>
        /// Write all the data about MUNView to packet.
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        private static Packet SerializeMUNWrite(MUNView view)
        {
            view.serializationHelper.syncValues = Array.Empty<object>();

            packetWrite.SetWriteData();
            packetWrite.Write((byte)ClientSendPackets.SendSerialization);
            packetWrite.Write(CurrentRoom.RoomId);
            packetWrite.Write(view.Id);

            int startCount = packetWrite.Count;
            view.SerializeView(packetWrite);

            //Do not send packet if any data has not been added to them
            if (packetWrite.Count <= startCount)
                return null;

            object[] currentWriteData = packetWrite.GetWriteDataItemsHelper;
            bool ignoreSerialize = (view.serializationHelper.ignoredSerialize < MUNNetwork.Application.TickRate * Consts.BYTE_FOUR) && currentWriteData.AreEquals(view.serializationHelper.lastSyncValues);

            if (ignoreSerialize)
            {
                view.serializationHelper.ignoredSerialize++;
                return null;
            }

            view.serializationHelper.ignoredSerialize = 0;
            object[] temp = view.serializationHelper.lastSyncValues;
            view.serializationHelper.lastSyncValues = currentWriteData;
            view.serializationHelper.syncValues = temp;

            return packetWrite.GetData;
        }
        /// <summary>
        /// Receive packet containing data related to MUNView object.
        /// </summary>
        private static void SerializeMUNRead(Packet data)
        {
            int viewId = data.ReadInt();
            MUNView view = GetMUNView(viewId);

            if (view is null || view.IsMine) return;

            packetRead.SetReadData(data);
            view.DeserializeView(packetRead);
        }
        /// <summary>
        /// Receive packet containing data related to voice chat.
        /// </summary>
        private static void SerializeMUNVoiceRead(Packet data)
        {
            int playerId = data.ReadInt();
            if (MUNNetwork.CurrentRoom.TryGetPlayer(playerId, out var player))
            {
                var voiceDataBytes = data.ReadBytes(data.UnreadLength);
                var voiceData = voiceDataBytes.ToObject<VoiceData>();

                player.Speaker?.ReadVoice(in voiceData);
            }
        }
    }
}
