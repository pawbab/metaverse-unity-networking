﻿namespace MUN.Client.Interfaces
{
    /// <summary>
    /// Interface containing calls to callbacks that will be called when connecting to MasterServer.
    /// </summary>
    public interface IMUNConnection : IMUNCallbacks
    {
        /// <summary>
        /// Called when the client joins the MasterServer.
        /// </summary>
        /// <remarks>
        /// The application logic should start with a connection to the MasterServer. Without connection, the client will not be able to perform other connection operations.
        /// The method allows you to diagnose whether MasterServer is available.
        /// The method is only executed after connecting to the MasterServer. It will not be executed when the user leaves the room
        /// </remarks>
        void OnConnectedToMaster();

        /// <summary>
        /// Called when the MasterServer refuses the connection
        /// </summary>
        /// <remarks>
        /// It will only invoke when MasterServer is available but will reject the client for some reason. 
        /// An example is the number of clients allowed in an application being exceeded.
        /// </remarks>
        /// <param name="message">Error message from the MasterServer</param>
        void OnConnectedToMasterFailed(string message);
        /// <summary>
        /// Called after disconnecting from the MasterServer.
        /// </summary>
        /// <remarks>
        /// For the intended purpose or if the connection is lost
        /// </remarks>
        void OnDisconnectedFromMaster();
    }
}
