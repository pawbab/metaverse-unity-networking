﻿using UnityEngine.SceneManagement;

namespace MUN.Client.Interfaces
{
    /// <summary>
    /// Interface containing calls to callbacks that will be called when the client is in the room.
    /// </summary>
    public interface IMUNInRoom : IMUNCallbacks
    {
        /// <summary>
        /// Called when a remote player entered the room. MUN previously added a player to the player list.
        /// </summary>
        /// <remarks>
        /// Called on all players who have previously been in the room.
        /// It can be useful to configure the settings of a given player or to send specific information to them.
        /// </remarks>
        /// <param name="player">The player who has joined the room</param>
        void OnPlayerJoinedRoom(Player player);
        /// <summary>
        /// Called when the remote player has left the room.
        /// </summary>
        /// <param name="player">The player who left the room</param>
        void OnPlayerLeftRoom(Player player);
        /// <summary>
        /// Called on changing custom room properties.
        /// </summary>
        /// <remarks>
        /// <code>changedProperties</code> contains values set in <see cref="Room.SetCustomProperties(LightHashTable)"/>
        /// Only RoomOwner can change room properties
        /// </remarks>
        void OnRoomPropertiesChanged(LightHashTable changedProperties);
        /// <summary>
        /// Called on changing custom player properties.
        /// </summary>
        /// <remarks>
        /// If you are not RoomOwner you can change only own properties (LocalPlayer). 
        /// Only RoomOwner can change all custom properties.
        /// </remarks>
        /// <param name="player">The player who change properties</param>
        void OnPlayerPropertiesChanged(Player player, LightHashTable changedProperties);
        /// <summary>
        /// Called after switching to the new RoomOwner while the current one leaves.
        /// </summary>
        /// <remarks>
        /// The previous RoomOwner may still be in the player list when this method is called.
        /// RoomOwner may designate a new room owner at any time.
        /// </remarks>
        /// <param name="newOwner">New RoomOwner</param>
        void OnRoomOwnerChanged(Player newOwner);
        /// <summary>
        /// Called after the scene changed in the room.
        /// </summary>
        /// <remarks>
        /// Only RoomOwner has the ability to change the scene via the network.
        /// The other clients in the room can only receive information about the change
        /// and this callback will be called for them too.
        /// </remarks>
        /// <param name="loadedScene">Current loaded scene</param>
        void OnRoomChangeScene(Scene loadedScene);
    }
}
