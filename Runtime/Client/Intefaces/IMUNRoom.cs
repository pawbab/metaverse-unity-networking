﻿using System.Collections.Generic;

namespace MUN.Client.Interfaces
{
    /// <summary>
    /// Interface containing callbacks that will be called after joining the MasterServer and before connecting to the room.
    /// They allow you to manage rooms.
    /// </summary>
    public interface IMUNRoom : IMUNCallbacks
    {
        /// <summary>
        /// Called when the client has created a room. It will automatically be attached to the room where the <see cref="OnJoinedRoom"/> method will be called.
        /// </summary>
        /// <remarks>
        /// Calling only on the client who created the room.
        /// If you need specific properties of the room, add claims when creating it.
        /// </remarks>
        void OnRoomCreated();
        /// <summary>
        /// Called when the server failed to create a room
        /// </summary>
        /// <remarks>
        /// The most common cause of room creation failure is an incorrect room name or network connection problems.
        /// Check if the client is connected to the MasterServer - (<see cref="MUNNetwork.Status"/>).
        /// </remarks>
        /// <param name="message">Error message from the MasterServer</param>
        void OnRoomCreatedFailed(string message);
        /// <summary>
        /// Called when Client joined the room, regardless of whether the client created it or just joined.
        /// </summary>
        /// <remarks>
        /// Provides access to players in <code>MUNNetwork.CurrentRoom</code> <see cref="Room.Players"/>, their properties and room properties.
        /// In this callback, you can create network objects.
        /// </remarks>
        void OnJoinedRoom();
        /// <summary>
        /// Called when it failed to join a room.
        /// </summary>
        /// <remarks>
        /// This may be because the room is overcrowded or you are trying to join a room that no longer exists.
        /// </remarks>
        /// <param name="message">Error message from the MasterServer</param>
        void OnJoinedRoomFailed(string message);
        /// <summary>
        /// Called when the local client has left the room
        /// </summary>
        /// <remarks>
        /// The client's status changes from InRoom to ConnectedToMaster.
        /// </remarks>
        void OnLeftRoom();
        /// <summary>
        /// Called when updating rooms with client status = ConnectedToMaster.
        /// Called after joining the MasterServer and after leaving the room.
        /// </summary>
        /// <remarks>
        /// Each <see cref="RoomData"/> item contains information about the room and can contain its own properties - <see cref="RoomData.Properties"/>
        /// Not all rooms are available on the list. Some are hidden, closed or full.
        /// </remarks>
        /// <param name="rooms">Enumerated type with a list of available rooms</param>
        void OnRoomListUpdated(IEnumerable<RoomData> rooms);
    }
}
