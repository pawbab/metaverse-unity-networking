namespace MUN.Client.Interfaces
{
    /// <summary>
    /// An interface that implements two methods responsible for easy serialization of individual MUNView parameters.
    /// </summary>
    public interface IMUNSerialize
    {
        /// <summary>
        /// MUNView writer function called every TickRate when there are at least two players in the room. This function is only performed by the owner of the object.
        /// </summary>
        void MUNSerializeWrite(MUNPackage packet);

        /// <summary>
        /// Function that reads MUNView data, called every TickRate, when there are at least two players in the room. This function is performed by everyone behind the washing machine of the owner of the facility.
        /// </summary>
        void MUNSerializeRead(MUNPackage packet);
    }
}
