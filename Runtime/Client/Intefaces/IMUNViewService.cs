﻿using System.Collections.Generic;
using UnityEngine;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        private interface IMUNViewService
        {
            int GetNextTemporaryMUNViewId(bool increaseValue);
            MUNView GetMUNView(int viewId);
            IEnumerable<MUNView> GetMUNViewsByOwner(int ownerId, bool includeServerObjects);
            IEnumerable<MUNView> GetAllMUNViews(bool includeServerObjects);
            MUNView Create(string prefabId, Vector3 position, Quaternion rotation);
            void Destroy(MUNView gameObject);
            void Destroy(MUNView[] gameObject);
            void RegisterMUNView(MUNView view);
            void UpdateViews();
            void RemoveMUNViewLocaly(MUNView view);
        }
    }
}
