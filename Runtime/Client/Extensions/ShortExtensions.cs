﻿using static MUN.Client.MUNNetwork;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the short class with additional methods
    /// </summary>
    public static class ShortExtensions
    {
        /// <summary>
        /// Convert short value to float
        /// </summary>
        public static float ToFloat(this short value)
        {
            return value / Consts.FLOAT_TO_SHORT_CONVERSION_MULTIPLER;
        }

        public static float[] ToFloat(this short[] values)
        {
            float[] result = new float[values.Length];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = values[i].ToFloat();
            }

            return result;
        }
    }
}
