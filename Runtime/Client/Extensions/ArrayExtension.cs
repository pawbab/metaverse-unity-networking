﻿namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the object array with additional methods
    /// </summary>
    public static class ArrayExtension
    {
        /// <summary>
        /// A method that compares object[] to the second object[].
        /// </summary>
        /// <param name="listB">second object array</param>
        /// <returns>
        /// True if both arrays have the same number of elements, 
        /// and all elements corresponding to each other with the same index
        /// are of the same type and have the same value (taking into account possible precision)</returns>
        public static bool AreEquals(this object[] listA, object[] listB)
        {
            if (listA == null && listB == null) return true;
            if (listA == null || listB == null || listA.Length != listB.Length) return false;

            for (int i = 0; i < listA.Length; i++)
            {
                object newObj = listA[i];
                object oldObj = listB[i];

                if (!newObj.AreEquals(oldObj)) return false;
            }

            return true;
        }
    }
}
