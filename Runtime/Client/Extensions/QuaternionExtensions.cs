﻿using UnityEngine;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the Quaternion class with additional methods
    /// </summary>
    public static class QuaternionExtensions
    {
        /// <summary>
        /// A method that compares Quaternion to the second object. In the case of type matching, precision is also compared
        /// </summary>
        /// <remarks>
        /// The method used to optimize the data sent to the server. If both values are not changed (or slightly changed) then serialization will not be performed.
        /// The system sends the synchronization after a specified time interval.
        /// </remarks>
        /// <param name="two">Second object you want to check for equality</param>
        /// <returns>True if both objects are of the same type and their difference (distance) is not greater than the specified precision</returns>
        public static bool AreEquals(this Quaternion quaternionA, object two, float precision)
        {
            if (!typeof(Quaternion).Equals(two.GetType())) return false;
            Quaternion quaternionB = (Quaternion)two;

            return Quaternion.Angle(quaternionA, quaternionB) < precision;

        }
    }
}
