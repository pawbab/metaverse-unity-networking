using System;
using System.Collections.Generic;
using System.Reflection;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the Type class with additional methods
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Get list of <see cref="MethodInfo"/> having at least one attribute or any of its derived types is used in this object
        /// </summary>
        /// <param name="attrbitute">type you are looking for</param>
        /// <returns>true if at least one attribute or any of its derived types is used in this object</returns>
        public static List<MethodInfo> GetMethods(this Type type, Type attrbitute)
        {
            List<MethodInfo> list = new List<MethodInfo>();

            if (type == null) return list;

            MethodInfo[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (MethodInfo method in methods)
            {
                if (attrbitute == null || method.IsDefined(attrbitute, false))
                    list.Add(method);
            }

            return list;
        }
    }
}