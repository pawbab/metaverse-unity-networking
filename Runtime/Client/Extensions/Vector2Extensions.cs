﻿using UnityEngine;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the Vector2 class with additional methods
    /// </summary>
    public static class Vector2Extensions
    {
        /// <summary>
        /// A method that compares Vector2 to the second object. In the case of type matching, precision is also compared
        /// </summary>
        /// <remarks>
        /// The method used to optimize the data sent to the server. If both values are not changed (or slightly changed) then serialization will not be performed.
        /// The system sends the synchronization after a specified time interval.
        /// </remarks>
        /// <param name="two">Second object you want to check for equality</param>
        /// <returns>True if both objects are of the same type and their difference (distance) is not greater than the specified precision</returns>
        public static bool AreEquals(this Vector2 vectorA, object two, float precision)
        {
            if (!typeof(Vector2).Equals(two.GetType())) return false;
            Vector2 vectorB = (Vector2)two;

            return (vectorA - vectorB).sqrMagnitude < precision;

        }
    }
}
