﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the object class with additional methods
    /// </summary>
    public static class ObjectExtension
    {
        /// <summary>
        /// A method that compares object to the second object.
        /// </summary>
        /// <remarks>
        /// The method used to optimize the data sent to the server. If both values are not changed (or slightly changed) then serialization will not be performed.
        /// The system sends the synchronization after a specified time interval.
        /// </remarks>
        /// <param name="two">Second object you want to check for equality</param>
        /// <returns>
        /// True if both objects are of the same type and are equals.
        /// For some types the precision is also verified
        /// </returns>
        public static bool AreEquals(this object one, object two)
        {
            if (one == null || two == null)
                return one == null && two == null;

            if (!one.Equals(two))
            {
                if (one is Vector3)
                {
                    Vector3 vectorA = (Vector3)one;
                    if (vectorA.AreEquals(two, MUNNetwork.Consts.VECTOR_SYNCHRONIZATION_PRECISSION)) return true;
                }
                else if (one is Vector2)
                {
                    Vector2 vectorA = (Vector2)one;
                    if (vectorA.AreEquals(two, MUNNetwork.Consts.VECTOR_SYNCHRONIZATION_PRECISSION)) return true;

                }
                else if (one is Quaternion)
                {
                    Quaternion quaternionA = (Quaternion)one;
                    if (quaternionA.AreEquals(two, MUNNetwork.Consts.QUATERNION_SYNCHRONIZATION_PRECISSION)) return true;
                }
                else if (one is float)
                {
                    float valueA = (float)one;
                    if (valueA.AreEquals(two, MUNNetwork.Consts.FLOAT_SYNCHRONIZATION_PRECISSION)) return true;
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Converts a serializable object to an array of bytes.
        /// </summary>
        /// <param name="obj">The object to be serialized into the byte array</param>
        /// <returns>an array of bytes of a serialized object</returns>
        public static byte[] ToByteArray(this object obj, bool compress = true)
        {
            if (obj == null) return null;

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            byte[] bytes = ms.ToArray();
            if (compress)
                bytes = bytes.Compress();

            return bytes;
        }
    }
}
