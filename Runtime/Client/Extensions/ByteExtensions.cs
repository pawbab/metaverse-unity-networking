using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;

namespace MUN.Client.Extensions
{
    /// <summary>
    /// Extending the byte class with additional methods
    /// </summary>
    public static class ByteExtensions
    {
        /// <summary>
        /// Attempts to cast the byte array to the object
        /// </summary>
        /// <typeparam name="T">The type of object you are trying to cast to</typeparam>
        /// <param name="bytes">an object written in bytes</param>
        public static T ToObject<T>(this byte[] bytes, bool decompress = true)
        {
            if (decompress)
                bytes = bytes.Decompress();

            MemoryStream memoryStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memoryStream.Write(bytes, 0, bytes.Length);
            memoryStream.Seek(0, SeekOrigin.Begin);
            object obj = binForm.Deserialize(memoryStream);
            return (T)obj;
        }

        public static byte[] Compress(this byte[] bytes)
        {
            MemoryStream output = new MemoryStream();
            using (DeflateStream dstream = new DeflateStream(output, CompressionLevel.Optimal))
            {
                dstream.Write(bytes, 0, bytes.Length);
            }

            return output.ToArray();
        }

        public static byte[] Decompress(this byte[] bytes)
        {
            MemoryStream input = new MemoryStream(bytes);
            MemoryStream output = new MemoryStream();

            using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }

            return output.ToArray();
        }
    }
}