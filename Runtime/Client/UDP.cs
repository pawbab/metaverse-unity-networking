﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private class UDP : MUNSocket
            {
                private UdpClient socket;
                public IPEndPoint endPoint;

                private int loopsFromLastSendPackage = 0;
                private readonly byte MUN_LOOP_UPDATE;

                protected internal override ProtocolType ProtocolType => ProtocolType.UDP;

                public UDP(MUNClient client, Dictionary<byte, PacketHandler> packetsHandler, string ip, int port) : base(client, packetsHandler, ip, port)
                {
                    endPoint = new IPEndPoint(ConnectionString.Ip, ConnectionString.Port);
                    MUN_LOOP_UPDATE = (byte)(MUNNetwork.Application.TickRate * Consts.LOOPS_TO_UDP_CONNECT);
                    Connect();
                }

                protected internal override void CloseSocket()
                {
                    socket?.Close();
                }
                protected internal override void SendData(Packet packet)
                {
                    try
                    {
                        if (socket == null)
                        {
                            MUNLogs.ShowException(new NullReferenceException("Socket is null"));
                            return;
                        }
                        //LocalPlayer.Id to byte
                        packet.InsertByte((byte)MUNNetwork.LocalPlayer.Id);

                        if (packet.Length > Consts.MAX_UDP_PACKAGE_SIZE)
                        {
                            MUNLogs.ShowException(new OutOfMemoryException($"Too large a package size {packet.Length} > {Consts.MAX_UDP_PACKAGE_SIZE}"));
                            return;
                        }
                        socket.BeginSend(packet.ToArray(), packet.Length, null, null);
                        loopsFromLastSendPackage = 0;
                    }
                    catch (Exception ex) {
                        MUNLogs.ShowError($"Error sending data to server via {ProtocolType}: {ex.Message}.");
                    }
                }

                protected override void Connect()
                {
                    MUNLoop.onLoopEvent += OnMUNLoop;

                    socket = new UdpClient();
                    socket.DontFragment = true;
                    socket.Connect(endPoint);
                    socket.BeginReceive(ReceiveCallback, null);

                    loopsFromLastSendPackage = 0;
                }

                private void OnMUNLoop()
                {
                    loopsFromLastSendPackage++;

                    if (loopsFromLastSendPackage < MUN_LOOP_UPDATE && loopsFromLastSendPackage <= byte.MaxValue)
                        return;

                    SendData(new Packet((byte)ClientSendPackets.UDPConnecting));
                }

                protected override void Disconnect()
                {
                    MUNLoop.onLoopEvent -= OnMUNLoop;
                    client.Disconnect();
                    endPoint = null;
                    socket = null;
                }
                protected override bool HandleData(byte[] data)
                {
                    if (data.Length < Consts.MIN_DATA_LENGTH_REQUIRED)
                    {
                        MUNLogs.ShowError($"Data length ({data.Length}) < {Consts.MIN_DATA_LENGTH_REQUIRED}");
                        return false;
                    }

                    ThreadManager.ExecuteOnMainThread(() =>
                    {
                        using (Packet packet = new Packet(data))
                        {
                            byte packetId = packet.ReadByte();

                            if (packetsHandler.TryGetValue(packetId, out PacketHandler handler))
                            {
                                handler(packet);
                            }
                            else
                                MUNLogs.ShowError($"Unknown Packet id = ({packetId}) from server.");
                        }
                    });

                    return true;
                }
                protected override void ReceiveCallback(IAsyncResult result)
                {
                    try
                    {
                        byte[] data = socket.EndReceive(result, ref endPoint);
                        socket.BeginReceive(ReceiveCallback, null);

                        if (data.Length < 4)
                        {
                            MUNLogs.ShowException(new Exception("End of data"));
                            return;
                        }
                        HandleData(data);
                    }
                    catch
                    {
                        Disconnect();
                    }
                }
            }
        }
    }
}
