using System;
using System.Collections.Generic;
using System.Linq;

namespace MUN.Client
{
    /// <summary>
    /// The repository responsible for storing RPC methods
    /// </summary>
    /// <remarks>
    /// <see cref="MUNNetwork.RPCTargets"/> with the Cache parameter store the calls for new players in the room in memory.
    /// If RPC data (same method for the same <see cref="MUNView"/>) is called multiple times, it is overwritten in the repository.
    /// </remarks>
    internal class MUNRPCRepository : IMunRPCRepository
    {
        private readonly HashSet<RPCTuple> repositoryCache;

        internal MUNRPCRepository()
        {
            repositoryCache = new HashSet<RPCTuple>();
        }

        /// <summary>
        /// Adds RPC data to the repository for a specific method, a particular MUNView
        /// </summary>
        /// <param name="rpcData">Data sent by RPC</param>
        /// <param name="methodName">The name of the method to invoke</param>
        /// <param name="munViewId">MUNView Id that has an RPC method</param>
        public void AddRPC(LightHashTable rpcData, string methodName, int munViewId)
        {
            RPCTuple foundCache = repositoryCache.FirstOrDefault(x => x.MunViewId == munViewId && x.MethodName == methodName);
            RPCTuple data = new RPCTuple(munViewId, methodName, rpcData.Clone());

            if (foundCache != default)
                repositoryCache.Remove(foundCache);

            repositoryCache.Add(data);
        }

        /// <summary>
        /// Clears memorized RPCs
        /// </summary>
        public void Clear()
        {
            repositoryCache.Clear();
        }

        /// <summary>
        /// Removes all RPCs for the selected MUNView.
        /// </summary>
        /// <param name="munViewId">MUNView Id for which you want to delete saved RPCs</param>
        public void RemoveRPCs(int munViewId)
        {
            lock (repositoryCache)
            {
                repositoryCache.RemoveWhere(x => x.MunViewId == munViewId);
            }
        }

        /// <summary>
        /// Get all remembered RPCs
        /// </summary>
        /// <returns>Enumerated type with remembered RPCs</returns>
        IEnumerable<RPCTuple> IMunRPCRepository.GetAllRPCs()
        {
            return repositoryCache;
        }


    }

    /// <summary>
    /// Interface implementing the methods responsible for managing the repository with RPC methods
    /// </summary>
    internal interface IMunRPCRepository
    {
        /// <summary>
        /// Adds RPC data to the repository for a specific method, a particular MUNView
        /// </summary>
        /// <param name="rpcData">Data sent by RPC</param>
        /// <param name="methodName">The name of the method to invoke</param>
        /// <param name="munViewId">MUNView Id that has an RPC method</param>
        void AddRPC(LightHashTable rpcData, string methodName, int munViewId);

        /// <summary>
        /// Removes all RPCs for the selected MUNView.
        /// </summary>
        /// <param name="munViewId">MUNView Id for which you want to delete saved RPCs</param>
        void RemoveRPCs(int munViewId);

        /// <summary>
        /// Get all remembered RPCs
        /// </summary>
        /// <returns>Enumerated type with remembered RPCs</returns>
        IEnumerable<RPCTuple> GetAllRPCs();

        /// <summary>
        /// Clears memorized RPCs
        /// </summary>
        void Clear();

    }

    internal class RPCTuple : Tuple<int, string, LightHashTable>
    {
        public RPCTuple(int munViewId, string methodName, LightHashTable rpcData) : base(munViewId, methodName, rpcData)
        {
        }

        public int MunViewId => base.Item1;
        public string MethodName => base.Item2;
        public LightHashTable RpcData => base.Item3;
    }
}
