﻿using MUN.Client.Interfaces;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private sealed class MUNConnectionContainer : MUNContainerBase<IMUNConnection>, IMUNConnection
            {
                public MUNConnectionContainer(MUNClient client) : base(client) { }

                public void OnConnectedToMaster()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNConnection target in this)
                    {
                        target.OnConnectedToMaster();
                    }
                }

                public void OnConnectedToMasterFailed(string message)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNConnection target in this)
                    {
                        target.OnConnectedToMasterFailed(message);
                    }
                }

                public void OnDisconnectedFromMaster()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNConnection target in this)
                    {
                        target.OnDisconnectedFromMaster();
                    }
                }
            }
        }
    }
}
