﻿using MUN.Client.Interfaces;
using UnityEngine.SceneManagement;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private sealed class MUNInRoomContainer : MUNContainerBase<IMUNInRoom>, IMUNInRoom
            {
                public MUNInRoomContainer(MUNClient client) : base(client) { }

                public void OnPlayerJoinedRoom(Player player)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNInRoom target in this)
                    {
                        target.OnPlayerJoinedRoom(player);
                    }
                }

                public void OnPlayerLeftRoom(Player player)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNInRoom target in this)
                    {
                        target.OnPlayerLeftRoom(player);
                    }
                }

                public void OnPlayerPropertiesChanged(Player player, LightHashTable changedProperties)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNInRoom target in this)
                    {
                        target.OnPlayerPropertiesChanged(player, changedProperties);
                    }
                }

                public void OnRoomChangeScene(Scene loadedScene)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNInRoom target in this)
                    {
                        target.OnRoomChangeScene(loadedScene);
                    }
                }

                public void OnRoomOwnerChanged(Player newOwner)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNInRoom target in this)
                    {
                        target.OnRoomOwnerChanged(newOwner);
                    }
                }

                public void OnRoomPropertiesChanged(LightHashTable changedProperties)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNInRoom target in this)
                    {
                        target.OnRoomPropertiesChanged(changedProperties);
                    }
                }
            }
        }
    }
}
