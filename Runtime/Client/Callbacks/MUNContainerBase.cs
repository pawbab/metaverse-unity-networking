﻿using MUN.Client.Interfaces;
using System.Collections.Generic;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private abstract class MUNContainerBase<T> : List<T>
                where T : IMUNCallbacks
            {
                protected readonly MUNClient client;
                protected MUNContainerBase(MUNClient client)
                {
                    this.client = client;
                }

            }
        }
    }
}
