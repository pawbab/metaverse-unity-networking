﻿namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private class CallbacksToUpdate
            {
                public readonly object Target;
                public readonly CallbackUpdateType UpdateType;

                public CallbacksToUpdate(object target, CallbackUpdateType updateType)
                {
                    Target = target;
                    UpdateType = updateType;
                }

                public enum CallbackUpdateType
                {
                    Add, Remove
                }
            }
        }
    }
}
