﻿using MUN.Client.Interfaces;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private sealed class MUNVoiceChatContainer : MUNContainerBase<IMUNVoice>, IMUNVoice
            {
                public MUNVoiceChatContainer(MUNClient client) : base(client) { }

                public void OnConnectedToVoiceChat()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNVoice target in this)
                    {
                        target.OnConnectedToVoiceChat();
                    }
                }

                public void OnDisconnectedFromVoiceChat()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNVoice target in this)
                    {
                        target.OnDisconnectedFromVoiceChat();
                    }
                }

                public void OnJoinedVoiceRoom()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNVoice target in this)
                    {
                        target.OnJoinedVoiceRoom();
                    }
                }

                public void OnLeaveVoiceRoom()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNVoice target in this)
                    {
                        target.OnLeaveVoiceRoom();
                    }
                }

                public void OnPlayerJoinedVoiceChat(Player player)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNVoice target in this)
                    {
                        target.OnPlayerJoinedVoiceChat(player);
                    }
                }

                public void OnPlayerLeftVoiceChat(Player player)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNVoice target in this)
                    {
                        target.OnPlayerLeftVoiceChat(player);
                    }
                }
            }
        }
    }
}