﻿using MUN.Client.Interfaces;
using System.Collections.Generic;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        internal partial class MUNClient
        {
            private sealed class MUNRoomContainer : MUNContainerBase<IMUNRoom>, IMUNRoom
            {
                public MUNRoomContainer(MUNClient client) : base(client) { }

                public void OnRoomCreated()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNRoom target in this)
                    {
                        target.OnRoomCreated();
                    }
                }

                public void OnRoomCreatedFailed(string message)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNRoom target in this)
                    {
                        target.OnRoomCreatedFailed(message);
                    }
                }

                public void OnJoinedRoom()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNRoom target in this)
                    {
                        target.OnJoinedRoom();
                    }
                }

                public void OnJoinedRoomFailed(string message)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNRoom target in this)
                    {
                        target.OnJoinedRoomFailed(message);
                    }
                }

                public void OnLeftRoom()
                {
                    client.UpdateCallbacks();
                    foreach (IMUNRoom target in this)
                    {
                        target.OnLeftRoom();
                    }
                }

                public void OnRoomListUpdated(IEnumerable<RoomData> rooms)
                {
                    client.UpdateCallbacks();
                    foreach (IMUNRoom target in this)
                    {
                        target.OnRoomListUpdated(rooms);
                    }
                }
            }
        }
    }
}
