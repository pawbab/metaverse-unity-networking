﻿using MUN.Client.Extensions;
using MUN.Client.VoiceChat;
using MUN.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using static MUN.Service.ServiceRequest;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        public static event Action<ClientStatus> onClientStatusChangedEvent;
        public static event Action<ClientVoiceStatus> onClientVoiceStatusChangedEvent;

        internal partial class MUNClient
        {
            private static MUNClient ClientInstance;
            private readonly Queue<Tuple<Packet, ProtocolType>> PacketsInLoop = new Queue<Tuple<Packet, ProtocolType>>();

            private readonly IMunRPCRepository MUNRPCRepository;
            private readonly Queue<CallbacksToUpdate> callbacksToUpdate = new Queue<CallbacksToUpdate>();
            private readonly HashSet<object> callbacks = new HashSet<object>();

            private readonly MUNConnectionContainer MUNConnectionCallbacks;
            private readonly MUNRoomContainer MUNRoomCallbacks;
            private readonly MUNInRoomContainer MUNInRoomCallbacks;
            private readonly MUNViewContainer MUNViewCallbacks;
            private readonly MUNVoiceChatContainer MUNVoiceChatCallbacks;

            /// <summary>
            /// Reference to local client data
            /// </summary>
            /// <remarks>
            /// It facilitates access to the player's own properties and name.
            /// When a client joins a room, these values are synchronized with other clients in the room.
            /// </remarks>
            public Player LocalPlayer { get; private set; }
            /// <summary>
            /// A reference to the room where the <see cref="LocalPlayer"/> is currently located.
            /// In case the local player is not in the room - it returns null
            /// </summary>
            public Room CurrentRoom { get; private set; }

            /// <summary>
            /// A reference to the room where the <see cref="LocalPlayer"/> is currently located.
            /// In case the local player is not in the room - it returns null
            /// </summary>
            public VoiceRoom CurrentVoiceRoom { get; private set; }
            /// <summary>
            /// Simplify the reading if a local player is in the room
            /// </summary>
            public bool InRoom => ClientStatus == ClientStatus.InRoom && CurrentRoom != null;
            public bool InVoiceRoom => ClientVoiceStatus == ClientVoiceStatus.InRoom && CurrentVoiceRoom != null;

            private MUNClient()
            {
                /*Callbacks First:*/
                MUNConnectionCallbacks = new MUNConnectionContainer(this);
                MUNRoomCallbacks = new MUNRoomContainer(this);
                MUNInRoomCallbacks = new MUNInRoomContainer(this);
                MUNViewCallbacks = new MUNViewContainer(this);
                MUNVoiceChatCallbacks = new MUNVoiceChatContainer(this);

                MUNRPCRepository = new MUNRPCRepository();
                ClientInstance = this;
                ClientStatus = ClientStatus.Disconnected;
                LocalPlayer = new Player(name: string.Empty, isLocal: true);
                MUNLoop.onLoopEvent += SendDataInLoop;
            }

            ~MUNClient()
            {
                MUNLoop.onLoopEvent -= SendDataInLoop;
            }

            public static MUNClient CreateAsAsync(string appId)
            {
                if (!UnityEngine.Application.isPlaying) return null;

                MUNClient client = new MUNClient();
                client.Application = ServiceRequest.Application.Get(appId).GetAwaiter().GetResult();
                return client;
            }

            internal void RegisterCallbacks(object target)
            {
                MUNLogs.ShowLog($"RegisterCallbacks: {target}");
                callbacksToUpdate.Enqueue(new CallbacksToUpdate(target, CallbacksToUpdate.CallbackUpdateType.Add));
            }
            internal void UnregisterCallbacks(object target)
            {
                MUNLogs.ShowLog($"UnregisterCallbacks: {target}");
                callbacksToUpdate.Enqueue(new CallbacksToUpdate(target, CallbacksToUpdate.CallbackUpdateType.Remove));
            }

            internal static void AddRPCToCache(LightHashTable rpcData, string methodName, int munViewId)
            {
                ClientInstance.MUNRPCRepository.AddRPC(rpcData, methodName, munViewId);
            }

            internal static void RemoveRPCForMUNView(int munViewId)
            {
                ClientInstance.MUNRPCRepository.RemoveRPCs(munViewId);
            }

            internal bool SendData(Packet packet, ProtocolType protocolType)
            {
                switch (protocolType)
                {
                    case ProtocolType.TCP:
                        tcp.SendData(packet);
                        return true;
                    case ProtocolType.UDP:
                        udp.SendData(packet);
                        return true;
                    case ProtocolType.VoiceChat:
                        voiceTcp.SendData(packet);
                        return true;
                    default:
                        MUNLogs.ShowError("Unknown protocol type.");
                        return false;
                }
            }

            internal bool SendDataInLoop(Packet packet, ProtocolType protocolType)
            {
                PacketsInLoop.Enqueue(new Tuple<Packet, ProtocolType>(packet, protocolType));
                return true;
            }

            private void SendDataInLoop()
            {
                lock (PacketsInLoop)
                {
                    Tuple<Packet, ProtocolType> packetToSend;

                    for (int i = 0; i < Consts.MAX_PACKETS_PER_TICK; i++)
                    {
                        if (PacketsInLoop.Count == 0) break;
                        packetToSend = PacketsInLoop.Dequeue();
                        SendData(packetToSend.Item1, packetToSend.Item2);
                    }

                    if (PacketsInLoop.Count > 0)
                    {
                        MUNLogs.ShowWarning($"Application try to send more packets then can (max = {Consts.MAX_PACKETS_PER_TICK})");
                    }
                }
            }

            internal virtual bool ConnectToMaster(string connectionString)
            {
                if (ClientStatus != ClientStatus.Disconnected)
                {
                    MUNLogs.ShowError($"ConnectToMaster failed. Required status = ClientStatus.Disconnected. Current status = {ClientStatus}");
                    return false;
                }

                string[] connectionInfo = connectionString.Split(':');
                string ip = connectionInfo[0];
                int port = Convert.ToInt32(connectionInfo[1]);

                tcp = new TCP(this, Consts.PACKET_HANDLERS, ip, port);
                udp = new UDP(this, Consts.PACKET_HANDLERS, ip, port);


                ClientStatus = ClientStatus.ConnectingToMasterServer;
                MUNLogs.ShowLog($"Send ConnectToMaster: {true}");
                return true;
            }

            internal virtual bool ConnectToVoiceMaster()
            {
                IServerInfo serverInfo = Client.Application.GetBestRegionOrDefault();
                string[] connectionInfo = serverInfo.IpPort.Split(':');
                string ip = connectionInfo[0];
                int port = Convert.ToInt32(connectionInfo[1]);

                if (MUNNetwork.Application.UseVoiceChat)
                {

                    SetupVoiceTcp(new TCP(this, Consts.PACKET_VOICE_HANDLERS, ip, port + Consts.INCREASE_PORT_FOR_VOICE_MASTERSERVER), ClientVoiceStatus.ConnectingToVoiceMasterServer);
                }
                else
                    SetupVoiceTcp(null, ClientVoiceStatus.Disconnected);


                return true;
                void SetupVoiceTcp(TCP tcpConnection, ClientVoiceStatus voiceStatus)
                {
                    voiceTcp = tcpConnection;
                    ClientVoiceStatus = voiceStatus;
                }
            }

            internal virtual bool CreateRoom(CreateRoomInfo createRoomInfo)
            {
                if (createRoomInfo is null)
                {
                    MUNLogs.ShowError("CreateRoom is null.");
                    return false;
                }

                if (createRoomInfo.Claims is null)
                    createRoomInfo.Claims = new Room.RoomClaims();

                createRoomInfo.Claims.MaxPlayers = (byte)Mathf.Clamp(createRoomInfo.Claims.MaxPlayers, 1, MUNNetwork.Application.MaxPlayers);
                Packet packet = new Packet((byte)ClientSendPackets.CreatingRoom);
                packet.Write(createRoomInfo.Name);
                WriteRoomDataIntoPacket(packet, createRoomInfo.Claims);

                bool isSending = SendData(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send CreateRoom: {isSending}");

                if (isSending)
                    ClientStatus = ClientStatus.CreatingRoom;

                return isSending;
            }

            internal virtual bool CreateOrJoinVoiceRoom(ICreateVoiceRoomInfo createVoiceRoomInfo)
            {
                if (MUNLoop.IsQuit || !MUNNetwork.Application.UseVoiceChat) return false;

                Packet packet = new Packet((byte)ClientVoiceSendPackets.CreatingOrJoiningVoiceRoom);
                packet.Write(createVoiceRoomInfo.RoomName);
                packet.Write(createVoiceRoomInfo.MaxPlayers);

                bool isSending = SendData(packet, ProtocolType.VoiceChat);

                if (isSending)
                    ClientVoiceStatus = ClientVoiceStatus.JoiningIntoRoom;

                return isSending;
            }

            internal virtual bool UpdateingRoomsList()
            {
                Packet packet = new Packet((byte)ClientSendPackets.UpdateingRoomsList);
                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send UpdateingRoomsList: {isSending}");

                return isSending;
            }

            internal virtual bool ChangeRoomOwner(Player newOwner)
            {
                if (newOwner == null) return false;
                if (!LocalPlayer.IsRoomOwner)
                {
                    MUNLogs.ShowError("Only RoomOwner can set new owner.");
                    return false;
                }

                Packet packet = new Packet((byte)ClientSendPackets.ChangeOwner);
                packet.Write(newOwner.Room.RoomId);
                packet.Write(newOwner.Id);

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);

                MUNLogs.ShowLog($"Send ChangeRoomOwner: {isSending}");
                return isSending;
            }

            internal virtual bool JoinOrCreateRandomRoom(Room.RoomClaims claims)
            {
                if (claims is null)
                    claims = new Room.RoomClaims();

                claims.MaxPlayers = (byte)Mathf.Clamp(claims.MaxPlayers, 1, MUNNetwork.Application.MaxPlayers);
                Packet packet = new Packet();

                packet.Write((byte)ClientSendPackets.CreatingOrJoiningRandomRoom);
                PackPlayerPropertiesOnJoin(packet, LocalPlayer);
                WriteRoomDataIntoPacket(packet, claims);

                bool isSending = SendData(packet, ProtocolType.TCP);
                if (isSending) ClientStatus = ClientStatus.CreatingOrJoiningRandomRoom;

                MUNLogs.ShowLog($"Send JoinOrCreateRandomRoom: {isSending}");
                return isSending;
            }

            internal virtual bool JoinRoom(JoinRoomInfo joinRoom)
            {
                if (joinRoom is null)
                {
                    MUNLogs.ShowError("JoinRoom is null.");
                    return false;
                }

                Packet packet = new Packet();

                packet.Write((byte)ClientSendPackets.JoiningRoom);
                packet.Write(joinRoom.RoomId);
                packet.Write(joinRoom.Password);

                PackPlayerPropertiesOnJoin(packet, LocalPlayer);

                bool isSending = SendData(packet, ProtocolType.TCP);
                if (isSending)
                    ClientStatus = ClientStatus.JoiningIntoRoom;

                MUNLogs.ShowLog($"Send JoinRoom: {isSending}");
                return isSending;
            }

            internal virtual void ChangeScene(int sceneBuildIndex, ChangeSceneType changeSceneType, bool destroyRoomObjects)
            {
                if (!MUNNetwork.InRoom)
                {
                    MUNLogs.ShowLog("ChangeScene Failed. Player is not in any room.");
                    return;
                }
                if (!MUNNetwork.LocalPlayer.IsRoomOwner)
                {
                    MUNLogs.ShowLog("ChangeScene Failed. Player is not a RoomOwner.");
                    return;
                }
                if (sceneBuildIndex < 0) return;
                if (SceneManager.GetActiveScene().buildIndex.Equals(sceneBuildIndex))
                {
                    MUNLogs.ShowError("ChangeScene Failed. You are already on this scene.");
                    return;
                }

                if (changeSceneType == ChangeSceneType.NoAsync)
                {
                    ClientStatus = ClientStatus.ChangingScene;
                    MUNSceneManager.LoadScene(sceneBuildIndex, () =>
                    {
                        ClientStatus = ClientStatus.InRoom;
                        Scene loadedScene = SceneManager.GetActiveScene();
                        ClientInstance.MUNInRoomCallbacks.OnRoomChangeScene(loadedScene);
                        RegisterServerMUNViewsOnServer();
                    });
                }


                MUNRPCRepository.Clear();
                DestroyMUNViewsOnChangeScene();

                Packet packet = new Packet((byte)ClientSendPackets.ChangingScene);
                packet.Write(CurrentRoom.RoomId);
                packet.Write(destroyRoomObjects);
                packet.Write(sceneBuildIndex);
                packet.Write(changeSceneType == ChangeSceneType.Async ? true : false);

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send ChangeScene: {isSending}");

                if (isSending)
                {
                    LightHashTable mapProperty = new LightHashTable() { { "Map", sceneBuildIndex } };
                    CurrentRoom.SetCustomProperties(mapProperty);
                }



                void DestroyMUNViewsOnChangeScene()
                {
                    MUNView[] serverMUNViews = MUNView.FindAll(true).Where(x => x.IsServerObject)?.ToArray();
                    if (serverMUNViews.Length > 0)
                        DestroyMUNViews(serverMUNViews, false);
                }
            }

            internal virtual void ChangeScene(string sceneName, ChangeSceneType changeSceneType, bool destroyRoomObjects)
            {
                if (string.IsNullOrEmpty(sceneName))
                {
                    MUNLogs.ShowError("ChangeScene Failed. Invalid value of sceneName");
                    return;
                }

                int sceneBuildIndex = SceneUtility.GetBuildIndexByScenePath(sceneName);
                ChangeScene(sceneBuildIndex, changeSceneType, destroyRoomObjects);
            }

            internal virtual bool CreateObject(MUNView newMUNView, ICreateObjectInfo createObjectInfo, bool sendToOthers)
            {
                if (!MUNNetwork.InRoom)
                {
                    MUNLogs.ShowLog("CreateObject Failed. Player is not in any room.");
                    return false;
                }

                if (string.IsNullOrEmpty(createObjectInfo.PrefabName))
                {
                    MUNLogs.ShowError("CreateObject Failed. Unknown PrefabName");
                    return false;
                }

                Packet packet = new Packet((byte)ClientSendPackets.CreatingObject);
                packet.Write(CurrentRoom.RoomId);
                packet.Write(sendToOthers);
                packet.Write(createObjectInfo.PrefabName);
                packet.Write(createObjectInfo.TempObjectId);
                packet.Write(createObjectInfo.ObjectId);
                packet.Write(createObjectInfo.Position);
                packet.Write(createObjectInfo.Rotation);


                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send CreateObject: {isSending}");

                return isSending;
            }

            internal void DestroyMUNViews(MUNView[] allMUNViews, bool sendToOthers)
            {
                if (!MUNNetwork.InRoom)
                {
                    MUNLogs.ShowLog("DestroyMUNViews Failled. Player is not in any room.");
                    return;
                }

                if (allMUNViews is null || allMUNViews.Length == 0)
                {
                    MUNLogs.ShowError("DestroyMUNViews Failled. allMUNViews to delete are null or array is empty");
                    return;
                }

                Packet packet = new Packet((byte)ClientSendPackets.DestroingObjects);
                packet.Write(CurrentRoom.RoomId);
                packet.Write(sendToOthers);                 //Send Destroy To Others

                int viewToDeleteCount = 0;
                foreach (MUNView munView in allMUNViews)
                {
                    if (munView is null || munView.Id < 1) continue;
                    packet.Write(munView.Id);
                    viewToDeleteCount++;

                    MUNViewCallbacks.OnPreMUNViewDelete(munView);
                }

                if (viewToDeleteCount == 0) return;

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send DestroyMUNViews: {isSending}");

            }

            internal void DestroyMUNView(int munViewId, bool sendToOthers)
            {
                if (!MUNNetwork.InRoom)
                {
                    MUNLogs.ShowLog("DestroyMUNViews Failled. Player is not in any room.");
                    return;
                }

                Packet packet = new Packet((byte)ClientSendPackets.DestroingObjects);
                packet.Write(CurrentRoom.RoomId);
                packet.Write(sendToOthers);                 //Send Destroy To Others
                packet.Write(munViewId);

                MUNView munView = MUNView.Find(munViewId);
                if (munView) MUNViewCallbacks.OnPreMUNViewDelete(munView);

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send DestroyMUNView: {isSending}");
            }

            internal virtual bool LeaveRoom()
            {
                if (CurrentRoom is null)
                {
                    MUNLogs.ShowError("Player is not in any room.");
                    return false;
                }

                if (LocalPlayer.Id <= 0)
                {
                    MUNLogs.ShowError("Incorrect player id. Try to reconnect into masterserver");
                    return false;
                }

                Packet packet = new Packet();
                packet.Write((byte)ClientSendPackets.LeavingRoom);
                packet.Write(CurrentRoom.RoomId);

                bool isSending = SendData(packet, ProtocolType.TCP);
                if (isSending)
                    ClientStatus = ClientStatus.LeavingRoom;

                if (MUNNetwork.Application.UseVoiceChat && MUNNetwork.VoiceStatus > ClientVoiceStatus.ConnectedToVoiceMasterServer)
                    LeaveVoiceRoom();

                MUNLogs.ShowLog($"Send LeaveRoom: {isSending}");
                return isSending;
            }

            internal virtual bool LeaveVoiceRoom()
            {
                Packet packet = new Packet();
                packet.Write((byte)ClientVoiceSendPackets.LeavingRoom);
                packet.Write(CurrentVoiceRoom.RoomId);

                bool isSending = SendData(packet, ProtocolType.VoiceChat);
                if (isSending)
                    ClientVoiceStatus = ClientVoiceStatus.LeavingRoom;

                MUNLogs.ShowLog($"Send LeaveVoiceRoom: {isSending}");
                return isSending;
            }

            protected internal bool UpdateRoomProperties(LightHashTable propertiesToUpdate)
            {
                if (LocalPlayer.Id != CurrentRoom.OwnerId)
                {
                    MUNLogs.ShowError("Only owner  can update room properties.");
                    return false;
                }

                Packet packet = new Packet();
                packet.Write((byte)ClientSendPackets.SendRoomProperties);
                packet.Write(CurrentRoom.RoomId);
                PackPropertiesIntoPacket(packet, propertiesToUpdate);

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send UpdateRoomProperties: {isSending}");

                if (isSending)
                {
                    MUNInRoomCallbacks.OnRoomPropertiesChanged(propertiesToUpdate);
                }

                return isSending;
            }

            protected internal bool ChangePlayerProperties(int playerId, LightHashTable propertiesToUpdate)
            {
                if (ClientStatus != ClientStatus.InRoom) return false;

                if (CurrentRoom.GetPlayer(playerId) == null)
                {
                    MUNLogs.ShowError($"Unknown player with playerId={playerId}.");
                    return false;
                }

                if (playerId != LocalPlayer.Id && !LocalPlayer.IsRoomOwner)
                {
                    MUNLogs.ShowError("Only PlayerOwner or RoomOwner can update player properties.");
                    return false;
                }

                if (propertiesToUpdate == null) propertiesToUpdate = new LightHashTable();

                Packet packet = new Packet();
                packet.Write((byte)ClientSendPackets.SendPlayerProperties);
                packet.Write(CurrentRoom.RoomId);
                packet.Write(playerId);
                PackPropertiesIntoPacket(packet, propertiesToUpdate);

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send ChangePlayerProperties: {isSending}");

                if (isSending)
                {
                    Player player = CurrentRoom.GetPlayer(playerId);
                    player.SetupProperties(propertiesToUpdate);
                    MUNInRoomCallbacks.OnPlayerPropertiesChanged(player, propertiesToUpdate);
                }
                return isSending;
            }

            internal bool KickPlayer(int playerId)
            {
                if (ClientStatus != ClientStatus.InRoom) return false;
                if (!LocalPlayer.IsRoomOwner)
                {
                    MUNLogs.ShowError("Only PlayerOwner or RoomOwner can update player properties.");
                    return false;
                }
                if (CurrentRoom.GetPlayer(playerId) == null)
                {
                    MUNLogs.ShowError($"Unknown player with playerId={playerId}.");
                    return false;
                }

                Packet packet = new Packet();
                packet.Write((byte)ClientSendPackets.KickPlayer);
                packet.Write(CurrentRoom.RoomId);
                packet.Write(playerId);

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send KickPlayer (id = {playerId}): {isSending}");

                return isSending;
            }

            internal void Disconnect()
            {
                if (ClientStatus != ClientStatus.Disconnected)
                {
                    ClientVoiceStatus = ClientVoiceStatus.Disconnected;
                    ClientStatus = ClientStatus.Disconnected;
                    tcp?.CloseSocket();
                    voiceTcp?.CloseSocket();
                    udp?.CloseSocket();
                    MUNConnectionCallbacks.OnDisconnectedFromMaster();
                }
                LocalPlayer.Id = -1;
                CurrentRoom = null;

                ClearWaitingFunctions();
            }

            internal void ChangeMUNViewOwner(MUNView munView, Player newOwner)
            {
                if (ClientStatus != ClientStatus.InRoom) return;
                if (!munView.IsMine && !MUNNetwork.LocalPlayer.IsRoomOwner) return;

                Packet packet = new Packet((byte)ClientSendPackets.ChangeMUNViewOwner);
                packet.Write(MUNNetwork.CurrentRoom.RoomId);
                packet.Write(munView.Id);
                packet.Write(newOwner.Id);

                bool isSending = SendDataInLoop(packet, ProtocolType.TCP);
                MUNLogs.ShowLog($"Send ChangeMUNViewOwner: {isSending}");

                if (isSending) MUNViewCallbacks.OnMUNViewOwnerChanged(munView, newOwner);
            }

            internal void SendRPC(LightHashTable rpcData, in byte targetsType, in int playerId, in ProtocolType protocolType)
            {
                /*
                 TargetType = (enum)TargetTypes. Jeżeli 0, to oznacza, że wysyłamy RPC do konkretnego gracza (playerId).
                 Także, albo playerId = 0 albo targetsType = 0. Nie ma innej opcji.
                 */

                if (ClientStatus != ClientStatus.InRoom) return;

                Packet packet = new Packet((byte)ClientSendPackets.SendingRPC);
                packet.Write(CurrentRoom.RoomId);
                packet.Write((byte)protocolType);
                packet.Write(targetsType);
                packet.Write(playerId);

                Packet rpcDataPacket = new Packet();
                try
                {
                    PackPropertiesIntoPacket(rpcDataPacket, rpcData);
                }
                catch
                {
                    MUNLogs.ShowError("Invalid RPC argument.");
                }
                byte[] rpcDataBytes = rpcDataPacket.ToByteArray();
                packet.Write(rpcDataBytes.Length);
                packet.Write(rpcDataBytes);

                bool isSending = SendDataInLoop(packet, protocolType);
                MUNLogs.ShowLog($"Send SendRPC: {isSending}");
            }

            internal void SendVoice(VoiceData voiceData)
            {
                if (MUNNetwork.CurrentRoom.PlayersInRoom < Consts.MIN_PLAYERS_TO_SERIALIZE) return;
                var voiceBytes = voiceData.ToByteArray();

                var packet = new Packet((byte)ClientVoiceSendPackets.SendVoice);
                packet.Write(MUNNetwork.CurrentVoiceRoom.RoomId);
                packet.Write(voiceBytes);

                SendData(packet, ProtocolType.VoiceChat);
            }

            private void UpdateCallbacks()
            {
                SetCallbacksToUpdate();

                void SetCallbacksToUpdate()
                {
                    while (callbacksToUpdate.Count > 0)
                    {
                        CallbacksToUpdate callback = callbacksToUpdate.Dequeue();

                        switch (callback.UpdateType)
                        {
                            case CallbacksToUpdate.CallbackUpdateType.Add:
                                if (callbacks.Contains(callback.Target)) continue;
                                callbacks.Add(callback.Target);
                                break;
                            default:
                                if (!callbacks.Contains(callback.Target)) continue;
                                callbacks.Remove(callback.Target);
                                break;
                        }


                        //Callbacks:
                        UpdateTarget(callback, MUNConnectionCallbacks);
                        UpdateTarget(callback, MUNRoomCallbacks);
                        UpdateTarget(callback, MUNInRoomCallbacks);
                        UpdateTarget(callback, MUNViewCallbacks);
                        UpdateTarget(callback, MUNVoiceChatCallbacks);
                    }
                }
                void UpdateTarget<T>(CallbacksToUpdate callback, List<T> container)
                    where T : class
                {
                    T target = callback.Target as T;
                    if (target is null) return;

                    switch (callback.UpdateType)
                    {
                        case CallbacksToUpdate.CallbackUpdateType.Add:
                            container.Add(target);
                            break;
                        default:
                            container.Remove(target);
                            break;
                    }
                }
            }
            private void PackPlayerPropertiesOnJoin(Packet packet, in Player player)
            {
                if (LocalPlayer.Properties.ContainsKey(PlayerProperties.Name))
                    player.Properties[PlayerProperties.Name] = player.Name;
                else
                    player.Properties.Add(PlayerProperties.Name, player.Name);

                PackPropertiesIntoPacket(packet, player.Properties);
            }
            private void WriteRoomDataIntoPacket(Packet packet, in Room.RoomClaims claims)
            {
                packet.Write(claims.IsOpen);
                packet.Write(claims.IsVisable);
                packet.Write(claims.Password);
                packet.Write(claims.MaxPlayers);
                PackPropertiesIntoPacket(packet, claims.Properties);
            }
            private static void RegisterServerMUNViewsOnServer()
            {
                IEnumerable<MUNView> serverMUNViews = MUNView.FindAll(true).Where(x => x.IsServerObject);

                foreach (MUNView munView in serverMUNViews)
                {
                    ICreateObjectInfo objectInfo = CreateObjectInfo.Build(munView.name, 0, munView.Owner, munView.Id, munView.transform.position, munView.transform.rotation);
                    ClientInstance.CreateObject(munView, objectInfo, false);
                }
            }
        }

        internal partial class MUNClient
        {
            public delegate void PacketHandler(Packet package);
            private const int MAX_DATA_BUFFER_SIZE = 16384 * 8;
            private TCP tcp;
            private TCP voiceTcp;
            private UDP udp;
            private static ClientStatus clientStatus;
            private static ClientVoiceStatus clientVoiceStatus;

            public IApplication Application { get; private set; }
            public static ClientStatus ClientStatus
            {
                get => clientStatus;
                private set
                {
                    clientStatus = value;
                    onClientStatusChangedEvent?.Invoke(value);
                }
            }

            public static ClientVoiceStatus ClientVoiceStatus
            {
                get => clientVoiceStatus;
                private set
                {
                    clientVoiceStatus = value;
                    onClientVoiceStatusChangedEvent?.Invoke(value);
                }
            }

            public static class MUNVoiceClientHandler
            {
                internal async static void OnConnectedToMaster(Packet package)
                {
                    try
                    {
                        await Task.Delay(Consts.CONNECTING_WAIT_TIME);

                        if (ClientVoiceStatus == ClientVoiceStatus.ConnectingToVoiceMasterServer)
                            ClientVoiceStatus = ClientVoiceStatus.ConnectedToVoiceMasterServer;

                        ClientInstance.MUNVoiceChatCallbacks.OnConnectedToVoiceChat();

                    }
                    catch (Exception e)
                    {
                        ClientVoiceStatus = ClientVoiceStatus.Disconnected;
                        MUNLogs.ShowException(e);
                    }
                }

                internal static void OnReceiveVoice(Packet package)
                {
                    if (!MUNNetwork.InRoom || MUNNetwork.VoiceStatus != ClientVoiceStatus.InRoom) return;
                    if (IsSceneChanging) return;
                    SerializeMUNVoiceRead(package);
                }

                internal static void OnJoinedRoom(Packet package)
                {
                    if (MUNNetwork.VoiceStatus != ClientVoiceStatus.JoiningIntoRoom) return;

                    string roomName = package.ReadString();
                    byte maxPlayers = package.ReadByte();

                    ClientInstance.CurrentVoiceRoom = new VoiceRoom(roomName, maxPlayers);

                    ClientVoiceStatus = ClientVoiceStatus.InRoom;
                    ClientInstance.MUNVoiceChatCallbacks.OnJoinedVoiceRoom();
                }

                internal static void OnPlayerJoinedRoom(Packet package)
                {
                    if (!MUNNetwork.InRoom || MUNNetwork.VoiceStatus != ClientVoiceStatus.InRoom) return;

                    int playerId = package.ReadInt();
                    if (playerId == MUNNetwork.LocalPlayer.Id) return;

                    var player = ClientInstance.CurrentRoom.GetPlayer(playerId);
                    ClientInstance.MUNVoiceChatCallbacks.OnPlayerJoinedVoiceChat(player);
                }

                internal static void OnLeftRoom(Packet package)
                {
                    if (
                        !MUNNetwork.Application.UseVoiceChat ||
                        (ClientVoiceStatus < ClientVoiceStatus.InRoom &&
                        ClientVoiceStatus != ClientVoiceStatus.LeavingRoom))
                    {
                        return;
                    }

                    int leftPlayerId = package.ReadInt();


                    if (MUNNetwork.LocalPlayer.Id == leftPlayerId)
                    {
                        ClientVoiceStatus = ClientVoiceStatus.ConnectedToVoiceMasterServer;
                        ClientInstance.CurrentVoiceRoom = null;
                        ClientInstance.MUNVoiceChatCallbacks.OnLeaveVoiceRoom();
                    }
                    else
                    {
                        Player leftPlayer = MUNNetwork.CurrentRoom.GetPlayer(leftPlayerId);

                        if (leftPlayer != null)
                            ClientInstance.MUNVoiceChatCallbacks.OnPlayerLeftVoiceChat(leftPlayer);
                    }
                }
            }

            public static class MUNClientHandler
            {
                public static async void OnConnectedToMaster(Packet packet)
                {
                    try
                    {
                        int playerId = packet.ReadInt();

                        Client.LocalPlayer.Id = playerId;
                        Client.LocalPlayer.Properties.Clear();

                        await Task.Delay(Consts.CONNECTING_WAIT_TIME);

                        if (ClientStatus == ClientStatus.ConnectingToMasterServer)
                            ClientStatus = ClientStatus.ConnectedToMasterServer;

                        ClientInstance.UpdateingRoomsList();
                        ClientInstance.MUNConnectionCallbacks.OnConnectedToMaster();

                    }
                    catch (Exception e)
                    {
                        ClientStatus = ClientStatus.Disconnected;
                        MUNLogs.ShowException(e);
                    }
                }

                public static void OnConnectedToMasterFailed(Packet packet)
                {
                    ClientStatus = ClientStatus.Disconnected;
                    string errorMessage = packet.ReadString();

                    ClientInstance.MUNConnectionCallbacks.OnConnectedToMasterFailed(errorMessage);
                }

                public static void OnSerialization(Packet packet)
                {
                    if (IsSceneChanging) return;
                    SerializeMUNRead(packet);
                }

                public static void OnCreatedRoom(Packet packet)
                {
                    string roomId = string.Empty;
                    string roomPassword = string.Empty;

                    try
                    {
                        roomId = packet.ReadString();
                        if (packet.UnreadLength > 4)
                        {
                            roomPassword = packet.ReadString();
                        }
                        ClientStatus = ClientStatus.CreatedRoom;
                    }
                    catch (Exception e)
                    {
                        ClientStatus = ClientStatus.ConnectedToMasterServer;
                        MUNLogs.ShowException(e);
                        return;
                    }


                    ClientInstance.MUNRoomCallbacks.OnRoomCreated();

                    JoinRoomInfo joinRoomInfo = new JoinRoomInfo()
                    {
                        RoomId = roomId,
                        Password = roomPassword
                    };

                    Client.JoinRoom(joinRoomInfo);
                }

                public static void OnCreatedRoomFailed(Packet packet)
                {
                    ClientStatus = ClientStatus.ConnectedToMasterServer;
                    string errorMessage = packet.ReadString();

                    ClientInstance.MUNRoomCallbacks.OnRoomCreatedFailed(errorMessage);
                }

                public static void OnJoinedRoom(Packet packet)
                {
                    try
                    {
                        #region Read RoomData
                        LightHashTable properties = ReadRoomPropertiesFromPacket(packet, out string roomName);
                        Room.RoomClaims claims = new Room.RoomClaims() { Properties = properties };
                        string mapProperty = "Map";
                        ClientInstance.CurrentRoom = new Room(roomName, claims);


                        if (properties.ContainsKey(mapProperty))
                        {
                            int mapIndex = (int)properties[mapProperty];
                            if (mapIndex != SceneManager.GetActiveScene().buildIndex)
                            {
                                ClientStatus = ClientStatus.ChangingScene;
                                MUNSceneManager.LoadScene(mapIndex, () =>
                                {
                                    Scene loadedScene = SceneManager.GetActiveScene();
                                    ClientInstance.MUNInRoomCallbacks.OnRoomChangeScene(loadedScene);
                                });
                            }
                        }
                        #endregion

                        #region Read PlayerData

                        while (packet.UnreadLength > 4)
                        {
                            int playerId = packet.ReadInt();
                            LightHashTable playerProperties = ReadPropertiesFromPacket(packet);

                            Player playerOnServer = new Player(playerId, playerProperties);
                            ClientInstance.CurrentRoom.AddPlayer(playerOnServer);
                        }

                        #endregion


                        MUNLoop.onLoopEvent += UpdateViews;

                        ClientStatus = ClientStatus.InRoom;
                        ClientInstance.CurrentRoom.AddPlayer(MUNNetwork.LocalPlayer);
                        ClientInstance.MUNRoomCallbacks.OnJoinedRoom();
                    }
                    catch (Exception e)
                    {
                        ClientStatus = ClientStatus.ConnectedToMasterServer;
                        ClientInstance.MUNRoomCallbacks.OnJoinedRoomFailed(e.Message);
                    }
                }

                public static void OnJoinedRoomFailed(Packet packet)
                {
                    ClientStatus = ClientStatus.ConnectedToMasterServer;
                    string errorMessage = packet.ReadString();

                    ClientInstance.MUNRoomCallbacks.OnJoinedRoomFailed(errorMessage);
                }

                internal static void OnPlayerJoinedRoom(Packet package)
                {
                    int playerId = package.ReadInt();
                    if (playerId == MUNNetwork.LocalPlayer.Id) return;

                    LightHashTable properties = ReadPropertiesFromPacket(package);

                    Player newPlayer = new Player(playerId, properties);
                    MUNNetwork.CurrentRoom.AddPlayer(newPlayer);

                    if (MUNNetwork.LocalPlayer.IsRoomOwner)
                        SendCachedRPCsToNewPlayer(newPlayer.Id);

                    ClientInstance.MUNInRoomCallbacks.OnPlayerJoinedRoom(newPlayer);
                }

                public static void OnLeftRoom(Packet packet)
                {
                    int leftPlayerId = packet.ReadInt();

                    if (MUNNetwork.LocalPlayer.Id == leftPlayerId)
                    {
                        ClientStatus = ClientStatus.ConnectedToMasterServer;

                        if (MUNNetwork.Application.UseVoiceChat && MUNNetwork.VoiceStatus > ClientVoiceStatus.Disconnected)
                            MUNNetwork.LeaveVoiceRoom();

                        ClientInstance.CurrentRoom = null;
                        ClientInstance.MUNRPCRepository.Clear();
                        ClientInstance.LocalPlayer.Properties = new LightHashTable();

                        ClearWaitingFunctions();
                        ClientInstance.MUNRoomCallbacks.OnLeftRoom();
                    }
                    else
                    {
                        Player leftPlayer = MUNNetwork.CurrentRoom.GetPlayer(leftPlayerId);

                        if (ClientInstance.LocalPlayer.IsRoomOwner)
                        {
                            switch (OnPlayerDisconectedMUNViewLogic)
                            {
                                case MUNViewsLogicOnPlayerDisconected.ChangeOwnerToRoomOwner:
                                    IEnumerable<MUNView> munViewsToChangeOwner = MUNView.FindAllByOwner(leftPlayerId, false);
                                    foreach (MUNView munViewToChangeOwner in munViewsToChangeOwner)
                                    {
                                        munViewToChangeOwner.ChangeOwner(ClientInstance.LocalPlayer);
                                    }
                                    break;
                                case MUNViewsLogicOnPlayerDisconected.DestroyAllPlayerMUNViews:
                                    DestroyAllPlayerMUNViews(leftPlayer);
                                    break;
                            }
                        }

                        MUNNetwork.CurrentRoom.RemovePlayer(leftPlayer);
                        ClientInstance.MUNInRoomCallbacks.OnPlayerLeftRoom(leftPlayer);
                    }
                }

                public static void OnRoomListUpdated(Packet packet)
                {
                    List<RoomData> roomsData = new List<RoomData>();

                    while (packet.UnreadLength >= 4)
                    {
                        LightHashTable properties = ReadRoomPropertiesFromPacket(packet, out string roomName);

                        RoomData roomData = new RoomData(roomName, properties);
                        roomsData.Add(roomData);
                    }

                    ClientInstance.MUNRoomCallbacks.OnRoomListUpdated(roomsData);
                }

                public static void OnRoomPropertiesUpdated(Packet packet)
                {
                    LightHashTable updatedRoomProperties = ReadPropertiesFromPacket(packet);
                    MUNNetwork.CurrentRoom.SetupProperties(updatedRoomProperties);

                    ClientInstance.MUNInRoomCallbacks.OnRoomPropertiesChanged(updatedRoomProperties);
                }

                internal static void OnPlayerPropertiesUpdated(Packet package)
                {
                    int senderPlayerId = package.ReadInt();
                    int targetPlayerId = package.ReadInt();
                    LightHashTable updatedProperties = ReadPropertiesFromPacket(package);

                    if (!MUNNetwork.CurrentRoom.TryGetPlayer(senderPlayerId, out Player senderPlayer))
                    {
                        MUNLogs.ShowError("SenderPlayer is not in currenr room.");
                        return;
                    }

                    if (!MUNNetwork.CurrentRoom.TryGetPlayer(targetPlayerId, out Player targetPlayer))
                    {
                        MUNLogs.ShowError("TargetPlayer is not in currenr room.");
                        return;
                    }

                    targetPlayer.SetupProperties(updatedProperties);
                    ClientInstance.MUNInRoomCallbacks.OnPlayerPropertiesChanged(targetPlayer, updatedProperties);
                }

                internal static void OnRoomOwnerChanged(Packet package)
                {
                    int oldOwnerId = package.ReadInt();
                    int newOwnerId = package.ReadInt();

                    if (ClientStatus != ClientStatus.InRoom) return;

                    if (!ClientInstance.CurrentRoom.TryGetPlayer(newOwnerId, out Player newOwner))
                    {
                        MUNNetwork.LeaveRoom();
                        MUNLogs.ShowException(new NullReferenceException("New RoomOwner is not at CurrentRoom"));
                        return;
                    }

                    LightHashTable newRoomProperties = new LightHashTable() { { RoomProperties.OwnerId, newOwnerId } };

                    UpdateMUNViewsOwner();

                    ClientInstance.CurrentRoom.SetupProperties(newRoomProperties);
                    ClientInstance.MUNInRoomCallbacks.OnRoomOwnerChanged(newOwner);

                    void UpdateMUNViewsOwner()
                    {
                        IEnumerable<MUNView> oldOwnerViews = GetAllMUNViews(true).Where(x => x.IsServerObject);
                        foreach (MUNView oldOwnerView in oldOwnerViews)
                        {
                            Consts.MUNViewHelper.ChangeOwner(oldOwnerView, newOwner.Id);
                        }
                    }
                }

                internal static void OnRoomSceenChanged(Packet package)
                {
                    if (!MUNNetwork.InRoom || MUNLoop.IsQuit) return;

                    int mapIndex = package.ReadInt();
                    ClientStatus = ClientStatus.ChangingScene;
                    ClientInstance.MUNRPCRepository.Clear();
                    MUNSceneManager.LoadScene(mapIndex, () =>
                    {
                        ClientStatus = ClientStatus.InRoom;
                        Scene loadedScene = SceneManager.GetActiveScene();
                        ClientInstance.MUNInRoomCallbacks.OnRoomChangeScene(loadedScene);

                        if (ClientInstance.LocalPlayer.IsRoomOwner) RegisterServerMUNViewsOnServer();
                    });

                }

                internal static void OnMUNViewOwnerChanged(Packet packet)
                {
                    int munViewId = packet.ReadInt();
                    int newOwnerId = packet.ReadInt();

                    if (ClientStatus != ClientStatus.InRoom) return;

                    MUNView munView = GetMUNView(munViewId);
                    if (munView == null)
                    {
                        MUNLogs.ShowError($"Cannot change MUNView owner. MUNView with id = {munViewId} not found.");
                        return;
                    }

                    Player newOwner = MUNNetwork.CurrentRoom.GetPlayer(newOwnerId);
                    if (newOwner == null)
                    {
                        MUNLogs.ShowError($"Cannot change MUNView owner. New oewner with id = {newOwnerId} not found.");
                        return;
                    }

                    Consts.MUNViewHelper.ChangeOwner(munView, newOwner.Id);
                    ClientInstance.MUNViewCallbacks.OnMUNViewOwnerChanged(munView, newOwner);
                }

                internal static void OnReceiveRPC(Packet packet)
                {
                    if (IsSceneChanging)
                    {
                        MUNLogs.ShowWarning("Cannot ReceiveRPC during scene changing.");
                        return;
                    }

                    if (!MUNNetwork.InRoom)
                    {
                        return;
                    }

                    Packet fromServerPacket = packet.ReadBytes(packet.UnreadLength).ToObject<Packet>();
                    Packet rpcDataPacket = new Packet(fromServerPacket.ReadBytes(fromServerPacket.Length));
                    LightHashTable rpcData = ReadPropertiesFromPacket(rpcDataPacket);

                    LightHashTable rpcDataToInvoke = new LightHashTable();
                    foreach (KeyValuePair<object, object> data in rpcData)
                    {
                        if (byte.TryParse(data.Key.ToString(), out byte keyByte))
                            rpcDataToInvoke.Add(keyByte, data.Value);
                        else
                            rpcDataToInvoke.Add(data.Key, data.Value);
                    }

                    InvokeRPCMethod(rpcDataToInvoke, ClientInstance.LocalPlayer);
                }

                internal static void OnCreatedObject(Packet packet)
                {
                    Queue<ICreateObjectInfo> createQueue = ReadPacket(packet);
                    ExecuteCreateObjects(createQueue);

                    Queue<ICreateObjectInfo> ReadPacket(Packet packet)
                    {
                        Queue<ICreateObjectInfo> createQueue = new Queue<ICreateObjectInfo>();

                        string prefabName;
                        int tempObjectId, objectOwnerId, objectId;
                        Vector3 position;
                        Quaternion rotation;
                        Player owner;
                        while (packet.UnreadLength > 0)
                        {
                            prefabName = packet.ReadString();
                            tempObjectId = packet.ReadInt();
                            objectOwnerId = packet.ReadInt();
                            objectId = packet.ReadInt();
                            position = packet.ReadVector3();
                            rotation = packet.ReadQuaternion();


                            try
                            {

                                owner = ClientInstance.CurrentRoom.GetPlayer(objectOwnerId);
                                createQueue.Enqueue(CreateObjectInfo.Build(prefabName, tempObjectId, owner, objectId, position, rotation));
                            }
                            catch
                            {
                            }
                        }

                        return createQueue;
                    }
                    void ExecuteCreateObjects(Queue<ICreateObjectInfo> createQueue)
                    {
                        while (createQueue.Count > 0)
                        {
                            MUNView createdMUNView = MUNNetwork.CreateObject(createQueue.Dequeue());

                            if (createdMUNView != null && createdMUNView.Id > Consts.MAX_START_SERVER_OBJECT_ID)
                                ClientInstance.MUNViewCallbacks.OnMUNViewCreated(createdMUNView);
                        }
                    }
                }

                internal static void OnCreateObjectFailed(Packet packet)
                {
                    int tempViewId = packet.ReadInt();
                    int limitViewsPerRoom = packet.ReadInt();
                    int limitViewsPerPlayerInRoom = packet.ReadInt();
                    var view = MUNView.Find(tempViewId);
                    MonoBehaviour.Destroy(view.gameObject);
                    MUNLogs.ShowError($"The object per room limit has been exceeded. Object limit per player in room = {limitViewsPerPlayerInRoom}. Object limit per room = {limitViewsPerRoom}. Object with tempId = {tempViewId} not created.");
                }

                internal static void OnDestroyedObject(Packet packet)
                {
                    List<int> viewsToDestroy = new List<int>();
                    while (packet.UnreadLength >= 4)
                    {
                        viewsToDestroy.Add(packet.ReadInt());
                    }

                    MUNView view;
                    foreach (int viewId in viewsToDestroy)
                    {
                        view = MUNView.Find(viewId);

                        if (view is null)
                        {
                            MUNLogs.ShowWarning($"OnDestroyedObject: View with id = {viewId} not found.");

                            var hasWitingObject = waitForCreateNetworkObjectTuples.FirstOrDefault(x => x.CreateObjectInfo.ObjectId == viewId);

                            if (hasWitingObject != null)
                            {
                                waitForCreateNetworkObjectTuples.Remove(hasWitingObject);
                            }

                            continue;
                        }

                        ClientInstance.MUNViewCallbacks.OnPreMUNViewDelete(view);
                        DestroyMUNObject(view, true);
                    }
                }

                private static LightHashTable ReadRoomPropertiesFromPacket(Packet packet, out string roomName)
                {
                    LightHashTable properties = new LightHashTable();

                    roomName = packet.ReadString();
                    properties.Add(RoomProperties.RoomId, packet.ReadString());
                    properties.Add(RoomProperties.OwnerId, packet.ReadInt());
                    properties.Add(RoomProperties.IsOpen, packet.ReadBool());
                    properties.Add(RoomProperties.IsVisable, packet.ReadBool());
                    properties.Add(RoomProperties.HasPassword, packet.ReadBool());
                    properties.Add(RoomProperties.MaxPlayers, packet.ReadByte());
                    properties.Add(RoomProperties.PlayersCount, packet.ReadByte());

                    LightHashTable customProperties = ReadPropertiesFromPacket(packet);
                    if (customProperties != null && customProperties.Keys.Count > 0)
                    {
                        foreach (KeyValuePair<object, object> property in customProperties)
                        {
                            properties.Add(property.Key, property.Value);
                        }
                    }

                    return properties;
                }

                private static void SendCachedRPCsToNewPlayer(int newPlayerId)
                {
                    IEnumerable<RPCTuple> cachedRPCs = ClientInstance.MUNRPCRepository.GetAllRPCs();
                    foreach (RPCTuple cachedRPC in cachedRPCs)
                    {
                        Client.SendRPC(cachedRPC.RpcData, 0, newPlayerId, ProtocolType.UDP);
                    }
                }
            }
        }

        internal static void PackPropertiesIntoPacket(Packet packet, LightHashTable properties)
        {
            if (properties is null)
            {
                packet.Write(0);
                return;
            }
            int propertyCount = properties.Count(property => property.Key != null && property.Value != null);

            packet.Write(propertyCount);

            foreach (KeyValuePair<object, object> property in properties)
            {
                if (property.Key is null || property.Value is null) continue;

                Type propetyType = property.Value.GetType();
                byte propetyTypeId = Consts.PACKET_PROPETY_TYPE[propetyType];

                packet.Write(property.Key.ToString());
                packet.Write(propetyTypeId);

                if (propetyType == typeof(byte))
                    packet.Write((byte)property.Value);
                else if (propetyType == typeof(byte[]))
                {
                    byte[] bytes = (byte[])property.Value;
                    packet.Write(bytes.Length);
                    packet.Write(bytes);
                }
                else if (propetyType == typeof(short))
                    packet.Write((short)property.Value);
                else if (propetyType == typeof(int))
                    packet.Write((int)property.Value);
                else if (propetyType == typeof(long))
                    packet.Write((long)property.Value);
                else if (propetyType == typeof(float))
                    packet.Write((float)property.Value);
                else if (propetyType == typeof(bool))
                    packet.Write((bool)property.Value);
                else if (propetyType == typeof(string))
                    packet.Write((string)property.Value);
                else if (propetyType == typeof(Vector2))
                    packet.Write((Vector2)property.Value);
                else if (propetyType == typeof(Vector3))
                    packet.Write((Vector3)property.Value);
                else if (propetyType == typeof(Quaternion))
                    packet.Write((Quaternion)property.Value);
                else
                {
                    MUNLogs.ShowException(new ArgumentException("<b>PackPropertiesIntoPacket</b>: Wrong propety value."));
                    return;
                }
            }
        }

        internal static LightHashTable ReadPropertiesFromPacket(Packet packet)
        {
            int propertiesCount = packet.ReadInt();

            LightHashTable properties = new LightHashTable();
            for (int i = 0; i < propertiesCount; i++)
            {
                string key = packet.ReadString();
                byte propetyTypeId = packet.ReadByte();
                Type propetyType = Consts.PACKET_PROPETY_TYPE.First(x => x.Value == propetyTypeId).Key;
                object value;

                if (propetyType == typeof(byte))
                    value = packet.ReadByte();
                else if (propetyType == typeof(byte[]))
                    value = packet.ReadBytes(packet.ReadInt());
                else if (propetyType == typeof(short))
                    value = packet.ReadShort();
                else if (propetyType == typeof(int))
                    value = packet.ReadInt();
                else if (propetyType == typeof(long))
                    value = packet.ReadLong();
                else if (propetyType == typeof(float))
                    value = packet.ReadFloat();
                else if (propetyType == typeof(bool))
                    value = packet.ReadBool();
                else if (propetyType == typeof(string))
                    value = packet.ReadString();
                else if (propetyType == typeof(Vector2))
                    value = packet.ReadVector2();
                else if (propetyType == typeof(Vector3))
                    value = packet.ReadVector3();
                else if (propetyType == typeof(Quaternion))
                    value = packet.ReadQuaternion();
                else
                {
                    MUNLogs.ShowException(new ArgumentException("<b>ReadPropertiesFromPacket</b>: Wrong propety value."));
                    return null;
                }

                properties.Add(key, value);
            }

            return properties;
        }
    }
}
