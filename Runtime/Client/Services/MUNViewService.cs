﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MUN.Client
{
    public static partial class MUNNetwork
    {
        private class MUNViewService : IMUNViewService
        {
            private HashSet<MUNView> MUNViews { get; set; }
            private Dictionary<string, MUNView> PrefabsResources { get; set; }

            public int GetNextTemporaryMUNViewId(bool increaseValue)
            {
                //int result = (Consts.TEMP_ID_MULTIPLER * MUNNetwork.LocalPlayer.Id + ++createdMUNViews) * -1;
                //createdMUNViews %= Consts.TEMP_ID_MULTIPLER;
                //return result;

                if (increaseValue)
                    createdMUNViews++;
                return -createdMUNViews;
            }

            private int createdMUNViews = 0;

            public MUNViewService()
            {
                MUNViews = new HashSet<MUNView>();
                PrefabsResources = new Dictionary<string, MUNView>();
            }

            public MUNView GetMUNView(int viewId)
            {
                return MUNViews.FirstOrDefault(x => x.Id == viewId);
            }

            public IEnumerable<MUNView> GetMUNViewsByOwner(int ownerId, bool includeServerObjects)
            {
                return MUNViews.Where(x => x.Owner != null && x.Owner.Id.Equals(ownerId) && (includeServerObjects || !x.IsServerObject)).ToArray();
            }

            public void RegisterMUNView(MUNView view)
            {
                if (!UnityEngine.Application.isPlaying)
                {
                    MUNViews = new HashSet<MUNView>();
                    return;
                }

                if (view is null)
                {
                    MUNLogs.ShowException(new NullReferenceException("Can not register null munView object."));
                    return;
                }

                if (view.Id == Consts.NOT_ASSIGNED_MUN_VIEW_ID)
                {
                    MUNLogs.ShowError($"<b>MUNNetwork</b>: Register failed. No id assigned yet to: {view}");
                    return;
                }

                if (MUNViews.Any(x => x.Id.Equals(view.Id)))
                {
                    MUNLogs.ShowError($"<b>MUNNetwork</b>: Register failed. Duplicated MUNView id = {view.Id}");
                    return;
                }

                MUNViews.Add(view);
            }

            public void UpdateViews()
            {
                if (!InRoom || Client.CurrentRoom.PlayersInRoom < Consts.MIN_PLAYERS_TO_SERIALIZE)
                    return;

                foreach (MUNView view in MUNViews)
                {
                    if (!view.IsMine || !view.isActiveAndEnabled)
                        continue;

                    Packet data = SerializeMUNWrite(view);
                    if (data == null || data.Length <= Consts.MIN_DATA_LENGTH_REQUIRED)
                        continue;

                    Client.SendData(data, ProtocolType.UDP);
                }
            }


            public void RemoveMUNViewLocaly(MUNView view)
            {
                MUNView munView = MUNViews.FirstOrDefault(x => x.Id.Equals(view.Id));
                if (munView != null)
                {
                    MUNViews.Remove(munView);
                }
            }

            public IEnumerable<MUNView> GetAllMUNViews(bool includeServerObjects)
            {
                return MUNViews.Where(x => includeServerObjects || !x.IsServerObject);
            }

            public MUNView Create(string prefabName, Vector3 position, Quaternion rotation)
            {
                if (!PrefabsResources.TryGetValue(prefabName, out MUNView prefab))
                {
                    prefab = Resources.Load<MUNView>(prefabName);
                    if (prefab is null)
                    {
                        MUNLogs.ShowError($"Creating object failed. Cannot find prefabname = {prefabName}. Check if Prefab is in <b>Resources</b> folder.");
                        return null;
                    }

                    PrefabsResources.Add(prefabName, prefab);
                }

                if (prefab.gameObject.activeSelf)
                    prefab.gameObject.SetActive(false);
                MUNView prefabCopy = GameObject.Instantiate(prefab, position, rotation).GetComponent<MUNView>();
                prefab.gameObject.SetActive(true);

                return prefabCopy;
            }

            public void Destroy(MUNView view)
            {
                GameObject.Destroy(view.gameObject);
            }

            public void Destroy(MUNView[] views)
            {
                foreach (MUNView view in views)
                {
                    Destroy(view);
                }
            }
        }
    }
}
