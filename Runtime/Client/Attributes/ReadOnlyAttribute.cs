﻿namespace MUN.Client
{
    using UnityEngine;
    /// <summary>
    /// Auxiliary attribute for viewing variables in the inspector while locking the editing of a given variable.
    /// </summary>
    public class ReadOnlyAttribute : PropertyAttribute { }
}