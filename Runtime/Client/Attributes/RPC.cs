﻿namespace MUN.Client
{
    using System;

    /// <summary>
    /// RPC attribute for the MUN package. Marks methods as remotely invokable.
    /// </summary>
    public class MunRPCAttribute : Attribute { }
}