using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
// ReSharper disable InconsistentNaming
// ReSharper disable IdentifierTypo
#endif

namespace MUN.Client
{
    // ReSharper disable once InconsistentNaming
    public class MUNSettings : ScriptableObject
    {
        private static readonly string MUNSETTINGS_FOLDER_PATH = "Assets/EpicVR MUN/Resources/Data";
        private static readonly string MUNSETTINGS_PATH = Path.Combine(MUNSETTINGS_FOLDER_PATH, "MUNSettings.asset");

        private static MUNSettings _munSettings;

        [FormerlySerializedAs("rpcs")]
        [SerializeField]
        private List<string> _rpcs = new List<string>();

        private static bool HasSettingsFile => File.Exists(MUNSETTINGS_PATH);


        /// <summary>
        /// Object to store MUN application settings.
        /// </summary>
        public static MUNSettings Get()
        {
#if UNITY_EDITOR
            if (!HasSettingsFile)
                CreateSettings();
            else
#endif
                _munSettings = Resources.Load<MUNSettings>("Data/MUNSettings");

            return _munSettings;
        }

#if UNITY_EDITOR

        public void RefreshRpcs()
        {
            RPCs = GetRpcs();
        }

        public void ClearRpcs()
        {
            RPCs.Clear();
        }

        internal List<string> GetRpcs()
        {
            List<string> list = new List<string>();

            TypeCache.MethodCollection rpcMethods = TypeCache.GetMethodsWithAttribute<MunRPCAttribute>();
            int id = 0;
            foreach (System.Reflection.MethodInfo rpcMethod in rpcMethods)
            {
                if (id < byte.MaxValue)
                {
                    list.Add(rpcMethod.Name);
                }
                else
                {
                    MUNLogs.ShowWarning("RPCs List is full.");
                    break;
                }

                id++;
            }

            list.Sort();
            EditorUtility.SetDirty(this);
            return list;
        }

        private static void CreateSettings()
        {
            var fullFolder = Path.GetFullPath(MUNSETTINGS_FOLDER_PATH);

            if (!Directory.Exists(fullFolder))
                Directory.CreateDirectory(fullFolder);

            _munSettings = CreateInstance<MUNSettings>();
            _munSettings.LogTypes = MUNLogs.LogTypes.Log | MUNLogs.LogTypes.Warning | MUNLogs.LogTypes.Exception |
                                    MUNLogs.LogTypes.Error;

            AssetDatabase.CreateAsset(_munSettings, MUNSETTINGS_PATH);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
#endif

        /// <summary>
        /// Application ID.
        /// </summary>
        [field: SerializeField]
        public string AppID { get; set; } = String.Empty;

        /// <summary>
        /// Specify the application testing region.
        /// </summary>
        [field: SerializeField]
        public string TestRegion { get; set; } = "eu;";

        /// <summary>
        /// The list of RPCs.
        /// </summary>
        public List<string> RPCs
        {
            get => _rpcs;
            private set => _rpcs = value;
        }

        [field: SerializeField, EnumFlags]
        public MUNLogs.LogTypes LogTypes { get; set; }
    }

    public static class LogTypesExntensions
    {
        public static MUNLogs.LogTypes Add(this MUNLogs.LogTypes logTypes, MUNLogs.LogTypes value)
        {
            logTypes |= value;
            return logTypes;
        }

        public static MUNLogs.LogTypes Remove(this MUNLogs.LogTypes logTypes, MUNLogs.LogTypes value)
        {
            logTypes &= ~value;
            return logTypes;
        }
    }
}