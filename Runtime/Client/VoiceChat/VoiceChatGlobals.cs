namespace MUN.Client.VoiceChat
{
    public sealed class VoiceChatGlobals
    {
        public const int SAMPLE_RATE = 1000;
        public const byte BUFFER_SEGMENT_COUNT = 10;
        public const byte MIN_SEGMENT_COUNT = 5;

        public const int FREQUENCY = 16000;
        public const int SAMPLE_LENGTH = 64;

        public static readonly string[] IceServers = new string[]
        {
        "stun.l.google.com:19302",
        "stun1.l.google.com:19302",
        "stun2.l.google.com:19302",
        "stun3.l.google.com:19302",
        "stun4.l.google.com:19302",
        "stun.vivox.com:3478",
        "stun.freecall.com:3478"
        };
    }
}