﻿using UnityEngine;

namespace MUN.Client.VoiceChat
{
    public class AudioBuffer
    {
        public AudioClip AudioClip { get; private set; }

        private readonly int segDataLength;
        private int _firstIndex = -1;

        /// <summary>
        /// Create an instance
        /// </summary>
        /// <param name="frequency">The frequency of the audio</param>
        /// <param name="channels">Number of channels in the audio</param>
        /// <param name="segDataLength">Numer of samples in the audio </param>
        public AudioBuffer(int frequency, int channels, int segDataLength)
        {
            var clipName = $"{MUNNetwork.LocalPlayer.Name}_clip";
            AudioClip = AudioClip.Create(clipName, segDataLength * VoiceChatGlobals.BUFFER_SEGMENT_COUNT, channels, frequency, false);

            this.segDataLength = segDataLength;
        }

        /// <summary>
        /// Feed an audio segment to the buffer.
        /// </summary>
        /// 
        /// <param name="absoluteIndex">
        /// Absolute index of the audio segment from the source.
        /// </param>
        /// 
        /// <param name="audioSegment">Audio samples data</param>
        public void Write(int absoluteIndex, float[] audioSegment)
        {
            if (audioSegment.Length != segDataLength)
                return;
            if (absoluteIndex < 0 || absoluteIndex < _firstIndex)
                return;

            if (_firstIndex == -1)
                _firstIndex = absoluteIndex;

            var localIndex = GetNormalizedIndex(absoluteIndex);

            if (localIndex >= 0)
                AudioClip.SetData(audioSegment, localIndex * segDataLength);
        }

        /// <summary>
        /// Returns the index after looping around the buffer
        /// </summary>
        public int GetNormalizedIndex(int absoluteIndex)
        {
            if (_firstIndex == -1 || absoluteIndex <= _firstIndex) return -1;
            return (absoluteIndex - _firstIndex) % VoiceChatGlobals.BUFFER_SEGMENT_COUNT;
        }

        /// <summary>
        /// Clears the buffer at the specified local index
        /// </summary>
        /// <param name="index"></param>
        public void Clear(int index)
        {
            if (index < 0)
                return;

            if (index >= VoiceChatGlobals.BUFFER_SEGMENT_COUNT)
                index = GetNormalizedIndex(index);
            AudioClip.SetData(new float[segDataLength], index * segDataLength);
        }

        /// <summary>
        /// Clear the entire buffer
        /// </summary>
        public void Clear()
        {
            AudioClip.SetData(new float[segDataLength * VoiceChatGlobals.BUFFER_SEGMENT_COUNT], 0);
        }
    }
}