using MUN.Client.Extensions;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MUN.Client.VoiceChat
{
    [RequireComponent(typeof(AudioSource))]
    public class MUNSpeaker : MonoBehaviour, IAudioOutput
    {
        private enum Status
        {
            Ahead,
            Current,
            Behind
        }

        private AudioSource audioSource;
        private AudioBuffer audioBuffer;

        private readonly Dictionary<int, Status> segments = new Dictionary<int, Status>();

        private int currentIndex;
        private int lastIndex = -1;
        private int readyCount;
        private Player speakerOwner;

        public int OwnerId => speakerOwner.Id;

        private void Awake()
        {
            if (!MUNNetwork.Application.UseVoiceChat)
                Destroy(this);

            audioSource = GetComponent<AudioSource>();
            speakerOwner ??= GetComponentInParent<MUNView>().Owner;
        }

        private void Start()
        {
            var recorder = FindObjectOfType<MUNVoiceRecorder>();
            if (!recorder)
            {
                Debug.LogError("MUNVoiceRecorder is not found.");
                return;
            }

            if (recorder.IsRecording) InitAudioBuffer(recorder);
            else recorder.onStartRecording += () => { InitAudioBuffer(recorder); };
        }

        private void Update()
        {
            if (!audioSource.clip) return;

            currentIndex = (int)(audioSource.Position() * VoiceChatGlobals.BUFFER_SEGMENT_COUNT);

            if (lastIndex != currentIndex)
            {
                audioBuffer.Clear(lastIndex);
                segments.EnsureKey(lastIndex, Status.Behind);
                segments.EnsureKey(currentIndex, Status.Current);

                lastIndex = currentIndex;
            }

            readyCount = segments.Count(x => x.Value.Equals(Status.Ahead));

            audioSource.mute = readyCount < VoiceChatGlobals.MIN_SEGMENT_COUNT;

            if (readyCount == 0) audioSource.mute = true;
            else if (readyCount >= VoiceChatGlobals.MIN_SEGMENT_COUNT)
            {
                audioSource.mute = false;
                if (!audioSource.isPlaying)
                    audioSource.Play();
            }
        }

        public void ReadVoice(in VoiceData voiceData)
        {
            if (audioBuffer is null || segments.ContainsKey(voiceData.segmentIndex)) return;

            int locIdx = (int)(audioSource.Position() * VoiceChatGlobals.BUFFER_SEGMENT_COUNT);
            locIdx = Mathf.Clamp(locIdx, 0, VoiceChatGlobals.BUFFER_SEGMENT_COUNT - 1);

            var bufferIndex = audioBuffer.GetNormalizedIndex(voiceData.segmentIndex);

            if (locIdx == bufferIndex) return;
            segments.Add(voiceData.segmentIndex, Status.Ahead);

            var samplesFloat = voiceData.samples.ToFloat();
            audioBuffer.Write(voiceData.segmentIndex, samplesFloat);
        }

        private void InitAudioBuffer(MUNVoiceRecorder recorder)
        {
            if (!MUNNetwork.InRoom) return;

            audioBuffer = new AudioBuffer(recorder.Frequency, recorder.ChannelCount, recorder.SampleLength);
            audioSource.clip = audioBuffer.AudioClip;
            audioSource.loop = true;
            lastIndex = -1;
        }
    }
}