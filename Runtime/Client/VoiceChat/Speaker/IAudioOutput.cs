﻿using System;

namespace MUN.Client.VoiceChat
{
    public interface IAudioOutput
    {
        void ReadVoice(in VoiceData voiceData);
    }
}