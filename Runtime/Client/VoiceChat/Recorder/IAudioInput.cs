﻿namespace MUN.Client.VoiceChat
{
    public interface IAudioInput
    {
        IMicrophone Microphone { get; }

        /// <summary>
        /// The number of channels in the audio
        /// </summary>
        int ChannelCount { get; }

        /// <summary>
        /// The number of segments (a segment is a sequence of audio samples)
        /// that are emitted from the source every second.
        /// A 16000 Hz source with a rate of 10 will output an array of
        /// 1600 samples every 100 milliseconds.
        /// </summary>
        int SegmentRate { get; }
    }
}