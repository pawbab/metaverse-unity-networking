﻿using System.Collections.Generic;
using UnityEngine;

namespace MUN.Client.VoiceChat
{
    public interface IMicrophone
    {
        /// <summary>
        /// Whether the microphone is running
        /// </summary>
        bool IsRecording { get; }
        /// <summary>
        /// The frequency at which the microphone is operating
        /// </summary>
        int Frequency { get; }
        /// <summary>
        /// Last populated audio sample
        /// </summary>
        float[] Sample { get; }
        /// <summary>
        /// Sample duration/length in milliseconds
        /// </summary>
        int SampleDurationMS { get; }
        /// <summary>
        /// The length of the sample float array
        /// </summary>
        int SampleLength { get; }
        /// <summary>
        /// The AudioClip currently being streamed in the microphone
        /// </summary>
        AudioClip AudioClip { get; }
        /// <summary>
        /// List of all the available microphone devices
        /// </summary>
        IList<string> Devices { get; }
        /// <summary>
        /// Index of the current microphone device
        /// </summary>
        int CurrentDeviceIndex { get; }
        /// <summary>
        /// Gets the name of the microphone device currently in use
        /// </summary>
        string CurrentDeviceName { get; }
        void UpdateDevices();
        /// <summary>
        /// Changes to a Mic device for Recording.
        /// </summary>
        /// <param name="index">The index of the microphone device.</param>
        void ChangeDevice(int index);
        /// <summary>
        /// Starts to stream the input of the current microphone device.
        /// </summary>
        /// <param name="frequency"> The frequency at which the microphone is operating.</param>
        /// <param name="sampleLength"> The length of the sample.</param>
        void StartRecording(in int frequency = 1600, in int sampleLength = 10);
        /// <summary>
        /// Ends the microphone stream.
        /// </summary>
        void StopRecording();
    }
}