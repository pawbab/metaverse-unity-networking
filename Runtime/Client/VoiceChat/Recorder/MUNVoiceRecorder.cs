using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MUN.Client.VoiceChat
{
    [RequireComponent(typeof(AudioSource))]
    public class MUNVoiceRecorder : MonoBehaviour, IAudioInput, IMicrophone
    {
        [SerializeField] private float minValueToPass = .07f;

        /// <summary>
        /// Invoked when the instance starts recording.
        /// </summary>
        public event Action onStartRecording;

        /// <summary>
        /// Invoked everytime an audio frame is collected. Includes the frame.
        /// </summary>
        public event Action<int, float[]> onSampleReady;

        /// <summary>
        /// Invoked when the instance stops recording.
        /// </summary>
        public event Action onStopRecording;

        private int sampleCount = 0;

        #region Properties
        public bool IsRecording { get; private set; }
        public int Frequency { get; private set; }
        public float[] Sample { get; private set; }
        public int SampleDurationMS { get; private set; }
        public int SampleLength => Frequency * SampleDurationMS / VoiceChatGlobals.SAMPLE_RATE;
        public AudioClip AudioClip { get; private set; }
        public IList<string> Devices { get; private set; }
        public int CurrentDeviceIndex { get; private set; } = -1;
        public string CurrentDeviceName
        {
            get
            {
                if (CurrentDeviceIndex < 0 || CurrentDeviceIndex >= Devices.Count)
                    return string.Empty;
                return Devices[CurrentDeviceIndex];

            }
        }
        public IMicrophone Microphone => this;
        public int ChannelCount => !Microphone.AudioClip ? 0 : Microphone.AudioClip.channels;
        public int SegmentRate => VoiceChatGlobals.SAMPLE_RATE / Microphone.SampleDurationMS;
        #endregion

        #region Unity Methods
        private void Start()
        {
            UpdateDevices();
            CurrentDeviceIndex = 0;
        }

        private void OnDestroy()
        {
            onStartRecording = null;
            onSampleReady = null;
            onStopRecording = null;
        }
        #endregion

        public void UpdateDevices()
        {
            Devices = new List<string>();
            foreach (var device in UnityEngine.Microphone.devices)
                Devices.Add(device);
        }

        /// <summary>
        /// Changes to a Mic device for Recording
        /// </summary>
        /// <param name="index">The index of the Mic device. Refer to <see cref="Devices"/></param>
        public void ChangeDevice(int index)
        {
            UnityEngine.Microphone.End(CurrentDeviceName);
            CurrentDeviceIndex = index;

            if (IsRecording)
            {
                StartRecording(Frequency, SampleDurationMS);
            }
        }
        /// <summary>
        /// Starts to stream the input of the current microphone device
        /// </summary>
        public void StartRecording(in int frequency = 8000, in int sampleLength = 128)
        {
            StopRecording();
            IsRecording = true;
            Frequency = frequency;
            SampleDurationMS = sampleLength;

            AudioClip = UnityEngine.Microphone.Start(CurrentDeviceName, true, 1, Frequency);
            Sample = new float[Frequency / VoiceChatGlobals.SAMPLE_RATE * SampleDurationMS * AudioClip.channels];

            StartCoroutine(ReadAudio());
            onStartRecording?.Invoke();
        }

        /// <summary>
        /// Ends the microphone stream.
        /// </summary>
        public void StopRecording()
        {
            if (!UnityEngine.Microphone.IsRecording(CurrentDeviceName))
                return;

            IsRecording = false;
            UnityEngine.Microphone.End(CurrentDeviceName);
            Destroy(AudioClip);
            AudioClip = null;

            StopCoroutine(ReadAudio());
            onStopRecording?.Invoke();
        }


        private IEnumerator ReadAudio()
        {
            int loops = 0;
            int readAbsPos = 0;
            int prevPos = 0;
            float[] tmp = new float[Sample.Length];

            while (AudioClip != null &&
                UnityEngine.Microphone.IsRecording(CurrentDeviceName))
            {
                while (true)
                {
                    int currPos = UnityEngine.Microphone.GetPosition(CurrentDeviceName);
                    if (currPos < prevPos) loops++;
                    prevPos = currPos;

                    var curAbsPos = loops * AudioClip.samples + currPos;
                    var nextReadAbsPos = readAbsPos + tmp.Length;

                    if (nextReadAbsPos >= curAbsPos)
                        break;

                    AudioClip.GetData(tmp, readAbsPos % AudioClip.samples);
                    sampleCount++;

                    if (ClarifyVoice.HasMinAvgVoiceVolumeToPass(in tmp, in minValueToPass))
                    {
                        Sample = tmp;
                        onSampleReady?.Invoke(sampleCount, Sample);
                    }

                    readAbsPos = nextReadAbsPos;
                }
                yield return null;
            }
        }
    }
}