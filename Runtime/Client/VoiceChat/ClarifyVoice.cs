﻿using UnityEngine;
using System.Linq;

namespace MUN.Client.VoiceChat
{
    internal static class ClarifyVoice
    {

        internal static bool HasMinAvgVoiceVolumeToPass(in float[] samples, in float minAvgVoiceVolumeToPass)
        {
            var tmpAbs = samples.Where(x => x > 0).Select(x => Mathf.Abs(x));
            var tmpAvg = tmpAbs.Sum() / tmpAbs.Count() * 10f;

            return tmpAvg > minAvgVoiceVolumeToPass;
        }
    }
}