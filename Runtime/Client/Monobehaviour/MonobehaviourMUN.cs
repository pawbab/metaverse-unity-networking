﻿using MUN.Client.Interfaces;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MUN.Client
{
    /// <summary>
    /// The class adds the <see cref="MUNView"/> property and registers the interception 
    /// of callbacks from the server in the <see cref="OnEnable"/> method.
    /// It has all the virtual methods called using callbacks  contained in interfaces:
    /// <see cref="IMUNConnection"/>, <see cref="IMUNRoom"/>, <see cref="IMUNInRoom"/>, <see cref="IMUNView"/>.
    /// </summary>
    /// <remarks>
    /// Do not add the keyword <b>new</b> before <code>OnEnable</code> and <code>OnDisable</code>, use <b>override</b> instead, and use <code>base.OnEnable</code>, 
    /// <code>base.OnDisable</code> in methods as these methods are responsible for registering callbacks
    /// </remarks>
    public class MonobehaviourMUN : MonoBehaviour, IMUNConnection, IMUNRoom, IMUNInRoom, IMUNView, IMUNVoice
    {
        /// <summary>
        /// A reference to a <see cref="MUNView"/> on the current <see cref="GameObject"/>
        /// </summary>
        private MUNView munView;

        /// <summary>
        /// A reference to a <see cref="MUNView"/> on the current <see cref="GameObject"/>
        /// </summary>
        /// <remarks>
        /// If you remove the <see cref="MUNView"/> component from this object or want to reference another <see cref="MUNView"/>,
        /// we recommend that you search for it with <see cref="MUNView.Find(Component)"/> or <seealso cref="MUNView.Find(int)"/>
        /// </remarks>
        public MUNView MUNView
        {
            get
            {
                if (munView == null) munView = MUNView.Find(this);
                return munView;
            }
        }

        protected virtual void OnEnable()
        {
            RegisterCallbacks();
        }

        protected virtual void OnDisable()
        {
            UnregisterCallbacks();
        }

        /// <summary>
        /// Called when the client joins the MasterServer.
        /// </summary>
        /// <remarks>
        /// The application logic should start with a connection to the MasterServer. Without connection, the client will not be able to perform other connection operations.
        /// The method allows you to diagnose whether MasterServer is available.
        /// The method is only executed after connecting to the MasterServer. It will not be executed when the user leaves the room
        /// </remarks>
        public virtual void OnConnectedToMaster() { }

        /// <summary>
        /// Called when the MasterServer refuses the connection
        /// </summary>
        /// <remarks>
        /// It will only invoke when MasterServer is available but will reject the client for some reason. 
        /// An example is the number of clients allowed in an application being exceeded.
        /// </remarks>
        /// <param name="message">Error message from the MasterServer</param>
        public virtual void OnConnectedToMasterFailed(string message) { }

        /// <summary>
        /// Called after disconnecting from the MasterServer.
        /// </summary>
        /// <remarks>
        /// For the intended purpose or if the connection is lost
        /// </remarks>
        public virtual void OnDisconnectedFromMaster() { }

        /// <summary>
        /// Called when the client has created a room. It will automatically be attached to the room where the <see cref="OnJoinedRoom"/> method will be called.
        /// </summary>
        /// <remarks>
        /// Calling only on the client who created the room.
        /// If you need specific properties of the room, add claims when creating it.
        /// </remarks>
        public virtual void OnRoomCreated() { }

        /// <summary>
        /// Called when the server failed to create a room
        /// </summary>
        /// <remarks>
        /// The most common cause of room creation failure is an incorrect room name or network connection problems.
        /// Check if the client is connected to the MasterServer - (<see cref="MUNNetwork.Status"/>).
        /// </remarks>
        /// <param name="message">Error message from the MasterServer</param>
        public virtual void OnRoomCreatedFailed(string message) { }

        /// <summary>
        /// Called when Client joined the room, regardless of whether the client created it or just joined.
        /// </summary>
        /// <remarks>
        /// Provides access to players in <code>MUNNetwork.CurrentRoom</code> <see cref="Room.Players"/>, their properties and room properties.
        /// In this callback, you can create network objects.
        /// </remarks>
        public virtual void OnJoinedRoom() { }

        /// <summary>
        /// Called when it failed to join a room.
        /// </summary>
        /// <remarks>
        /// This may be because the room is overcrowded or you are trying to join a room that no longer exists.
        /// </remarks>
        /// <param name="message">Error message from the MasterServer</param>
        public virtual void OnJoinedRoomFailed(string message) { }

        /// <summary>
        /// Called when the local client has left the room
        /// </summary>
        /// <remarks>
        /// The client's status changes from InRoom to ConnectedToMaster.
        /// </remarks>
        public virtual void OnLeftRoom() { }

        /// <summary>
        /// Called when updating rooms with client status = ConnectedToMaster.
        /// Called after joining the MasterServer and after leaving the room.
        /// </summary>
        /// <remarks>
        /// Each <see cref="RoomData"/> item contains information about the room and can contain its own properties - <see cref="RoomData.Properties"/>
        /// Not all rooms are available on the list. Some are hidden, closed or full.
        /// </remarks>
        /// <param name="rooms">Enumerated type with a list of available rooms</param>
        public virtual void OnRoomListUpdated(IEnumerable<RoomData> rooms) { }

        /// <summary>
        /// Called when a remote player entered the room. MUN previously added a player to the player list.
        /// </summary>
        /// <remarks>
        /// Called on all players who have previously been in the room.
        /// It can be useful to configure the settings of a given player or to send specific information to them.
        /// </remarks>
        /// <param name="player">The player who has joined the room</param>
        public virtual void OnPlayerJoinedRoom(Player player) { }

        /// <summary>
        /// Called when the remote player has left the room.
        /// </summary>
        /// <param name="player">The player who left the room</param>
        public virtual void OnPlayerLeftRoom(Player player) { }

        /// <summary>
        /// Called on changing custom room properties.
        /// </summary>
        /// <remarks>
        /// <code>changedProperties</code> contains values set in <see cref="Room.SetCustomProperties(LightHashTable)"/>
        /// Only RoomOwner can change room properties
        /// </remarks>
        public virtual void OnRoomPropertiesChanged(LightHashTable changedProperties) { }

        /// <summary>
        /// Called on changing custom player properties.
        /// </summary>
        /// <remarks>
        /// If you are not RoomOwner you can change only own properties (LocalPlayer). 
        /// Only RoomOwner can change all custom properties.
        /// </remarks>
        /// <param name="player">The player who change properties</param>
        public virtual void OnPlayerPropertiesChanged(Player player, LightHashTable changedProperties) { }
        /// <summary>
        /// Called after switching to the new RoomOwner while the current one leaves.
        /// </summary>
        /// <remarks>
        /// The previous RoomOwner may still be in the player list when this method is called.
        /// RoomOwner may designate a new room owner at any time.
        /// </remarks>
        /// <param name="newOwner">New RoomOwner</param>
        public virtual void OnRoomOwnerChanged(Player newOwner) { }

        /// <summary>
        /// Called after the scene changed in the room.
        /// </summary>
        /// <remarks>
        /// Only RoomOwner has the ability to change the scene via the network.
        /// The other clients in the room can only receive information about the change
        /// and this callback will be called for them too.
        /// </remarks>
        /// <param name="loadedScene">Current loaded scene</param>
        public virtual void OnRoomChangeScene(Scene loadedScene) { }

        /// <summary>
        /// Called when any of the clients creates any network object.
        /// </summary>
        /// <param name="newMUNView"></param>
        public virtual void OnMUNViewCreated(MUNView newMUNView) { }

        /// <summary>
        /// Called when any of the clients delete any network object.
        /// </summary>
        /// <remarks>
        /// Only RoomOwner and the property owner can delete the specified network object.
        /// </remarks>
        public virtual void OnPreMUNViewDelete(MUNView munViewToDelete) { }

        /// <summary>
        /// Called when a specific MUNView changes its owner.
        /// </summary>
        /// <remarks>
        /// Only RoomOwner and the owner of the MUNView can set a new owner.
        /// </remarks>
        /// <param name="munView">MUNView That Changed ownership</param>
        /// <param name="newOwner">New owner of the network object</param>
        public virtual void OnMUNViewOwnerChanged(MUNView munView, Player newOwner) { }
        public virtual void OnConnectedToVoiceChat() { }
        public virtual void OnDisconnectedFromVoiceChat() { }
        public virtual void OnPlayerJoinedVoiceChat(Player player) { }
        public virtual void OnPlayerLeftVoiceChat(Player player) { }
        public virtual void OnJoinedVoiceRoom() { }
        public virtual void OnLeaveVoiceRoom() { }

        private void RegisterCallbacks()
        {
            MUNNetwork.RegisterCallbacks(this);
        }
        private void UnregisterCallbacks()
        {
            MUNNetwork.UnregisterCallbacks(this);
        }
    }
}
