﻿using System;
using System.Collections;
using UnityEngine;

namespace MUN.Client
{
    /// <summary>
    /// Class generated after connecting to MasterServer, responsible for calculating the TickRate of the application and calling the <b>onLoopEvent</b> event after each tickrate.
    /// </summary>
    /// <remarks>
    /// This class pushes some packets to the server and also performs <see cref="MUNView"/> serialization
    /// </remarks>
    internal class MUNLoop : MonoBehaviour
    {
        private const int MS_IN_SEC = 1000;

        private static MUNLoop instance;
        private static MUNLoop Instance
        {
            get
            {
                if (instance is null)
                {
                    instance = FindObjectOfType<MUNLoop>();
                    if (instance is null)
                    {
                        instance = new GameObject("Metaverse Unity Networking Manager").AddComponent<MUNLoop>();
                        instance.gameObject.AddComponent<ThreadManager>();
                        instance.gameObject.AddComponent<MUNSceneManager>();
                    }
                }

                return instance;
            }
        }

        #region Inspector Debug
        [SerializeField, ReadOnly]
        private int currentTimeSinceStart;
        #endregion

        /// <summary>
        /// If true, the object will not be destroyed when the scene changes.
        /// </summary>
        /// <remarks>
        /// It is recommended that you leave this value as true.
        /// </remarks>
        public bool dontDestroyOnLoad = true;
        private int nextUpdateTime;
        private bool isQuit;

        /// <summary>
        /// Event called every application tickrate.
        /// </summary>
        internal static event Action onLoopEvent;

        
        /// <summary>
        /// True if the application is shutting down.
        /// </summary>
        internal static bool IsQuit => Instance.isQuit;

        private void Awake()
        {
            CheckInstance();
            Application.runInBackground = true;
            if (dontDestroyOnLoad) DontDestroyOnLoad(this);

            isQuit = false;
        }

        private void Start()
        {
            IncreaseNextUpdateTime();
        }

        private void LateUpdate()
        {

            if (IsQuit || MUNNetwork.Status == MUNNetwork.ClientStatus.Disconnected) return;
            currentTimeSinceStart = (int)(Time.realtimeSinceStartup * MS_IN_SEC);

            if (nextUpdateTime >= currentTimeSinceStart) 
                return;
            
            onLoopEvent?.Invoke();
            IncreaseNextUpdateTime();
        }


        private void OnApplicationQuit()
        {
            if (!IsQuit)
                OnQuit();
        }

        private void OnDestroy()
        {
            if (!IsQuit)
                OnQuit();
        }

        internal static Coroutine InvokeCoroutine(IEnumerator routine)
        {
            return Instance.StartCoroutine(routine);
        }

        private void CheckInstance()
        {
            if (instance == null || ReferenceEquals(this, instance))
                instance = this;
            else
                Destroy(gameObject);
        }
        private void IncreaseNextUpdateTime()
        {
            nextUpdateTime = currentTimeSinceStart + MUNNetwork.Client.Application.TickRate;
        }
        private void OnQuit()
        {
            isQuit = true;
            MUNNetwork.DisconnectFromMasterServer();
        }
    }
}
