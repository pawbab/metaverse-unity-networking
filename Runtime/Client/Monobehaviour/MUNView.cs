﻿using MUN.Client.Extensions;
using MUN.Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using static MUN.Client.MUNNetwork;

namespace MUN.Client
{
    /// <summary>
    /// MUNView identifies the object in the network and enables synchronization of its configuration and parameters with other clients in the room.
    /// </summary>
    [DisallowMultipleComponent]
    public class MUNView : MonoBehaviour
    {
        /// <summary>
        /// Unique MUNView Id
        /// </summary>
        [SerializeField, ReadOnly]
        private int id = Consts.NOT_ASSIGNED_MUN_VIEW_ID;

        [SerializeField, ReadOnly]
        private List<Component> serializeComponents;

        internal MonoBehaviour[] RPCBehaviours;
        protected internal SerializationHelper serializationHelper;

        /// <summary>
        /// If true, MUNView belongs to a Local player
        /// </summary>
        public bool IsMine => Owner?.Id == MUNNetwork.LocalPlayer.Id;
        /// <summary>
        /// If true, MUNView is on the built scene by default.
        /// </summary>
        /// <remarks>
        /// This means that the MUNView was not created with <see cref="MUNNetwork.CreateNetworkObject(string, Vector3, Quaternion)"/>, and has existed on the scene from the beginning.
        /// </remarks>
        public bool IsServerObject => Id <= Consts.MAX_START_SERVER_OBJECT_ID && Id > Consts.NOT_ASSIGNED_MUN_VIEW_ID;
        /// <summary>
        /// Reference to an Owner of this MUNView
        /// </summary>
        public Player Owner { get; private set; }
        /// <summary>
        /// Unique View object Id
        /// </summary>
        public int Id => id;

        private void Awake()
        {
            Setup();
        }
        private void OnDestroy()
        {
            MUNNetwork.RemoveMUNViewLocaly(this);
        }

        /// <summary>
        /// Finds components responsible for custom serialization of an object.
        /// The system takes care of searching for these components on its own.
        /// </summary>
        public void FindSerializeComponents()
        {
            if (serializeComponents == null)
                serializeComponents = new List<Component>();
            else
                serializeComponents.Clear();

            serializeComponents = transform.FindComponentsInChildren<IMUNSerialize>(true).ToList();
        }

        /// <summary>
        /// Allows you to change the owner of the selected network object
        /// </summary>
        /// <remarks>
        /// The owner can only be changed by the current owner of the property or by the owner of the room.
        /// 
        /// When the owner of an object changes, the OnMUNViewOwnerChanged method is called.
        /// </remarks>
        /// <param name="newOwner">New owner for the facility.</param>
        public void ChangeOwner(Player newOwner)
        {
            if (newOwner.Id == this.Owner.Id)
            {
                return;
            }

            if (!MUNNetwork.LocalPlayer.IsRoomOwner && !this.IsMine)
            {
                MUNLogs.ShowError("Only RoomOwner or Owner of MUNView can set new MUNView Owner.");
                return;
            }

            MUNNetwork.ChangeMUNViewOwner(this, newOwner);
            ChangeOwner(this, newOwner.Id);
        }
        /// <summary>
        /// Removes a network object with an id corresponding to the object from the parameter.
        /// The object will not be removed if the method is performed by a player other than the owner of the object (except RoomOwner)
        /// </summary>
        /// <remarks>
        /// All RPC calls associated with that object will be deleted as the object is cleared.
        /// 
        /// The destruction of network objects will succeed if and only if they have been added using <see cref="MUNNetwork.CreateNetworkObject(string, Vector3, Quaternion)"/> 
        /// or were on the scene when the scene was started.
        /// </remarks>
        public void Destroy()
        {
            MUNNetwork.DestroyNetworkObject(this);
        }
        /// <summary>
        /// Send (via UDP) a remote procedure call of this GameObject on remote clients (in the same room).
        /// </summary>
        /// <remarks>
        /// Sending RPC too frequently can cause lag for remote clients.
        /// 
        /// RPC methods for security have a limited limit of information that we can transmit.
        /// 
        /// Transferring methods RPC automatically takes care of the appropriate execution of the method at the appropriate client on the appropriate object.
        /// </remarks>
        /// <param name="method">The name of the method you cant to call. The method requires the MunRPC attribute.</param>
        /// <param name="targets">The group of recipients where the method is to be called. Some RPCTargets may cache calls for newly joined players.</param>
        /// <param name="args">Parameters that we send using RPC. They must match the called method.</param>
        public void SendRPC(string method, RPCTargets targets, params object[] args)
        {
            MUNNetwork.SendRPC(this, method, targets, ProtocolType.UDP, args);
        }
        /// <summary>
        /// Send a remote procedure call of this GameObject on remote clients (in the same room).
        /// </summary>
        /// <remarks>
        /// Sending RPC too frequently can cause lag for remote clients.
        /// 
        /// RPC methods for security have a limited limit of information that we can transmit.
        /// 
        /// Transferring methods RPC automatically takes care of the appropriate execution of the method at the appropriate client on the appropriate object.
        /// </remarks>
        /// <param name="method">The name of the method you cant to call. The method requires the MunRPC attribute.</param>
        /// <param name="targets">The group of recipients where the method is to be called. Some RPCTargets may cache calls for newly joined players.</param>
        /// <param name="protocolType">Protocol by which the RPC packet will be sent.</param>
        /// <param name="args">Parameters that we send using RPC. They must match the called method.</param>
        public void SendRPC(string method, RPCTargets targets, ProtocolType protocolType = ProtocolType.UDP, params object[] args)
        {
            MUNNetwork.SendRPC(this, method, targets, protocolType, args);
        }
        /// <summary>
        /// Send (via UDP) a remote procedure call of this GameObject on specyfic player (in the same room).
        /// </summary>
        /// <remarks>
        /// Sending RPC too frequently can cause lag for remote clients.
        /// 
        /// RPC methods for security have a limited limit of information that we can transmit.
        /// 
        /// Transferring methods RPC automatically takes care of the appropriate execution of the method at the appropriate client on the appropriate object.
        /// </remarks>
        /// <param name="method">The name of the method you cant to call. The method requires the MunRPC attribute.</param>
        /// <param name="target">The specific player you want to send the RPC to.</param>
        /// <param name="args">Parameters that we send using RPC. They must match the called method.</param>
        public void SendRPC(string method, Player target, params object[] args)
        {
            MUNNetwork.SendRPC(this, method, target, ProtocolType.UDP, args);
        }
        /// <summary>
        /// Send a remote procedure call of this GameObject on specyfic player (in the same room).
        /// </summary>
        /// <remarks>
        /// Sending RPC too frequently can cause lag for remote clients.
        /// 
        /// RPC methods for security have a limited limit of information that we can transmit.
        /// 
        /// Transferring methods RPC automatically takes care of the appropriate execution of the method at the appropriate client on the appropriate object.
        /// </remarks>
        /// <param name="method">The name of the method you cant to call. The method requires the MunRPC attribute.</param>
        /// <param name="target">The specific player you want to send the RPC to.</param>
        /// <param name="protocolType">Protocol by which the RPC packet will be sent.</param>
        /// <param name="args">Parameters that we send using RPC. They must match the called method.</param>
        public void SendRPC(string method, Player target, ProtocolType protocolType = ProtocolType.UDP, params object[] args)
        {
            MUNNetwork.SendRPC(this, method, target, protocolType, args);
        }

        /// <summary>
        /// Updates all MonoBehaviour components for the given MUNView.
        /// </summary>
        public void RefreshRPCBehaviours()
        {
            RPCBehaviours = GetComponents<MonoBehaviour>();
        }
        /// <summary>
        /// Find MUNView via Id
        /// </summary>
        /// <param name="id">The id of the searched MUNView</param>
        public static MUNView Find(int id)
        {
            return MUNNetwork.GetMUNView(id);
        }
        /// <summary>
        ///  Searches for all network objects.
        /// </summary>
        /// <param name="includeServerObjects">If true, then the enumeration will also return the objects in the scene build that belong to the player.</param>
        public static IEnumerable<MUNView> FindAll(bool includeServerObjects)
        {
            return MUNNetwork.GetAllMUNViews(includeServerObjects);
        }
        /// <summary>
        /// Searches for all player network objects.
        /// </summary>
        /// <param name="includeServerObjects">If true, then the enumeration will also return the objects in the scene build that belong to the player.</param>
        /// <returns>The objects belonging to the specified player.</returns>
        public static IEnumerable<MUNView> FindAllByOwner(int ownerId, bool includeServerObjects)
        {
            return MUNNetwork.GetMUNViewsByOwner(ownerId, includeServerObjects);
        }
        /// <summary>
        /// Find MUNView with component.
        /// </summary>
        public static MUNView Find(Component component)
        {
            return component.transform.GetComponentInParent<MUNView>();
        }
        /// <summary>
        /// Set Id for MUNView.
        /// </summary>
        /// <remarks>
        ///  We recommend that you do not use this method on your own, as the system takes care of the appropriate object identifiers.
        ///  Changing them inappropriately can generate problems.
        /// </remarks>
        /// <param name="id">New MUNView Id</param>
        internal void SetViewId(int id)
        {
            this.id = id;
        }
        internal void SerializeView(MUNPackage packet)
        {
            if (serializeComponents == null ||
                serializeComponents.Count == 0 ||
                Id < 1) return;

            IMUNSerialize serialize;
            MonoBehaviour serializedObject;
            foreach (Component component in serializeComponents)
            {
                serialize = component as IMUNSerialize;
                serializedObject = (MonoBehaviour)serialize;
                if (serializedObject is null || !serializedObject.isActiveAndEnabled) continue;
                serialize.MUNSerializeWrite(packet);
            }
        }
        internal void DeserializeView(MUNPackage packet)
        {
            if (serializeComponents == null ||
                serializeComponents.Count == 0) return;

            foreach (Component component in serializeComponents)
            {
                IMUNSerialize serialize = component as IMUNSerialize;
                if (serialize == null) continue;
                serialize.MUNSerializeRead(packet);
            }
        }

        private void Setup()
        {
            if (Id != Consts.NOT_ASSIGNED_MUN_VIEW_ID) MUNNetwork.RegisterMUNView(this);
            serializationHelper = new SerializationHelper();
            FindSerializeComponents();
            SetupOwnerIfNull();
        }
        private void SetupOwnerIfNull()
        {
            if (Owner != null) return;

            if (MUNNetwork.InRoom)
            {
                Player roomOwner = MUNNetwork.CurrentRoom.GetPlayer(MUNNetwork.CurrentRoom.OwnerId);
                Owner = roomOwner;
            }
            else
            {
                MUNLogs.ShowError("You have to be InRoom before you start set MUNViews");
            }
        }

        private static void ChangeOwner(MUNView munView, int newOwnerId)
        {
            Player newOwner = CurrentRoom.GetPlayer(newOwnerId);
            munView.Owner = newOwner;
        }

#if UNITY_EDITOR
        public static void UpdateViewsId(in MUNView[] munViews)
        {
            if (UnityEngine.Application.isPlaying) return;

            int index = 1;
            int currentId, setId;
            foreach (MUNView view in munViews)
            {
                currentId = view.Id;
                setId = currentId;

                if (!view.gameObject.activeInHierarchy) setId = Consts.NOT_ASSIGNED_MUN_VIEW_ID;
                else
                {
                    setId = index++;
                    if (setId > Consts.MAX_START_SERVER_OBJECT_ID)
                    {

                        setId = -1;
                        view.gameObject.SetActive(false);
                        MUNLogs.ShowError($"The limit of allowed object Ids ({Consts.MAX_START_SERVER_OBJECT_ID}) has been exceeded. Object: {view.name}");
                    }
                }

                if (currentId != setId)
                {
                    view.id = setId;
                    EditorUtility.SetDirty(view);
                }
            }
        }
#endif

        /// <summary>
        /// The values will be used to serialize the MUNView.
        /// </summary>
        /// <remarks>
        /// Leave them as they are and be glad it works :)
        /// </remarks>
        public class SerializationHelper
        {
            protected internal object[] syncValues;
            protected internal object[] lastSyncValues;
            protected internal byte ignoredSerialize = 0;
        }
    }
}
