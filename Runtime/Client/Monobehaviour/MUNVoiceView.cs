using MUN.Client.Extensions;
using MUN.Client.Interfaces;
using UnityEngine;

namespace MUN.Client.VoiceChat
{
    [DisallowMultipleComponent]
    public class MUNVoiceView : MonoBehaviour, IMUNVoice
    {
        [SerializeField] MUNVoiceRecorder recorder;

        #region Unity Methods
        private void Awake()
        {
            CheckMUNVoiceViewComponents(1);
        }

        private void Start()
        {
            switch (MUNNetwork.VoiceStatus)
            {
                case MUNNetwork.ClientVoiceStatus.Disconnected:
                    MUNNetwork.ConnectToVoiceMasterServer();
                    break;
                case MUNNetwork.ClientVoiceStatus.ConnectedToVoiceMasterServer:
                    MUNNetwork.CreateOrJoinVoiceRoom();
                    break;
                case MUNNetwork.ClientVoiceStatus.InRoom:
                    OnJoinedVoiceRoom();
                    break;
                default:
                    break;
            }
        }

        private void OnEnable()
        {
            MUNNetwork.RegisterCallbacks(this);
            recorder.onSampleReady += OnSampleReadyToSend;
        }
        private void OnDisable()
        {
            MUNNetwork.UnregisterCallbacks(this);
            recorder.onSampleReady -= OnSampleReadyToSend;
            recorder.StopRecording();
        }
        #endregion

        public void OnConnectedToVoiceChat() => MUNNetwork.CreateOrJoinVoiceRoom();

        public void OnJoinedVoiceRoom() => recorder.StartRecording(VoiceChatGlobals.FREQUENCY, VoiceChatGlobals.SAMPLE_LENGTH);

        public void OnLeaveVoiceRoom() => recorder.StopRecording();

        public void OnPlayerJoinedVoiceChat(Player player) => Debug.Log($"@OnPlayerJoinedVoiceChat: {player.Name}");

        public void OnPlayerLeftVoiceChat(Player player) => Debug.Log($"@OnPlayerLeftVoiceChat: {player.Name}");
        public void OnDisconnectedFromVoiceChat() { }

        private void OnSampleReadyToSend(int sampleCount, float[] samples)
        {
            var shortSamples = samples.ToShort();
            var voiceData = new VoiceData
            {
                frequency = recorder.Frequency,
                channelCount = recorder.ChannelCount,
                samples = shortSamples,
                segmentIndex = sampleCount
            };

            MUNNetwork.SendVoice(voiceData);
        }
        private void CheckMUNVoiceViewComponents(int maxCount)
        {
            var voiceViewComponets = FindObjectsOfType<MUNVoiceView>();
            if (voiceViewComponets.Length > maxCount)
            {
                MUNLogs.ShowError("There are more then one MUNVoiceView components in the same scene.");

                //Remove any redundant components:
                for (int i = 1; i < voiceViewComponets.Length; i++)
                {
                    Destroy(voiceViewComponets[i]);
                }
            }
        }

    }
}