using MUN.Client;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MUN
{
    public static class MUNLogs
    {
        private const string MSG_FORMAT = "<b><color={0}>MUNLogs</color></b>: {1}";
        private static readonly Dictionary<LogTypes, Action<string, UnityEngine.Object>> LogActions = new Dictionary<LogTypes, Action<string, UnityEngine.Object>>
        {
            {LogTypes.Log, Debug.Log },
            {LogTypes.Warning, Debug.LogWarning },
            {LogTypes.Error, Debug.LogError },
        };

        private static readonly Dictionary<LogTypes, string> LogColors = new Dictionary<LogTypes, string>
        {
            {LogTypes.Log, "#e7e7e7"},
            {LogTypes.Warning, "#f99c39"},
            {LogTypes.Error, "#fe5e41"},
            {LogTypes.Exception, "#313639"},
        };
        private static MUNSettings munSettings;

        public static void ShowLog(string message, UnityEngine.Object context = null)
        {
            Show(LogTypes.Log, message, context);
        }
        public static void ShowWarning(string message, UnityEngine.Object context = null)
        {
            Show(LogTypes.Warning, message, context);
        }
        public static void ShowError(string message, UnityEngine.Object context = null)
        {
            Show(LogTypes.Error, message, context);
        }
        public static void ShowException(Exception exception, UnityEngine.Object context = null)
        {
            if (munSettings == null)
                munSettings = MUNSettings.Get();

            if (!munSettings.LogTypes.HasFlag(LogTypes.Exception)) return;
            Debug.LogException(exception, context);
        }

        private static void Show(LogTypes logType, string message, UnityEngine.Object context)
        {
            if (munSettings == null)
                munSettings = MUNSettings.Get();

            if (!munSettings.LogTypes.HasFlag(logType)) return;

            if (LogActions.TryGetValue(logType, out Action<string, UnityEngine.Object> action))
                Show(action, message, context);

            void Show(Action<string, UnityEngine.Object> action, string message, UnityEngine.Object context)
            {
                action(string.Format(MSG_FORMAT, LogColors[logType], message), context);
            }
        }

        [Flags]
        public enum LogTypes
        {
            Log = 1,
            Warning = 2,
            Error = 4,
            Exception = 8
        }
    }
}