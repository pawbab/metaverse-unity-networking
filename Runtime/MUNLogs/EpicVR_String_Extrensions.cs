using UnityEngine;

namespace MUN
{
    public static class EpicVR_String_Extrensions
    {
        private const string SUCCESS_COLOR = "#85bb65";
        private const string FAIL_COLOR = "#fe5e41";

        public static string WithColor(this string input, Color color)
        {
            return $"<color=#{ColorUtility.ToHtmlStringRGBA(color)}>{input}</color>";
        }

        public static string WithColorSuccess(this string input)
        {
            return WithColor(input, SUCCESS_COLOR);
        }

        public static string WithColorFailed(this string input)
        {
            return WithColor(input, FAIL_COLOR);
        }

        public static string WithColor(this string input, string hex)
        {
            if (!hex.StartsWith("#")) hex = '#' + hex;
            return $"<color={hex}>{input}</color>";
        }

        public static string WithBold(this string input)
        {
            return $"<b>{input}</b>";
        }
    }
}