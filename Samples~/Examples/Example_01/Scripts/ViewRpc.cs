using MUN.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MUNView))]
public class ViewRpc : MonoBehaviour
{
    [SerializeField] MUNNetwork.RPCTargets rpcTargers = MUNNetwork.RPCTargets.OthersInRoom;

    private MUNView munView;

    private void Awake()
    {
        munView = GetComponent<MUNView>();
    }

    public void SendRPC()
    {
        munView.SendRPC("Send", rpcTargers); 
    }

    [MunRPC]
    private void Send()
    {
        Debug.Log($"MUNView Id = {munView.Id} send RPC <color=red>:)</color>.");
    }
}
