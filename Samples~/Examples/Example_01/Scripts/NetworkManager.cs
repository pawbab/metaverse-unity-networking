using MUN.Client;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonobehaviourMUN
{
    private static NetworkManager Instance { get; set; }

    private readonly Room.RoomClaims claims = new Room.RoomClaims()
    {
        MaxPlayers = 20
    };

    [SerializeField] string playerName;

    [Tooltip("Testy wykonywane, gdy nowy gracz do��czy si� do pokoju. Testy wykonywane przez RoomOwnera")]
    [SerializeField] ExecuteTestType executeTestOnPlayerJoined;

    [Tooltip("Testy wykonywane, gdy do��cz� si� do pokoju i nie jestem RoomOwnerem. Testy wykonywane przez innego gracza ni� RoomOwner")]
    [SerializeField] ExecuteTestType executeTestOnJoined;

    [Tooltip("Testy wykonywany, gdy RoomOwner zmieni scene.")]
    [SerializeField] ExecuteTestType executeTestOnSceneChangedRoomOwner;



    [SerializeField] bool disconnectFromRoomOnRoomOwnerChanged = false;
    [SerializeField] int munViewId = 0;
    [SerializeField] int playerId = 0;

    private readonly Dictionary<ExecuteTestType, IExecuteTest> testsToExecute = new Dictionary<ExecuteTestType, IExecuteTest>()
    {
        {ExecuteTestType.KickNewPlayer, new KickNewPlayer() },
        {ExecuteTestType.ChangeRoomOwnerToNewPlayer, new ChangeRoomOwner() },
        {ExecuteTestType.ChangeSceneOnNewPlayerJoin, new ChangeSceneOnNewPlayerJoin("Scene2") },
        {ExecuteTestType.DisconnectFromRoom, new DisconnectFromRoom() },
        {ExecuteTestType.AddRoomProperty, new AddRoomProperty() },
        {ExecuteTestType.AddPlayerProperties, new AddPlayerProperty(new LightHashTable()
            {
            {"prop_1", 0},
            {"prop_2", true},
            {"prop_3", "tekst"},
            {"prop_4", 12.5f},
            }) },
        {ExecuteTestType.ChangeMUNViewOwner, new ChangeMUNViewOwner() },
        {ExecuteTestType.DestroyMUNView, new DestroyMUNView() },
        {ExecuteTestType.SendRPC, new SendRPCByMUNView() }
    };


    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        MUNNetwork.PlayerName = playerName;
    }

    public void ConnectToMaster()
    {
        MUNNetwork.ConnectToMaster();
    }

    public void JoinOrCreateRandomRoom()
    {
        MUNNetwork.JoinOrCreateRandomRoom(claims);
    }

    public void LeftRoom()
    {
        MUNNetwork.LeaveRoom();
    }

    public void DisconnectFromMaster()
    {
        MUNNetwork.DisconnectFromMasterServer();
    }

    public void CreateNewMUNView()
    {
        MUNNetwork.CreateNetworkObject("MUNView", Vector3.zero, Quaternion.identity);
    }

    public void SwitchScene()
    {
        string currentScene = SceneManager.GetActiveScene().name;

        if (currentScene.Equals("Scene1")) MUNNetwork.ChangeScene("Scene2", MUNNetwork.ChangeSceneType.Async);
        else if (currentScene.Equals("Scene2")) MUNNetwork.ChangeScene("Scene1", MUNNetwork.ChangeSceneType.Async);
        else Debug.Log("Unknown Scene");

    }

    public override void OnPlayerJoinedRoom(Player player)
    {
        foreach (var testToExecute in testsToExecute)
        {
            if (executeTestOnPlayerJoined.HasFlag(testToExecute.Key))
            {
                Debug.Log($"<color=blue>Test OnPlayerJoinedRoom</color>: {testToExecute.Key}");
                testToExecute.Value.Execute(player, munViewId, playerId);
            }
        }
    }

    public override void OnJoinedRoom()
    {
        foreach (var testToExecute in testsToExecute)
        {
            if (executeTestOnJoined.HasFlag(testToExecute.Key))
            {
                Debug.Log($"<color=pink>Test OnJoinedRoom</color>: {testToExecute.Key}");
                testToExecute.Value.Execute(MUNNetwork.LocalPlayer, munViewId, playerId);
            }
        }
    }

    public override void OnRoomOwnerChanged(Player newOwner)
    {
        if (disconnectFromRoomOnRoomOwnerChanged) MUNNetwork.LeaveRoom();
    }

    public override void OnRoomChangeScene(Scene loadedScene)
    {
        foreach (var testToExecute in testsToExecute)
        {
            if (executeTestOnSceneChangedRoomOwner.HasFlag(testToExecute.Key))
            {
                Debug.Log($"<color=pink>Test OnJoinedRoom</color>: {testToExecute.Key}");
                testToExecute.Value.Execute(MUNNetwork.LocalPlayer, munViewId, playerId);
            }
        }
    }

    public override void OnJoinedRoomFailed(string message)
    {
        Debug.Log($"<b>OnJoinedRoomFailed</b>: {message}.");
    }
}
