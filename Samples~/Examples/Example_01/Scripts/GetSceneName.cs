using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(TMP_Text))]
public class GetSceneName : MonoBehaviour
{
    private void Start()
    {
        var tmpText = GetComponent<TMP_Text>();
        var sceneName = SceneManager.GetActiveScene().name;

        tmpText.text = sceneName;
    }
}
