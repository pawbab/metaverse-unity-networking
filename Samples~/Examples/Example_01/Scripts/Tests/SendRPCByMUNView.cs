﻿using MUN.Client;
using System;

public class SendRPCByMUNView : IExecuteTest
{
    public void Execute(params object[] args)
    {
        if (args is null || args.Length == 0)
        {
            throw new ArgumentException($"Klasa {this.GetType().Name} wymaga argumentu nowego gracza.");
        }

        if (args[1] is int)
        {
            Send((int)args[1]);
        }
        else
        {
            throw new InvalidCastException($"Klasa {GetType().Name} wymaga argumentu Player.");
        }
    }

    private void Send(int munViewId)
    {
        MUNView munView = MUNView.Find(munViewId);

        if (munView != null)// throw new NullReferenceException($"Nieznaleziono MUNView dla Id = {munViewId}");
            munView.GetComponent<ViewRpc>().SendRPC();
    }
}
