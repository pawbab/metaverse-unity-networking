﻿using MUN.Client;
using System;

public class AddPlayerProperty : IExecuteTest
{
    private readonly LightHashTable properties;

    public AddPlayerProperty(LightHashTable properties)
    {
        this.properties = properties;
    }

    public void Execute(params object[] args)
    {
        if (args is null || args.Length == 0)
        {
            throw new ArgumentException($"Klasa {this.GetType().Name} wymaga argumentu nowego gracza.");
        }

        if (args[2] is int)
        {
            var player = MUNNetwork.CurrentRoom.GetPlayer((int)args[2]);

            if (player != null)
                player.SetCustomProperties(properties);
        }
        else
        {
            throw new InvalidCastException($"Klasa {GetType().Name} wymaga argumentu PlayerId.");
        }
    }
}
