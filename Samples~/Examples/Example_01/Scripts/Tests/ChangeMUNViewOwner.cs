﻿using MUN.Client;
using System;

public class ChangeMUNViewOwner : IExecuteTest
{
    public void Execute(params object[] args)
    {
        if (args is null || args.Length == 0)
        {
            throw new ArgumentException($"Klasa {this.GetType().Name} wymaga argumentu nowego gracza.");
        }

        if (args[0] is Player && args[1] is int && args[2] is int)
        {

            ChangeMUNViewOwnerForNewPlayer((Player)args[0], (int)args[1], (int)args[2]);
        }
        else
        {
            throw new InvalidCastException($"Klasa {GetType().Name} wymaga argumentu Player.");
        }
    }

    private void ChangeMUNViewOwnerForNewPlayer(Player newOwner, int munViewId, int otherNewOwner)
    {
        MUNView munView = MUNView.Find(munViewId);

        if (munView is null) throw new NullReferenceException($"Nieznaleziono MUNView dla Id = {munViewId}");

        if (munView.Owner == newOwner)
            newOwner = MUNNetwork.CurrentRoom.GetPlayer(otherNewOwner);

            munView.ChangeOwner(newOwner);
    }
}
