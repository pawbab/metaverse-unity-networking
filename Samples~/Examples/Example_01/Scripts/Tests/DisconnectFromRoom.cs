using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MUN.Client;

public class DisconnectFromRoom : IExecuteTest
{
    public void Execute(params object[] args)
    {
        MUNNetwork.LeaveRoom();
    }
}
