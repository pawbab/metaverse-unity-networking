﻿using MUN.Client;

public class ChangeSceneOnNewPlayerJoin : IExecuteTest
{
    private readonly string sceneName;

    public ChangeSceneOnNewPlayerJoin(string sceneName)
    {
        this.sceneName = sceneName;
    }

    public void Execute(params object[] args)
    {
        MUNNetwork.ChangeScene(sceneName, MUNNetwork.ChangeSceneType.Async);
    }
}
