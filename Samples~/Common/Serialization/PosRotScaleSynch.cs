using MUN.Client;
using MUN.Client.Interfaces;
using System;
using UnityEngine;

namespace MUN.Common
{
    [RequireComponent(typeof(MUNView))]
    public class PosRotScaleSynch : MonoBehaviour, IMUNSerialize
    {
        private event Action<float> update;
        private event Action<MUNPackage> readPacket;
        private event Action<MUNPackage> writePacket;

        [SerializeField] float smooth = 20f;
        [SerializeField] bool updatePos = true;
        [SerializeField] bool updateRot = true;
        [SerializeField] bool updateScale = true;

        private MUNView munView;

        private PosSynch posSynch;
        private RotSynch rotSynch;
        private ScaleSynch scaleSynch;

        private void Start()
        {
            munView = GetComponent<MUNView>();

            if (!munView.IsMine)
            {
                var rbs = GetComponentsInChildren<Rigidbody>();
                foreach (var rb in rbs)
                {
                    rb.useGravity = false;
                    rb.isKinematic = true;
                }
            }

            if (updatePos)
            {
                posSynch = new PosSynch(transform,
                  (updateAction) =>
                {
                    update += updateAction;
                },
                  (readAction) =>
                {
                    readPacket += readAction;
                },
                  (writeAction) =>
                {
                    writePacket += writeAction;
                });
            }

            if (updateRot)
            {
                rotSynch = new RotSynch(transform,
                (updateAction) =>
                {
                    update += updateAction;
                },
                (readAction) =>
                {
                    readPacket += readAction;
                },
                (writeAction) =>
                {
                    writePacket += writeAction;
                });
            }

            if (updateScale)
            {
                scaleSynch = new ScaleSynch(transform,
                (updateAction) =>
                {
                    update += updateAction;
                },
                (readAction) =>
                {
                    readPacket += readAction;
                },
                (writeAction) =>
                {
                    writePacket += writeAction;
                });
            }
        }

        private void Update()
        {
            if (!munView.IsMine)
                update?.Invoke(Time.deltaTime * smooth);
        }

        public void MUNSerializeRead(MUNPackage packet)
        {
            readPacket?.Invoke(packet);
        }

        public void MUNSerializeWrite(MUNPackage packet)
        {
            writePacket?.Invoke(packet);
        }

        private abstract class SynchBase
        {
            protected readonly Transform transform;

            public SynchBase(Transform transform, Action<Action<float>> update, Action<Action<MUNPackage>> readPacket, Action<Action<MUNPackage>> writePacket)
            {
                this.transform = transform;

                update.Invoke(Update);
                readPacket.Invoke(ReadPacket);
                writePacket.Invoke(WritePacket);
            }

            protected abstract void Update(float value);
            protected abstract void ReadPacket(MUNPackage packet);
            protected abstract void WritePacket(MUNPackage packet);
        }

        private class PosSynch : SynchBase
        {
            private Vector3 positionFromServer;

            public PosSynch(Transform transform, Action<Action<float>> update, Action<Action<MUNPackage>> readPacket, Action<Action<MUNPackage>> writePacket)
                : base(transform, update, readPacket, writePacket)
            {
                positionFromServer = transform.position;
            }

            protected override void Update(float value)
            {
                transform.position = Vector3.Lerp(transform.position, positionFromServer, value);
            }

            protected override void WritePacket(MUNPackage packet)
            {
                positionFromServer = transform.position;
                packet.Write(positionFromServer);
            }

            protected override void ReadPacket(MUNPackage packet)
            {
                positionFromServer = packet.ReadVector3();
            }
        }

        private class RotSynch : SynchBase
        {
            private Quaternion rotationFromServer;

            public RotSynch(Transform transform, Action<Action<float>> update, Action<Action<MUNPackage>> readPacket, Action<Action<MUNPackage>> writePacket)
                : base(transform, update, readPacket, writePacket)
            {
                rotationFromServer = transform.rotation;
            }

            protected override void Update(float value)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, rotationFromServer, value);
            }

            protected override void WritePacket(MUNPackage packet)
            {
                rotationFromServer = transform.rotation;
                packet.Write(rotationFromServer);
            }

            protected override void ReadPacket(MUNPackage packet)
            {
                rotationFromServer = packet.ReadQuaternion();
            }
        }

        private class ScaleSynch : SynchBase
        {
            private Vector3 scaleFromServer;

            public ScaleSynch(Transform transform, Action<Action<float>> update, Action<Action<MUNPackage>> readPacket, Action<Action<MUNPackage>> writePacket) : base(transform, update, readPacket, writePacket)
            {
                scaleFromServer = transform.localScale;
            }

            protected override void Update(float value)
            {
                transform.localScale = Vector3.Lerp(transform.localScale, scaleFromServer, value);
            }

            protected override void WritePacket(MUNPackage packet)
            {
                scaleFromServer = transform.localScale;
                packet.Write(scaleFromServer);
            }

            protected override void ReadPacket(MUNPackage packet)
            {
                scaleFromServer = packet.ReadVector3();
            }
        }
    }

    [RequireComponent(typeof(MUNView))]
    public class SynchTest : MonoBehaviour, IMUNSerialize
    {
        public void MUNSerializeWrite(MUNPackage packet)
        {
            packet.Write(transform.position);
        }

        public void MUNSerializeRead(MUNPackage packet)
        {
            transform.position = packet.ReadVector3();
        }
    }
}
