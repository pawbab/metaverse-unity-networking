using MUN.Client;
using MUN.Client.Interfaces;
using UnityEngine;

namespace MUN.Common
{

    [RequireComponent(typeof(Animator))]
    public class MUNAnimation : MonoBehaviour, IMUNSerialize
    {
        [SerializeField] bool updateAnimatorParameters = true;
        [SerializeField] bool updateAnimatorLayersWeight = false;

        private Animator animator;


        private void OnValidate()
        {
            bool hasMUNView = gameObject.GetComponentInParent<MUNView>(true) != null;
            if (!hasMUNView) Debug.LogError($"{gameObject.name} has <b>MUNAnimation</b> component that require <b>MUNView</b> in this object or parent.");
        }

        private void Start()
        {
            animator = GetComponent<Animator>();
        }

        public void MUNSerializeWrite(MUNPackage packet)
        {
            //Do not write data if not required :)
            packet.Write(animator.enabled);
            if (!animator.enabled) return;

            WriteAnimatorParameters();
            WriteAnimatorLayersWeight();

            void WriteAnimatorParameters()
            {
                if (!updateAnimatorParameters) return;

                foreach (var parameter in animator.parameters)
                {
                    switch (parameter.type)
                    {
                        case AnimatorControllerParameterType.Float:
                            float valueFloat = animator.GetFloat(parameter.name);
                            packet.Write(valueFloat);
                            break;
                        case AnimatorControllerParameterType.Int:
                            int valueInt = animator.GetInteger(parameter.name);
                            packet.Write(valueInt);
                            break;
                        case AnimatorControllerParameterType.Bool:
                            bool valueBool = animator.GetBool(parameter.name);
                            packet.Write(valueBool);
                            break;
                    }
                }
            }
            void WriteAnimatorLayersWeight()
            {
                if (!updateAnimatorLayersWeight) return;

                float layerWeight;
                for (int i = 0; i < animator.layerCount; i++)
                {
                    layerWeight = animator.GetLayerWeight(i);
                    packet.Write(layerWeight);
                }
            }
        }

        public void MUNSerializeRead(MUNPackage packet)
        {
            //Do not read data if not required :)
            animator.enabled = packet.ReadBool();
            if (!animator.enabled) return;

            ReadAnimatorParameters();
            ReadAnimatorLayersWeight();

            void ReadAnimatorParameters()
            {
                if (!updateAnimatorParameters) return;

                foreach (var parameter in animator.parameters)
                {
                    switch (parameter.type)
                    {
                        case AnimatorControllerParameterType.Float:
                            float valueFloat = packet.ReadFloat();
                            animator.SetFloat(parameter.name, valueFloat);
                            break;
                        case AnimatorControllerParameterType.Int:
                            int valueInt = packet.ReadInt();
                            animator.SetInteger(parameter.name, valueInt);
                            break;
                        case AnimatorControllerParameterType.Bool:
                            bool valueBool = packet.ReadBool();
                            animator.SetBool(parameter.name, valueBool);
                            break;
                    }
                }
            }
            void ReadAnimatorLayersWeight()
            {
                if (!updateAnimatorLayersWeight) return;

                float layerWeight;
                for (int i = 0; i < animator.layerCount; i++)
                {
                    layerWeight = packet.ReadFloat();
                    animator.SetLayerWeight(i, layerWeight);
                }
            }
        }

        public void SetupCustomAnimator(Animator customAnimator)
        {
            animator = customAnimator;
        }
    }
}