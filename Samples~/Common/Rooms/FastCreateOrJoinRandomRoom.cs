using MUN.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MUN.Common
{
    public class FastCreateOrJoinRandomRoom : MUNobehaviour
    {
        [SerializeField] string playerName = string.Empty;
        [SerializeField, Range(2, 8)] byte maxRoomPlayers = 4;
        [SerializeField] string changeSceneOnJoinedRoomAsOwner = string.Empty;

        private void Start()
        {
            MUNNetwork.PlayerName = playerName;
            MUNNetwork.onPlayerDisconectedMUNViewLogic = MUNNetwork.MUNViewsLogicOnPlayerDisconected.DestroyAllPlayerMUNViews;
            MUNNetwork.ConnectToMaster();
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("OnConnectedToMaster");

            Room.RoomClaims claims = new Room.RoomClaims()
            {
                MaxPlayers = maxRoomPlayers,
                IsVisable = false
            };
            MUNNetwork.JoinOrCreateRandomRoom(claims);
        }

        public override void OnRoomCreated()
        {
            Debug.Log("OnRoomCreated: You are a room owner.");
        }

        public override void OnJoinedRoom()
        {
            byte playersCount = MUNNetwork.CurrentRoom.PlayersInRoom;
            byte maxPlayers = MUNNetwork.CurrentRoom.MaxPlayers;
            Debug.Log($"OnJoinedRoom. Players: {playersCount}/{maxPlayers}");

            if (MUNNetwork.LocalPlayer.IsRoomOwner && !string.IsNullOrEmpty(changeSceneOnJoinedRoomAsOwner))
            {
                MUNNetwork.ChangeScene(changeSceneOnJoinedRoomAsOwner, MUNNetwork.ChangeSceneType.NoAsync);
            }
        }
    }
}