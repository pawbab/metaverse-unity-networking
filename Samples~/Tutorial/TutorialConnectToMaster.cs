using MUN.Client;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MUN.Tutorial
{
    public class TutorialConnectToMaster : MUNobehaviour
    {
        private const string TUTORIAL_USERNAME = "tutorial_username_";

        private void Awake()
        {
            MUNNetwork.PlayerName = TUTORIAL_USERNAME + $"{Random.Range(0, 999)}";
            MUNNetwork.ConnectToMaster();
        }

        #region IMUNCallbacks implementations

        public override void OnJoinedRoom()
        {
            Debug.Log("OnJoinedRoom");
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("OnConnectedToMaster");

            Room.RoomClaims claims = new Room.RoomClaims()
            {
                MaxPlayers = 8
            };
            MUNNetwork.JoinOrCreateRandomRoom(claims);
        }

        public override void OnRoomOwnerChanged(Player newOwner)
        {
            MUNNetwork.LeaveRoom();
        }

        public override void OnLeftRoom()
        {
            OnConnectedToMaster();
        }

        public override void OnConnectedToMasterFailed(string message)
        {
            Debug.Log($"OnConnectedToMasterFailed: {message}");
        }

        public override void OnDisconnectedFromMaster()
        {
            Debug.Log("OnDisconnectedFromMaster");
        }

        #endregion
    }
}