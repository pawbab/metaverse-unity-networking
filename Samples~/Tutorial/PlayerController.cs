using MUN.Client;
using UnityEngine;

namespace MUN.Tutorial
{
    [RequireComponent(typeof(MUNView), typeof(Rigidbody2D), typeof(Collider2D))]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] 
        private float movementSpeed = 5f;
        private Vector2 moveVector = Vector2.zero;
        private bool isMove;

        private MUNView _munView;

        private Rigidbody2D _rigidbody;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _munView = GetComponent<MUNView>();
        }

        private void Update()
        {
            // Get inputs for only our local instance, not for server instance,
            // witch is displayed for other player
            if (_munView.IsMine)
            {
                moveVector.x = Input.GetAxisRaw("Horizontal");
                moveVector.y = Input.GetAxisRaw("Vertical");
            }
        }

        private void FixedUpdate()
        {
            if (_munView.IsMine)
            {
                _rigidbody.velocity = moveVector.normalized * movementSpeed;
            }
        }
    }
}