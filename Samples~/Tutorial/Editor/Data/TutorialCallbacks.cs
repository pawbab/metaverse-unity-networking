﻿using System.IO;
using Unity.Tutorials.Core.Editor;
using UnityEditor;
using UnityEngine;

namespace MUN.Tutorial.Editor.Data
{
    /// <summary>
    /// Implement your Tutorial callbacks here.
    /// </summary>
    [CreateAssetMenu(fileName = DefaultFileName, menuName = "Tutorials/" + DefaultFileName + " Instance")]
    public class TutorialCallbacks : ScriptableObject
    {
        [SerializeField] private TutorialContainer _tutorialContainer;
        
        /// <summary>
        /// The default file name used to create asset of this class type.
        /// </summary>
        public const string DefaultFileName = "TutorialCallbacks";

        /// <summary>
        /// Creates a TutorialCallbacks asset and shows it in the Project window.
        /// </summary>
        /// <param name="assetPath">
        /// A relative path to the project's root. If not provided, the Project window's currently active folder path is used.
        /// </param>
        /// <returns>The created asset</returns>
        public static ScriptableObject CreateAndShowAsset(string assetPath = null)
        {
            assetPath = assetPath ?? $"{TutorialEditorUtils.GetActiveFolderPath()}/{DefaultFileName}.asset";
            var asset = CreateInstance<TutorialCallbacks>();
            AssetDatabase.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(assetPath));
            EditorUtility
                .FocusProjectWindow(); // needed in order to make the selection of newly created asset to really work
            Selection.activeObject = asset;
            return asset;
        }

        /// <summary>
        /// Example callback for basic UnityEvent
        /// </summary>
        public void SetDocumentationDataPath()
        {
            string documentationPath = Path.Combine(Application.dataPath, @"MUN\Documentation\Index.lnk");

            _tutorialContainer.Sections[4].Url = documentationPath;
            EditorUtility.SetDirty(_tutorialContainer);
        }
    }
}