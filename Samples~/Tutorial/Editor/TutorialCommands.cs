using Unity.Tutorials.Core.Editor;
using UnityEditor;
using UnityEngine;

namespace MUN.Tutorial
{
    public class TutorialCommands
    {
        [MenuItem("MUN/Tutorial", false, 15)]
        public static void OpenTutorial()
        {
            var welcomePage = TutorialProjectSettings.Instance.WelcomePage;
            if (welcomePage != null)
                TutorialModalWindow.Show(welcomePage);
            else
                Debug.LogError("No TutorialProjectSettings.WelcomePage set.");
            
            UserStartupCode.ShowTutorialWindow();
        }
    }
}
