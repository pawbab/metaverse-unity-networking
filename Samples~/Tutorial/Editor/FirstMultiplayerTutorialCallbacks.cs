﻿using UnityEngine;
using UnityEditor;
using Unity.Tutorials.Core.Editor;

/// <summary>
/// Implement your Tutorial callbacks here.
/// </summary>
[CreateAssetMenu(fileName = DefaultFileName, menuName = "Tutorials/" + DefaultFileName + " Instance")]
public class FirstMultiplayerTutorialCallbacks : ScriptableObject
{
    [SerializeField] private AudioClip goodSound;
    
    /// <summary>
    /// The default file name used to create asset of this class type.
    /// </summary>
    public const string DefaultFileName = "FirstMultiplayerTutorialCallbacks";

    /// <summary>
    /// Creates a TutorialCallbacks asset and shows it in the Project window.
    /// </summary>
    /// <param name="assetPath">
    /// A relative path to the project's root. If not provided, the Project window's currently active folder path is used.
    /// </param>
    /// <returns>The created asset</returns>
    public static ScriptableObject CreateAndShowAsset(string assetPath = null)
    {
        assetPath = assetPath ?? $"{TutorialEditorUtils.GetActiveFolderPath()}/{DefaultFileName}.asset";
        var asset = CreateInstance<FirstMultiplayerTutorialCallbacks>();
        AssetDatabase.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(assetPath));
        EditorUtility
            .FocusProjectWindow(); // needed in order to make the selection of newly created asset to really work
        Selection.activeObject = asset;
        return asset;
    }

    public void PingConnectToMaster()
    {
        string path = "Assets/MUN/Tutorial/TutorialConnectToMaster.cs";
        TextAsset file = (TextAsset) AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset));
        EditorGUIUtility.PingObject(file);
    }

    public void PingPlayerSpawner()
    {
        string path = "Assets/MUN/Tutorial/PlayerSpawner.cs";
        TextAsset file = (TextAsset) AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset));
        EditorGUIUtility.PingObject(file);
    }
    
    public void PingPlayerController()
    {
        string path = "Assets/MUN/Tutorial/PlayerController.cs";
        TextAsset file = (TextAsset) AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset));
        EditorGUIUtility.PingObject(file);
    }

    public void PingTutorialPlayerPrefab()
    {
        var playerPrefab = Resources.Load("TutorialPlayer") as GameObject;
        EditorGUIUtility.PingObject(playerPrefab);
    }

    public void PlayCorrectSound() => EditorSFX.PlayClip(goodSound);
}