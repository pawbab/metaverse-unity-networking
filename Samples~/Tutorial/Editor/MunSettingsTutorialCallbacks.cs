﻿#if UNITY_EDITOR
using MUN.Editor;
using Unity.Tutorials.Core.Editor;
using UnityEditor;
using UnityEngine;

namespace MUN.Tutorial.Scripts
{
    /// <summary>
    /// Implement your Tutorial callbacks here.
    /// </summary>
    [CreateAssetMenu(fileName = DefaultFileName, menuName = "Tutorials/" + DefaultFileName + " Instance")]
    public class MunSettingsTutorialCallbacks : ScriptableObject
    {
        /// <summary>
        /// The default file name used to create asset of this class type.
        /// </summary>
        public const string DefaultFileName = "MunSettingsTutorialCallbacks";

        /// <summary>
        /// Creates a TutorialCallbacks asset and shows it in the Project window.
        /// </summary>
        /// <param name="assetPath">
        /// A relative path to the project's root. If not provided, the Project window's currently active folder path is used.
        /// </param>
        /// <returns>The created asset</returns>
        public static ScriptableObject CreateAndShowAsset(string assetPath = null)
        {
            assetPath = assetPath ?? $"{TutorialEditorUtils.GetActiveFolderPath()}/{DefaultFileName}.asset";
            var asset = CreateInstance<MunSettingsTutorialCallbacks>();
            AssetDatabase.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(assetPath));
            EditorUtility
                .FocusProjectWindow(); // needed in order to make the selection of newly created asset to really work
            Selection.activeObject = asset;
            return asset;
        }

        public bool IsMunSettingsWindowOpened() => MUNEditor.MUNSettingsWindow.IsWindowOpened;
        public bool IsGeneralTabSelected() => MUNEditor.MUNSettingsWindow.IsGeneralTabSelected;
        public bool IsRpcTabSelected() => MUNEditor.MUNSettingsWindow.IsRpcTabSelected;
        public bool AlwaysTrue() => true;
        public bool AlwaysFalse() => false;
        public void OnDone() => MUNEditor.MUNSettingsWindow.CloseWindow();
    }
}

#endif