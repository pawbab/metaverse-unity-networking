﻿using UnityEngine;
using UnityEditor;
using Unity.Tutorials.Core.Editor;

/// <summary>
/// Implement your Tutorial callbacks here.
/// </summary>
[CreateAssetMenu(fileName = DefaultFileName, menuName = "Tutorials/" + DefaultFileName + " VoiceChatCallbacksInstance")]
public class VoiceChatCallbacks : ScriptableObject
{
    [SerializeField] private AudioClip goodSound;

    public Transform firstTransform;
    public Transform secondTransform;
    
    /// <summary>
    /// The default file name used to create asset of this class type.
    /// </summary>
    public const string DefaultFileName = "VoiceChatCallbacks";

    /// <summary>
    /// Creates a TutorialCallbacks asset and shows it in the Project window.
    /// </summary>
    /// <param name="assetPath">
    /// A relative path to the project's root. If not provided, the Project window's currently active folder path is used.
    /// </param>
    /// <returns>The created asset</returns>
    public static ScriptableObject CreateAndShowAsset(string assetPath = null)
    {
        assetPath = assetPath ?? $"{TutorialEditorUtils.GetActiveFolderPath()}/{DefaultFileName}.asset";
        var asset = CreateInstance<VoiceChatCallbacks>();
        AssetDatabase.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(assetPath));
        EditorUtility.FocusProjectWindow(); // needed in order to make the selection of newly created asset to really work
        Selection.activeObject = asset;
        return asset;
    }

    public void PingVoiceConnector()
    {
        var voiceConnector = Resources.Load("MunConnectorSpawner") as GameObject;
        EditorGUIUtility.PingObject(voiceConnector);
    } 
    
    public void PingSpeaker()
    {
        var voiceConnector = Resources.Load("Speaker") as GameObject;
        EditorGUIUtility.PingObject(voiceConnector);
    }

    public bool DoesVoiceConnectorExist()
    {
        if (!GameObject.Find("MunConnectorSpawner")) return false;
        Transform firstTransform = GameObject.Find("MunConnectorSpawner").transform;
        Transform secondTransform = GameObject.Find("PlayerSpawner").transform;
        int munVoiceChatSpawnerIndex = firstTransform.GetSiblingIndex();
        int playerSpawnerIndex = secondTransform.GetSiblingIndex();
        return firstTransform.parent == secondTransform.parent // The same indent
               && munVoiceChatSpawnerIndex < playerSpawnerIndex; // Voice chat spawner is before 
    }

    public bool DoesSpeakerExist()
    {
        var playerPrefab = Resources.Load("TutorialPlayer") as GameObject;
        return playerPrefab.transform.Find("Speaker") != null;
    }

    public void PlayCorrectSound() => EditorSFX.PlayClip(goodSound);
}
