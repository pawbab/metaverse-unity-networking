﻿using MUN.Editor;
using MUN.Service;
using Unity.Tutorials.Core.Editor;
using UnityEditor;
using UnityEngine;

namespace MUN.Tutorial.Editor.Scripts
{
    
    /// <summary>
    /// Implement your Tutorial callbacks here.
    /// </summary>
    [CreateAssetMenu(fileName = DefaultFileName, menuName = "Tutorials/" + DefaultFileName + " Instance")]
    public class MunConfigurationTutorialCallbacks : ScriptableObject
    {
        /// <summary>
        /// The default file name used to create asset of this class type.
        /// </summary>
        public const string DefaultFileName = "MunConfigurationTutorialCallbacks";

        /// <summary>
        /// Creates a TutorialCallbacks asset and shows it in the Project window.
        /// </summary>
        /// <param name="assetPath">
        /// A relative path to the project's root. If not provided, the Project window's currently active folder path is used.
        /// </param>
        /// <returns>The created asset</returns>
        public static ScriptableObject CreateAndShowAsset(string assetPath = null)
        {
            assetPath = assetPath ?? $"{TutorialEditorUtils.GetActiveFolderPath()}/{DefaultFileName}.asset";
            var asset = CreateInstance<MunConfigurationTutorialCallbacks>();
            AssetDatabase.CreateAsset(asset, AssetDatabase.GenerateUniqueAssetPath(assetPath));
            EditorUtility
                .FocusProjectWindow(); // needed in order to make the selection of newly created asset to really work
            Selection.activeObject = asset;
            return asset;
        }

        private bool wasCreateApplicationWindowOpen;

        public bool MunServiceWindowOpened() => MUNEditor.IsWindowOpen;

        public bool IsUserLogged() => !string.IsNullOrEmpty(ServiceRequest.Account.Token);
        public bool IsAccountTabActive() => MUNEditor.IsAccountTabActive;
        public bool IsOrganizationTabActive() => MUNEditor.IsOrganizationTabActive;
        public bool HasOrganizations() => ServiceRequest.Organization.Organizations.Length > 0;
        public bool IsApplicationTabActive() => MUNEditor.IsApplicationTabActive;
        public bool IsCreateAppWindowOpen()
        {
            bool isCreateApplicationWindowOpened = MUNEditor.IsCreateApplicationWindowOpened;
            if (isCreateApplicationWindowOpened)
                wasCreateApplicationWindowOpen = true;

            return wasCreateApplicationWindowOpen;
        }
        public bool HasAnyApplication() => ServiceRequest.Application.Applications.Length > 0;
        public void OnDone() => MUNEditor.CloseWindow();
    }
}